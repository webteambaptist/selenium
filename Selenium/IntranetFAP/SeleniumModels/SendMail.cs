﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace IntranetFAP.SeleniumModels
{
    public class SendMail
    {
        private string to;
        private string from;

        public SendMail()
        {
            var configs = new Configs();
            to = configs.GetMailTo();
            from = configs.GetFrom();
        }

        public void SendEMail(string subject, string body)
        {
            var msg = new MailMessage(from, to)
            {
                Subject = subject,
                Body = body,
                Sender = new MailAddress(from)
            };
            SendMailMessage(msg);
        }

        private void SendMailMessage(MailMessage m)
        {
            try
            {
                m.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Host = "smtp.bmcjax.com",
                    Port = 25,
                    EnableSsl = false
                };


                //if (string.IsNullOrEmpty(m.To.ToString())) return "NO SENDER"; //must have a sender

                smtp.Send(m);

            }
            catch (Exception)
            {

            }
        }
    }
}
