﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace IntranetFAP.SeleniumModels
{
    public class AllSearchFilters
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        public AllSearchFilters(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestAllSearchFilters()
        {
            var searchNameInput = "";
            var searchByDoctorSpecialty = "";
            var searchByLanguage = "";
            var searchZipCodeInput = "";
            var searchSearchWithinOption = "";

            var result = "Starting Test for the Search By All Filters";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            result = "Testing Search By All filters";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            try
            {
                searchNameInput = _configs.GetAllFiltersNameInput();
                searchByDoctorSpecialty = _configs.GetAllFiltersByDoctorSpecialty();
                searchByLanguage = _configs.GetAllFiltersByLanguage();
                searchZipCodeInput = _configs.GetAllFiltersZipCodeInput();
                searchSearchWithinOption = _configs.GetAllFiltersSearchWithinOption();
            }
            catch (Exception e)
            {
                result = "EXCEPTION :: Exception occurred in :: Getting all of the Search Filter options ::. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                m.AddToBody(result += "<br/>");
            }


            //Search By Name
            try
            {
                Task.Delay(5000).Wait();
                d.SendKeysById("lastName", searchNameInput);
                d.ClickById("lastName");
            }
            catch (Exception e)
            {
                result = "EXCEPTION :: Exception occurred in :: Filling out Search By Name field ::. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                m.AddToBody(result += "<br/>");
            }

            //Search by Doctor Specialty
            try
            {
                d.ClickById("searchBySpecialty");

                d.wait.Until(profile =>
                {
                    try
                    {
                        return d.IsIdDisplayed("specialtyPanel");
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                });

                if (d.IsIdDisplayed("specialtyPanel"))
                {
                    var specialtiesList = d.GetElementByClassName("docSpecialtyFields");
                    var specialtyCount = specialtiesList.FindElements(By.ClassName("cbSpecialty"));
                    var count = specialtyCount.Count;

                    if (count > 0)
                    {
                        result = "Pass: Intranet FAP Doctor Specialties Displayed successful ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");

                        try
                        {
                            var checkbox = d.GetElementById(searchByDoctorSpecialty);
                            checkbox.Click();
                        }
                        catch (Exception e)
                        {
                            result = "EXCEPTION :: Exception occurred in :: Doctor Specialties ::. <br/>";
                            result += e.Message;
                            Console.WriteLine(result);
                            m.AddToBody(result += "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: Intranet FAP Search By Doctor Specialty results didnt display. Error retrieving Provider Data from DB. ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                else
                {
                    result = "Fail: Intranet FAP Doctor Specialties did not Display. <br/>";
                    Console.WriteLine(result);
                    m.AddToBody(result += "<br/>");
                }
            }
            catch (Exception e)
            {
                result = "EXCEPTION :: Exception occurred in :: Filling out Search By Specialty field ::. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                m.AddToBody(result += "<br/>");
            }

            //Search by Language
            try
            {
                Task.Delay(2000).Wait();
                d.ClickById("searchByLanguage");

                d.wait.Until(profile =>
                {
                    try
                    {
                        return d.IsIdDisplayed("languagePanel");
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                });

                if (d.IsIdDisplayed("languagePanel"))
                {
                    var specialtiesList = d.GetElementByClassName("docLanguageFields");
                    var specialtyCount = specialtiesList.FindElements(By.ClassName("cbLanguage"));
                    var count = specialtyCount.Count;

                    if (count > 0)
                    {
                        result = "Pass: Intranet FAP Doctor Languages Displayed successful ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");

                        try
                        {
                            var checkbox = d.GetElementById(searchByLanguage);
                            checkbox.Click();
                        }
                        catch (Exception e)
                        {
                            result = "EXCEPTION :: Exception occurred in :: Doctor Languages ::. <br/>";
                            result += e.Message;
                            Console.WriteLine(result);
                            m.AddToBody(result += "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: Intranet FAP Search By Doctor Language results didnt display. Error retrieving Provider Data from DB. ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                else
                {
                    result = "Fail: Intranet FAP Doctor Languages did not Display. <br/>";
                    Console.WriteLine(result);
                    m.AddToBody(result += "<br/>");
                }
            }
            catch (Exception e)
            {
                result = "Fail: Intranet FAP Search By Doctor Language Results didnt return any data. Could find Doctor's Language field";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }

            //Search by BPP Flag
            try
            {
                Task.Delay(2000).Wait();
                d.ClickById("isBPP");
            }
            catch (Exception e)
            {
                result = "EXCEPTION :: Exception occurred in :: Filling out Search By BPP Flag field ::. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                m.AddToBody(result += "<br/>");
            }

            //Search by Gender
            try
            {
                Task.Delay(2000).Wait();
                d.ClickById("genderFemale");
            }
            catch (Exception e)
            {
                result = "EXCEPTION :: Exception occurred in :: Filling out Search By Gender field ::. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                m.AddToBody(result += "<br/>");
            }

            //Search by Distance
            try
            {
                d.SendKeysById("zipCode", searchZipCodeInput);
                d.ClickById("zipCode");

                var searchWithin = d.GetElementById("searchDistance");
                var selectElement = new SelectElement(searchWithin);

                selectElement.SelectByValue(searchSearchWithinOption);
            }
            catch (Exception e)
            {
                result = "EXCEPTION :: Exception occurred in :: Filling out Search By Distance fields ::. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                m.AddToBody(result += "<br/>");
            }

            result = "All Input fields filled in successfully";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            d.ClickById("btnSearch");

            try
            {
                Task.Delay(5000).Wait();
                var provider = d.GetElementById("providerName").Text;

                var providerResult = _configs.GetAllAppliedFiltersProvider();
                if (provider.Contains(providerResult))
                {
                    result = "Pass: Intranet FAP Search From All Filters Results successful ";
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception e)
            {
                // checking results from search name
                result = "Fail: Intranet FAP Search By All Filters Results didnt return any data. Error retrieving Provider Data from DB. ";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }

        }
    }
}
