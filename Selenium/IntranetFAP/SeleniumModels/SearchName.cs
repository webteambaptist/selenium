﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntranetFAP.SeleniumModels
{
    public class SearchName
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        public SearchName(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestSearchByName()
        {
            var result = "Starting Test for the Search By Name Filter";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            result = "Testing Search By Name Field";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            try
            {
                Task.Delay(5000).Wait();
                var searchNameInput = _configs.GetSearchNameInput();
                d.SendKeysById("lastName", searchNameInput);
                d.ClickById("lastName");

                d.ClickById("btnSearch");

                try
                {
                    Task.Delay(5000).Wait();
                    var provider = d.GetElementById("providerName").Text;

                    if (provider.Contains("Aziz"))
                    {
                        result = "Pass: Intranet FAP Search By Name Results successful ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");

                        try
                        {
                            var providerProfiles = d.GetElementsByClassName("view-profiles");
                            providerProfiles.First().Click();

                            d.wait.Until(profile =>
                            {
                                try
                                {
                                    return d.IsClassNameDisplayed("providerName");
                                }
                                catch (Exception e)
                                {
                                    return false;
                                }
                            });

                            if (d.IsClassNameDisplayed("providerName"))
                            {
                                result = "Pass: Intrnaet FAP Profile Displayed successful ";
                                Console.WriteLine(result);
                                m.AddToBody(result + "<br/>");

                                var profileHeader = d.GetElementByClassName("providerName").Text;
                                if (profileHeader.Contains("Zeeshan S. Aziz, MD"))
                                {
                                    result = "Pass: FAP Profile For Zeeshan S. Aziz Displayed successful ";
                                    Console.WriteLine(result);
                                    m.AddToBody(result + "<br/>");
                                }
                            }
                            else
                            {
                                result = "Fail: Intranet FAP Profile did not Display when selecting profile from Search By Name. <br/>";
                                Console.WriteLine(result);
                                m.AddToBody(result += "<br/>");
                            }

                            result = "FAP Search for from Search by Name Zeeshan S. Aziz Completed";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");

                            //Back to Search Results
                            d.GetElementById("back").Click();

                            //Clear Search By Name Field
                            d.GetElementById("lastName").Clear();

                            //Reset Search Filters
                            d.GetElementById("btnResetFilters").Click();
                        }
                        catch (Exception e)
                        {
                            result = "EXCEPTION :: Exception occurred in :: Provider Profile ::. <br/>";
                            result += e.Message;
                            Console.WriteLine(result);
                            m.AddToBody(result += "<br/>");
                        }
                    }
                }
                catch (Exception e)
                {
                    // checking results from search name
                    result = "Fail: Intranet FAP Search By Name Results didnt return any data. Error retrieving Provider Data from DB. ";
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception e)
            {
                // search name try
                result = "Fail: Intranet FAP Search By Name Results didnt return any data. Couldnt find Search Name field or Filter button ";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }
        }

    }
}
