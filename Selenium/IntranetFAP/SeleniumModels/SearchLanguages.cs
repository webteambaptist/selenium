﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace IntranetFAP.SeleniumModels
{
    public class SearchLanguages
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        public SearchLanguages(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestSearchByLanguage()
        {
            var result = "Starting Test for the Search By Language";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            result = "Testing Search By Doctor Language";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            try
            {
                Task.Delay(2000).Wait();
                d.ClickById("searchByLanguage");

                d.wait.Until(profile =>
                {
                    try
                    {
                        return d.IsIdDisplayed("languagePanel");
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                });

                if (d.IsIdDisplayed("languagePanel"))
                {
                    var specialtiesList = d.GetElementByClassName("docLanguageFields");
                    var specialtyCount = specialtiesList.FindElements(By.ClassName("cbLanguage"));
                    var count = specialtyCount.Count;

                    if (count > 0)
                    {
                        result = "Pass: Intranet FAP Doctor Languages Displayed successful ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");

                        try
                        {
                            var searchByLanguage = _configs.GetSearchByLanguage();

                            var checkbox = d.GetElementById(searchByLanguage);
                            checkbox.Click();
                        }
                        catch (Exception e)
                        {
                            result = "EXCEPTION :: Exception occurred in :: Doctor Languages ::. <br/>";
                            result += e.Message;
                            Console.WriteLine(result);
                            m.AddToBody(result += "<br/>");
                        }

                        try
                        {
                            d.ClickById("btnSearch");

                            Task.Delay(5000).Wait();

                            var providerResult = d.GetElementById("rPhysicianSearch");

                            var providerCount = providerResult.FindElements(By.Id("physicianRow"));
                            var count2 = providerCount.Count;

                            if (count2 > 0)
                            {
                                result = "Pass: Intranet FAP Doctor Language Search results Displayed successful ";
                                Console.WriteLine(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "Fail: Intranet FAP Doctor Language Search results didnt display. Error retrieving Provider Data from DB. ";
                                Console.WriteLine(result);
                                m.AddToBody(result + "<br/>");
                            }

                            result = "FAP Search by Doctor Language completed";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");

                            //Reset Search Filters
                            d.GetElementById("btnResetFilters").Click();

                        }
                        catch (Exception e)
                        {
                            //search after applying search by doctor language filter
                            result = "Fail: Intranet FAP Search By Doctor Language Results didnt return any data. Could find Doctor's Specialty field or Search button.";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: Intranet FAP Search By Doctor Language results didnt display. Error retrieving Provider Data from DB. ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                else
                {
                    result = "Fail: Intranet FAP Doctor Languages did not Display. <br/>";
                    Console.WriteLine(result);
                    m.AddToBody(result += "<br/>");
                }
            }
            catch (Exception e)
            {
                result = "Fail: Intranet FAP Search By Doctor Language Results didnt return any data. Could find Doctor's Language field";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }
        }

    }
}
