﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace IntranetFAP.SeleniumModels
{
    public class SearchBPP
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        public SearchBPP(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestSearchBPPFlag()
        {
            var result = "Starting Test for the Search By BPP Flag";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            result = "Testing Search By BPP Flag option";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            try
            {
                Task.Delay(2000).Wait();
                d.ClickById("isBPP");
                d.ClickById("btnSearch");

                try
                {
                    var providerResult = d.GetElementById("rPhysicianSearch");

                    var providerCount = providerResult.FindElements(By.Id("physicianRow"));
                    var count = providerCount.Count;

                    if (count > 0)
                    {
                        var providersAreBPP = providerResult.FindElements(By.Id("bppLogo"));
                        var bppCount = providersAreBPP.Count;

                        if (bppCount == count)
                        {
                            result = "Pass: Intranet FAP Search by BPP Flag results Displayed successful. Only providers that have the BPP Flag are being displayed.";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Intranet FAP Search By BPP Flag Results returned someone that didnt have the BPP Flag.";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: Intranet FAP Search By BPP Flag Results didnt return any data. Could find BPP Flag option";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");
                    }

                    result = "FAP Search by BPP Flag completed";
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");

                    //Reset Search Filters
                    d.GetElementById("btnResetFilters").Click();
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Provider Profile ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    m.AddToBody(result += "<br/>");
                }
            }
            catch (Exception e)
            {
                result = "Fail: Intranet FAP Search By BPP Flag Results didnt return any data. Could find BPP Flag option or Filter Button.";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }
        }
    }
}
