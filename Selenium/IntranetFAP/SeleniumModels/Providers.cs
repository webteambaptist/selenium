﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Linq;

namespace IntranetFAP.SeleniumModels
{
    public class Providers
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        public Providers(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestInitialProviderResults()
        {
            var result = "Starting Testing Initial Load of Providers";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");
            try
            {
                Task.Delay(5000).Wait();

                var providerResult = d.GetElementById("rPhysicianSearch");

                var providerCount = providerResult.FindElements(By.Id("physicianRow"));
                var count = providerCount.Count;

                if (count > 0)
                {
                    result = "Pass: Intranet FAP initial results Displayed successful ";
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");

                    try
                    {
                        var providerProfiles = d.GetElementsByClassName("view-profiles");
                        providerProfiles.First().Click();

                        d.wait.Until(profile =>
                        {
                            try
                            {
                                return d.IsClassNameDisplayed("providerName");
                            }
                            catch (Exception e)
                            {
                                return false;
                            }
                        });

                        if (d.IsClassNameDisplayed("providerName"))
                        {
                            result = "Pass: Intrnaet FAP Profile Displayed successful ";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");

                            var profileHeader = d.GetElementByClassName("providerName").Text;
                            if (profileHeader.Contains("Abiedu C. Abaaba"))
                            {
                                result = "Pass: FAP Profile For Abiedu C. Abaaba Displayed successful ";
                                Console.WriteLine(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }
                        else
                        {
                            result = "Fail: Intranet FAP Profile did not Display. <br/>";
                            Console.WriteLine(result);
                            m.AddToBody(result += "<br/>");
                        }
                    }
                    catch (Exception e)
                    {
                        result = "EXCEPTION :: Exception occurred in :: Provider Profile ::. <br/>";
                        result += e.Message;
                        Console.WriteLine(result);
                        m.AddToBody(result += "<br/>");
                    }
                }
                else
                {
                    result = "Fail: Intranet FAP initial results didnt display. Error retrieving Provider Data from DB. ";
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");
                }

                result = "FAP Search for Abiedu C. Abaaba Completed";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");

                d.GetElementById("back").Click();
            }
            catch (Exception e)
            {
                result = "EXCEPTION: Exception occurred while testing Profile in Intranet FAP. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                m.AddToBody(result += "<br/>");
            }
        }
    }
}
