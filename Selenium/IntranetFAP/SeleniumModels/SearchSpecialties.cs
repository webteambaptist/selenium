﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace IntranetFAP.SeleniumModels
{
    public class SearchSpecialties
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        public SearchSpecialties(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestSearchByDoctorSpecialty()
        {
            var result = "Starting Test for the Search By Doctor Specialty";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            result = "Testing Search By Doctor Specialty";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            try
            {
                Task.Delay(2000).Wait();
                d.ClickById("searchBySpecialty");

                d.wait.Until(profile =>
                {
                    try
                    {
                        return d.IsIdDisplayed("specialtyPanel");
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                });

                if (d.IsIdDisplayed("specialtyPanel"))
                {
                    var specialtiesList = d.GetElementByClassName("docSpecialtyFields");
                    var specialtyCount = specialtiesList.FindElements(By.ClassName("cbSpecialty"));
                    var count = specialtyCount.Count;

                    if (count > 0)
                    {
                        result = "Pass: Intranet FAP Doctor Specialties Displayed successful ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");

                        try
                        {
                            var searchByDoctorSpecialty = _configs.GetSearchByDoctorSpecialty();

                            var checkbox = d.GetElementById(searchByDoctorSpecialty);
                            checkbox.Click();
                        }
                        catch (Exception e)
                        {
                            result = "EXCEPTION :: Exception occurred in :: Doctor Specialties ::. <br/>";
                            result += e.Message;
                            Console.WriteLine(result);
                            m.AddToBody(result += "<br/>");
                        }

                        try
                        {
                            d.ClickById("btnSearch");

                            Task.Delay(5000).Wait();

                            var providerResult = d.GetElementById("rPhysicianSearch");

                            var providerCount = providerResult.FindElements(By.Id("physicianRow"));
                            var count2 = providerCount.Count;

                            if (count2 > 0)
                            {
                                result = "Pass: Intranet FAP Doctor Specialty Search results Displayed successful ";
                                Console.WriteLine(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "Fail: Intranet FAP Doctor Specialty Search results didnt display. Error retrieving Provider Data from DB. ";
                                Console.WriteLine(result);
                                m.AddToBody(result + "<br/>");
                            }

                            result = "FAP Search by Doctor Specialty";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");

                            //Reset Search Filters
                            d.GetElementById("btnResetFilters").Click();

                        }
                        catch (Exception e)
                        {
                            //search after applying search by doctor specialty filter
                            result = "Fail: Intranet FAP Search By Doctor Specialty Results didnt return any data. Could find Doctor's Specialty field or Search button.";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: Intranet FAP Search By Doctor Specialty results didnt display. Error retrieving Provider Data from DB. ";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                else
                {
                    result = "Fail: Intranet FAP Doctor Specialties did not Display. <br/>";
                    Console.WriteLine(result);
                    m.AddToBody(result += "<br/>");
                }
            }
            catch (Exception e)
            {
                result = "Fail: Intranet FAP Search By Doctor Specialty Results didnt return any data. Could find Doctor's Specialty field";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }
        }

    }
}
