﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IntranetFAP.SeleniumModels
{
    public class Configs
    {
        private IConfigurationRoot _config;
        private string mailTo;
        private string from;
        private string mainUrl;
        private string searchNameInput;
        private string doctorSpecialty;
        private string language;
        private string zipCode;
        private string searchWithin;

        private string allFiltersNameInput;
        private string allFiltersDoctorSpecialty;
        private string allFiltersLanguage;
        private string allFiltersZipCode;
        private string allFiltersWithin;
        private string allFiltersAppliedProvider;

        public Configs()
        {
            var directory = Directory.GetCurrentDirectory();
            var builder = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("appsettings.json", true, true);

            _config = builder.Build();

            mailTo = _config.GetSection("EmailSetting").GetSection("MailTo").Value;
            from = _config.GetSection("EmailSetting").GetSection("From").Value;
            mainUrl = _config.GetSection("AppSettings").GetSection("Intranet_URL").Value;
            searchNameInput = _config.GetSection("SearchFilters").GetSection("SearchByName").Value;
            doctorSpecialty = _config.GetSection("SearchFilters").GetSection("SearchByDoctorSpecialty").Value;
            language = _config.GetSection("SearchFilters").GetSection("SearchByLanguage").Value;
            zipCode = _config.GetSection("SearchFilters").GetSection("SearchByZipCode").Value;
            searchWithin = _config.GetSection("SearchFilters").GetSection("SearchWithin").Value;

            allFiltersNameInput = _config.GetSection("AllSearchFilters").GetSection("AllFiltersByName").Value;
            allFiltersDoctorSpecialty = _config.GetSection("AllSearchFilters").GetSection("AllFiltersByDoctorSpecialty").Value;
            allFiltersLanguage = _config.GetSection("AllSearchFilters").GetSection("AllFiltersByLanguage").Value;
            allFiltersZipCode = _config.GetSection("AllSearchFilters").GetSection("AllFiltersByZipCode").Value;
            allFiltersWithin = _config.GetSection("AllSearchFilters").GetSection("AllFiltersSearchWithin").Value;
            allFiltersAppliedProvider = _config.GetSection("AllSearchFilters").GetSection("AllFiltersAppliedProvider").Value;
        }

        public string GetMailTo()
        {
            return mailTo;
        }
        public string GetFrom()
        {
            return from;
        }
        public string GetMainUrl()
        {
            return mainUrl;
        }

        public string GetSearchNameInput()
        {
            return searchNameInput;
        }
        public string GetSearchByDoctorSpecialty()
        {
            return doctorSpecialty;
        }
        public string GetSearchByLanguage()
        {
            return language;
        }
        public string GetSearchZipCodeInput()
        {
            return zipCode;
        } 
        public string GetSearchWithinOption()
        {
            return searchWithin;
        }

        public string GetAllFiltersNameInput()
        {
            return allFiltersNameInput;
        }
        public string GetAllFiltersByDoctorSpecialty()
        {
            return allFiltersDoctorSpecialty;
        }
        public string GetAllFiltersByLanguage()
        {
            return allFiltersLanguage;
        }
        public string GetAllFiltersZipCodeInput()
        {
            return allFiltersZipCode;
        }
        public string GetAllFiltersSearchWithinOption()
        {
            return allFiltersWithin;
        }
        public string GetAllAppliedFiltersProvider()
        {
            return allFiltersAppliedProvider;
        }
    }
}
