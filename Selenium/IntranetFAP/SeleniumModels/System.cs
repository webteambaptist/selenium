﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntranetFAP.SeleniumModels
{
    public class System
    {
        private readonly Driver _driver;
        private readonly Configs _configs;
        private readonly Mail _m;

        public System()
        {
            if (_driver != null) return;
            _driver = new Driver();
            _configs = new Configs();
            _m = new Mail("Intranet FAP Regression Testing", _configs.GetMailTo(), _configs.GetFrom());
        }

        public Driver GetDriver()
        {
            return _driver;
        }
        public Configs GetConfigs()
        {
            return _configs;
        }
        public Mail GetMail()
        {
            return _m;
        }
    }
}
