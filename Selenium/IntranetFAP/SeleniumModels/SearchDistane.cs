﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace IntranetFAP.SeleniumModels
{
    public class SearchDistane
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        public SearchDistane(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestSearchByDistance()
        {
            var result = "Starting Test for the Search By Distance";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            result = "Testing Search By Distance";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            try
            {
                Task.Delay(2000).Wait();
                var searchZipCodeInput = _configs.GetSearchZipCodeInput();
                d.SendKeysById("zipCode", searchZipCodeInput);
                d.ClickById("zipCode");

                d.wait.Until(webDriver =>
                {
                    try
                    {
                        return d.IsIdDisplayed("searchDistance");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                var searchWithin = d.GetElementById("searchDistance");
                var selectElement = new SelectElement(searchWithin);

                var searchSearchWithinOption = _configs.GetSearchWithinOption();
                selectElement.SelectByValue(searchSearchWithinOption);

                d.ClickById("btnSearch");

                d.wait.Until(webDriver =>
                {
                    try
                    {
                        return d.IsIdDisplayed("rPhysicianSearch");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                try
                {
                    var providerResult = d.GetElementById("rPhysicianSearch");

                    var providerCount = providerResult.FindElements(By.Id("physicianRow"));
                    var count = providerCount.Count;

                    if (count > 0)
                    { 
                        result = "Pass: Intranet FAP Search by Distance results Displayed successful. Only providers that are with the selected Distance are being displayed.";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");
                    }
                    else
                    {
                        result = "Fail: Intranet FAP Search By Distance Results returned someone that isnt outside of the range of the Distance selected.";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");
                    }

                    result = "FAP Search by Distance completed";
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");

                    //Reset Search Filters
                    d.GetElementById("btnResetFilters").Click();
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Distance Search ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    m.AddToBody(result += "<br/>");
                }
            }
            catch (Exception e)
            {
                result = "Fail: Intranet FAP Search By Distance Results didnt return any data. Couldnt find ZipCode field, Search Within options or Filter button ";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }
        }
    }
}
