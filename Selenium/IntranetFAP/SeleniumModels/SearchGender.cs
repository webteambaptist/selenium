﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace IntranetFAP.SeleniumModels
{
    public class SearchGender
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        public SearchGender(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestSearchByGender()
        {
            var result = "Starting Test for the Search By Gender";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            result = "Testing Search By Gender option";
            Console.WriteLine(result);
            m.AddToBody(result += "<br/>");

            try
            {
                d.wait.Until(webDriver =>
                {
                    try
                    {
                        return d.IsIdDisplayed("genderMale");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickById("genderMale");
                d.wait.Until(webDriver =>
                {
                    try
                    {
                        return d.IsIdDisplayed("btnSearch");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickById("btnSearch");
                d.wait.Until(webDriver =>
                {
                    try
                    {
                        return d.IsIdDisplayed("rPhysicianSearch");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                try
                {
                    var providerResult = d.GetElementById("rPhysicianSearch");

                    var providerCount = providerResult.FindElements(By.Id("physicianRow"));
                    var count = providerCount.Count;

                    if (count > 0)
                    {
                        var providersAreMale = providerResult.FindElements(By.ClassName("gender"));
                        var maleCount = providersAreMale.Count;

                        if (maleCount == count)
                        {
                            result = "Pass: Intranet FAP Search by Gender(Male) results Displayed successful. Only providers that are Male are being displayed.";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Intranet FAP Search By Gender Results returned someone that has a Gender that isnt Male.";
                            Console.WriteLine(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: Intranet FAP Search By Gender Results didnt return any data.";
                        Console.WriteLine(result);
                        m.AddToBody(result + "<br/>");
                    }

                    result = "FAP Search by Gender completed";
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");

                    //Reset Search Filters
                    d.wait.Until(webDriver =>
                    {
                        try
                        {
                            return d.IsIdDisplayed("btnResetFilters");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    d.GetElementById("btnResetFilters").Click();
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Gender Search ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    m.AddToBody(result += "<br/>");
                }
            }
            catch (Exception e)
            {
                result = "Fail: Intranet FAP Search By Gender Results didnt return any data. Could find the selected Gender option or Filter Button.";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }
        }
    }
}
