﻿using IntranetFAP.SeleniumModels;
using NLog;
using SeleniumCommon;
using System;

namespace IntranetFAP
{
    class Program
    {
        private static string _result;
        private static Mail m;
        private static SendMail mail;
        private static Driver driver;
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            try
            {
                var config = new NLog.Config.LoggingConfiguration();
                var logFile = new NLog.Targets.FileTarget("logFile")
                {
                    FileName = @".\logs\IntranetFAP.log"
                };

                config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
                LogManager.Configuration = config;

                Logger.Info("Starting Test for Intranet FAP Application...");

                var system = new SeleniumModels.System();

                Logger.Info("System Model Created..");
                var configs = system.GetConfigs();
                driver = system.GetDriver();
                m = system.GetMail();
                mail = new SendMail();

                driver.MaximizeWindow();
                _result = "** Starting Intranet FAP Regression Testing **";
                Logger.Info("** Starting Intranet FAP Regression Testing **");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                #region load site
                _result = "** Loading Site **";
                Logger.Info("Loading Site");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Configs " + configs);
                Logger.Info("URL " + configs.GetMainUrl());
                driver.LoadSite(configs.GetMainUrl());

                _result = "Site Loaded";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                #endregion

                #region providers
                _result = "** Checking Initial Providers List **";
                Logger.Info("Checking Initial Providers List");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var providers = new Providers(driver, m, system);
                providers.TestInitialProviderResults();

                _result = "Initial Provider Profile Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Initial Provider Profile Completed");
                #endregion


                #region search by name filter
                _result = "** Provider Search By Name Filter **";
                Logger.Info("Starting Provider Search By Name Filter");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var searchName = new SearchName(driver, m, system);
                searchName.TestSearchByName();

                #endregion

                #region search by doctor specialty
                _result = "** Provider Search By Doctor Specialty **";
                Logger.Info("Starting Provider Search By Doctor Specialty");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var searchSpecialties = new SearchSpecialties(driver, m, system);
                searchSpecialties.TestSearchByDoctorSpecialty();

                #endregion

                #region search by language
                _result = "** Provider Search By Language **";
                Logger.Info("Starting Provider Search By Language");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var searchLanguages = new SearchLanguages(driver, m, system);
                searchLanguages.TestSearchByLanguage();

                #endregion

                #region search by BPP Flag
                _result = "** Provider Search By BPP Flag **";
                Logger.Info("Starting Provider Search By BPP Flag");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var searchBPP = new SearchBPP(driver, m, system);
                searchBPP.TestSearchBPPFlag();

                #endregion

                #region search by gender
                _result = "** Provider Search By Gender **";
                Logger.Info("Starting Provider Search By Gender");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var searchGender = new SearchGender(driver, m, system);
                searchGender.TestSearchByGender();

                #endregion

                #region search by Distance
                _result = "** Provider Search By Distance **";
                Logger.Info("Starting Provider Search By Distance");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var searchDistance = new SearchDistane(driver, m, system);
                searchDistance.TestSearchByDistance();

                #endregion

                #region search by all filters
                _result = "** Provider Search By All Available Search Filters **";
                Logger.Info("Starting Provider Search By All Available Search Filters");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var allSearchFilters = new AllSearchFilters(driver, m, system);
                allSearchFilters.TestAllSearchFilters();

                #endregion

                mail.SendEMail("Intranet FAP Regression Finished", m.GetBody());
            }
            catch (Exception e)
            {
                Logger.Error("Error occurred " + e.Message);
                driver.CloseDriver();

                _result = "An Exception occurred in Program::Main " + e.Message;
                m.AddToBody(_result + "<br/>");
                mail.SendEMail("Intranet FAP Regression Testing Failed (Exception)", m.GetBody());
                Environment.Exit(0);
                return;
            }

            driver.CloseDriver();
            Environment.Exit(0);
            return;
        }
    }
}
