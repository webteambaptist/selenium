﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace SeleniumPrimecomp.SeleniumModels
{
    public class Driver : SeleniumCommon.Driver
    {
        public Driver() : base()
        {

        }
        public IReadOnlyCollection<IWebElement> GetElementsByClassNameForElement(IWebElement element, string className)
        {
            return element.FindElements(By.ClassName(className));
        }
        public IReadOnlyCollection<IWebElement> GetElementsByClassName(string className)
        {
            return driver.FindElements(By.ClassName(className));
        }
        public IWebElement GetElementByClassNameForElement(IWebElement element, string className)
        {
            return element.FindElement(By.ClassName(className));
        }
        public int GetNumTabs()
        {
            return driver.WindowHandles.Count;
        }
        public string GetTabTitle()
        {
            return driver.Title;
        }
        public void BackPage()
        {
            driver.ExecuteScript("window.history.go(-1)");
        }

        public IWebElement GetDrInfoLeft(IWebElement element)
        {
            var row = element.FindElement(By.ClassName("row"));
            return row.FindElement(By.ClassName("dr-info-left"));
        }
        public IWebElement GetDrInfoRight(IWebElement element)
        {
            var row = element.FindElement(By.ClassName("row"));
            return row.FindElement(By.ClassName("dr-info-right"));
        }
        public IReadOnlyCollection<IWebElement> GetElementsByTagName(IWebElement element, string tagName)
        {
            return element.FindElements(By.TagName(tagName));
        }
        public IReadOnlyCollection<IWebElement> GetElementsByTagName(string tagName)
        {
            return driver.FindElements(By.TagName(tagName));
            //return driver.FindElementsByTagName(tagName);
        }
        public string GetUrl()
        {
            return driver.Url;
        }

        public IWebElement GetSlickList()
        {
            var top = GetElementById("fulltop");
            var libraryRow = GetElementByClassNameForElement(top, "landing-libraries-row");
            var centerBlock = GetElementByClassNameForElement(libraryRow, "center-block");
            var librarySlick = GetElementByClassNameForElement(centerBlock, "library-slick");
            var slickList = GetElementByClassNameForElement(librarySlick, "slick-list");
            return slickList;
        }
        public IWebElement GetElementByTagName(IWebElement element, string tagName)
        {
            return element.FindElement(By.TagName(tagName));
        }
        public IWebElement GetElementByTagName(string tagName)
        {
            return driver.FindElement(By.TagName(tagName));
        }
        public IWebElement GetElementByIdForElement(IWebElement element, string id)
        {
            return element.FindElement(By.Id(id));
        }
        public IReadOnlyCollection<IWebElement> GetElementsByIdForElement(IWebElement element, string id)
        {
            return element.FindElements(By.Id(id));
        }
        public bool IsStale(IWebElement element)
        {
            try
            {
                element.Click();
                return false;
            }
            catch (StaleElementReferenceException)
            {
                return true;
            }
        }
        public void refresh()
        {
            driver.Navigate().Refresh();
        }
        public IWebElement GetInnerNavbar()
        {
            return GetElementByXPath("//div[@class='pp-navbar-inner']");
        }
        public IReadOnlyCollection<IWebElement> GetElementsByXPath(string xPath)
        {
            return driver.FindElements(By.XPath(xPath));
            //return driver.FindElementsByXPath(xPath);
        }
        public IReadOnlyCollection<IWebElement> GetNavBarDropDowns()
        {
            return  GetInnerNavbar().FindElements(By.ClassName("dropdown"));
        }
        public IReadOnlyCollection<IWebElement> GetLinksByElement(IWebElement element)
        {
            return element.FindElements(By.TagName("a"));
        }
        public IReadOnlyCollection<IWebElement> OrderCollection(IReadOnlyCollection<IWebElement> list)
        {
            return list.OrderBy(x => x.FindElement(By.TagName("href"))).ToList();
        }
        public IWebElement GetLinkByElementOrderedByHref(IWebElement element)
        {
            var link = element.FindElement(By.TagName("a"));
            return link;
        }
         public void ClickHeaderLink(IWebElement element)
        {
            element.FindElement(By.ClassName("dropdown-content")).FindElement(By.TagName("a")).Click();
        }
        public IReadOnlyCollection<IWebElement> GetNavIconsLis()
        {
            var element = driver.FindElement(By.ClassName("nav-icons")).FindElements(By.TagName("li"));
            //return driver.FindElementByClassName("nav-icons").FindElements(By.TagName("li"));
            return element;
        }
        public IWebElement GetCenterColumn()
        {
            return driver.FindElement(By.Id("center-column"));
        }
        public IReadOnlyCollection<IWebElement> GetInformaticsProviders()
        {
            return GetCenterColumn().FindElements(By.Id("informatics-provider"));
        }
        public bool IsDisplayedByTagElement(IWebElement element, string tag)
        {
            try
            {
                return element.FindElement(By.TagName(tag)).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            
        }
        public IWebElement GetRightColumn()
        {
            return driver.FindElement(By.Id("right-column"));
        }

        public void ClickByCssSelectorForElement(IWebElement element, string cssSelector)
        {
            element.FindElement(By.CssSelector(cssSelector)).Click();
        }
        public void ClickByTagNameForElement(IWebElement element, string tagName)
        {
            element.FindElement(By.TagName(tagName)).Click();
        }
        public void ClickBTagNameForElement(IWebElement element, string tagName)
        {
            element.FindElement(By.TagName(tagName)).Click();
        }
        public void ClickByCssSelector(string cssSelector)
        {
            driver.FindElement(By.CssSelector(cssSelector)).Click();
        }
        public void ClickFapResetButton()
        {
            GetElementById("reset-filter-btn").Click();
        }
        public IReadOnlyCollection<IWebElement> GetFapResults()
        {
            var element = GetElementByClassName("fap-results");
            return GetElementsByClassNameForElement(element, "fap-result");
        }
        public IWebElement GetFapPagination()
        {
            return GetElementByClassName("pagination");
        }
        public IReadOnlyCollection<IWebElement> GetFormGroups(IWebElement element)
        {
            return element.FindElement(By.ClassName("row")).FindElement(By.ClassName("col-md-12")).FindElements(By.ClassName("form-group"));
        }
        public List<IWebElement> GetQuickLinks(IReadOnlyCollection<IWebElement> divs, string linkName)
        {
            return divs.Where(x =>
                    x.FindElement(By.TagName("li")).FindElement(By.ClassName("addlink-a")).Text == linkName)
                .ToList();
        }
        public IReadOnlyCollection<IWebElement> GetElementsByName(string element)
        {
            return driver.FindElements(By.Name(element));
        }
    }
}
