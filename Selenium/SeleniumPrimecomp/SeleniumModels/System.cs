﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using SeleniumCommon;
using SeleniumPrimecomp.SeleniumModels;
using Driver = SeleniumPrimecomp.SeleniumModels.Driver;

namespace SeleniumPrimecomp.SeleniumModels
{
    public class System 
    {
        private readonly Driver driver;
        private readonly Configs configs;
       // private readonly SendMail m;
        
        public System()
        {
            if (driver != null) return;
            driver = new Driver();
            configs = new Configs();
          //  m = new SendMail("Primecomp Regression Testing", configs.GetMailTo(), configs.GetFrom());
        }
        public Driver GetDriver()
        {
            return driver;
        }
        public Configs GetConfigs()
        {
            return configs;
        }
        //public SendMail GetMail()
        //{
        //    return m;
        //}
        public bool CheckFileDownloaded(string extension)
        {
            try
            {
                Task.Delay(10000).Wait();
                //_driver.ImplicitWait(20);
                var directory = new DirectoryInfo(@"C:\Users\tanth001\Downloads");
                var files = directory.GetFiles();
                if (files.Length == 0) return false;
                var file = files.OrderByDescending(x => x.LastAccessTime).First();
                if (!file.Extension.Equals(extension)) return false;
                file.Delete();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
    }
}
