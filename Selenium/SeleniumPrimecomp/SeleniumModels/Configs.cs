﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace SeleniumPrimecomp.SeleniumModels
{
    public class Configs
    {
        private IConfigurationRoot _config;
        private string mailTo;
        private string from;

        public Configs()
        {
            var directory = Directory.GetCurrentDirectory();
            var builder = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("appsettings.json", true, true);
            _config = builder.Build();
            mailTo = _config.GetSection("GeneralSettings").GetSection("EmailSetting").GetSection("MailTo").Value;
            from = _config.GetSection("GeneralSettings").GetSection("Email").Value;
        }

        public string GetMailTo()
        {
            return mailTo;
        }

        public string GetFrom()
        {
            return from;
        }
    }

}