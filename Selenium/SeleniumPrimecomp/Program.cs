﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Threading;
using Limilabs.Mail.Fluent;
using Microsoft.Extensions.Configuration;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumCommon;
using LogLevel = NLog.LogLevel;
using Driver = SeleniumPrimecomp.SeleniumModels.Driver;
using Mail = SeleniumCommon.Mail;
namespace SeleniumPrimecomp
{
    class Program
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static Driver _driver;
        private static IConfiguration _config;
        private static string _body;
        private static string _result;
        private static Mail m;
        private static string _url;
       // private static WebDriverWait _wait;
        private static string _email;
        private static string _mailTo;
        
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true);

            _config = builder.Build();

            try
            {
                var config = new NLog.Config.LoggingConfiguration();
                var logFile = new NLog.Targets.FileTarget("logFile")
                {
                    FileName = @".\logs\SeleniumPrimecomp.log"
                };

                config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
                LogManager.Configuration = config;

                Logger.Info("Test started...");
                var system = new SeleniumModels.System();
                BuildConfigs();
                _body += "Regression test for site: " + _url + "</br/>";
                Console.WriteLine("Regression test for site: " + _url);
                Logger.Info("Regression test for site: " + _url);
                var success = true;
                var configs = system.GetConfigs();
                _driver = system.GetDriver(); //CreateDriver();
                _driver.MaximizeWindow();//.Manage().Window.Maximize();
                
                Console.WriteLine("Starting Primecomp Regression Testing....");
                Logger.Info("Starting Primecomp Regression Testing....");
                LoadSite();
                success = TestMainPage();

                _result = "------------------------------------<br/>";
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result;

                //if (success)
                //    success = TestPcnNews();

                _result = "------------------------------------<br/>";
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result;

                if (success)
                    success = TestProviders();

                _result = "------------------------------------<br/>";
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result;

                if (success)
                    success = TestToolsAndResources();

                _result = "------------------------------------<br/>";
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result;

                if (success)
                    success = TestEmployersPayors();        

                _result = "------------------------------------<br/>";
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result;

                SendMail("Primecomp Regression Testing");
                _driver.CloseDriver();
                Environment.Exit(0);
            }
            catch (Exception e)
            {
                _result = "------------------------------------<br/>";
                _result += "Exception occurred in Main " + e.Message;
                Console.WriteLine(_result);
                Logger.Error(_result);
                _body += _result;

                SendMail("Primecomp Regression Testing");
                _driver.CloseDriver();
                Environment.Exit(0);
            }

        }

        private static bool TestFap()
        {
            try
            {
                var links = _driver.GetElementsByXPath("//div[@class='container']//div[@class='row']//div[@class='col-md-12 twocolumn']//div[@id='right-column']//div[@class='col-md-12']/div[@class='col-md-12 blocks']//a");
                var link = links.First(x => x.Text=="Find a Doctor");
                var linkblocks = _driver.GetElementByXPathForElement(link, "//div[@class='col-md-4 linksblocks']");
                linkblocks.Click();
                 
                _driver.wait.Until(wait => _driver.IsClassNameDisplayed("fap-results-container"));
                
                _result = "Pass :: TestFap :: FAP Results Loaded-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";

                #region Hospitals
                // Test In Network Hospitals
                var success = HospitalLinks();
                try
                {
                    if (success)
                    {
                        _result = "Pass :: TestFap :: Hospital Tests Passed-- " + DateTime.Now;
                        Console.WriteLine(_result);
                        Logger.Info(_result);
                        _body += _result + "<br/>";
                    }
                    else
                    {
                        _result = "Fail :: TestFap :: Hospital Tests Failed " + DateTime.Now + "<br/>";
                        Console.WriteLine(_result);
                        Logger.Error(_result);
                        _body += _result + "<br/>";
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _result = "Fail :: TestFap :: Exception occurred (Hospital Tests) Failed " + DateTime.Now + "<br/>";
                    _result += e.Message + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
                #endregion
                _driver.GetElementById("reset-filter-btn").Click();

                #region Test Provider Type
                try
                {
                    _driver.wait.Until(wait =>
                    {
                        try
                        {
                            return _driver.IsClassNameDisplayed("show-on-results");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Fail :: TestFap :: No results after reset" + " " + e.Message);
                            Logger.Info("Fail :: TestFap :: No results after reset" + " " + e.Message);
                            _body += _result + "<br/>";
                            return false;
                        }
                    });
                    success = TestProviderType("Ancillary");
                    if (success)
                    {
                        _result = "Pass :: TestFap :: Ancillary Provider Type (Ancillary) -- " + DateTime.Now;
                        Console.WriteLine(_result);
                        Logger.Info(_result);
                        _body += _result + "<br/>";
                    }
                    else
                    {
                        _result = "Fail :: TestFap :: Ancillary Provider Type " + DateTime.Now + "<br/>";
                        Console.WriteLine(_result);
                        Logger.Error(_result);
                        _body += _result + "<br/>";
                        return false;
                    }
                    success = TestProviderType("Physician");
                    if (success)
                    {
                        _result = "Pass :: TestFap :: Physicians Provider Type (Physician)-- " + DateTime.Now;
                        Console.WriteLine(_result);
                        Logger.Info(_result);
                        _body += _result + "<br/>";
                    }
                    else
                    {
                        _result = "Fail :: TestFap :: Physicians Provider Type " + DateTime.Now + "<br/>";
                        Console.WriteLine(_result);
                        Logger.Error(_result);
                        _body += _result + "<br/>";
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _result = "Fail :: TestFap :: Exception (Physicians Provider Type) " + DateTime.Now + "<br/>";
                    _result += e.Message + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
                #endregion
                
                _driver.GetElementById("reset-filter-btn").Click();

                #region Specialty Filter
                try
                {
                    // get drop down for Specialty
                    var specialtyDiv = _driver.GetElementById("specialty");
                    //Console.WriteLine("Specialty Div: <br/>" + specialtyDiv.Text + "<br/>");
                    var specialty = specialtyDiv.FindElement(By.Name("specialties_pipe"));  
                    //Console.WriteLine("Specialty <br/>" + specialty.Text + "<br/>");
                    var selectElement = new SelectElement(specialty);
                    //Console.WriteLine("SelectElement: <br/>" + selectElement.Options + "<br/>");
                    var specialtyConfig = _config.GetSection("FAP").GetSection("Specialty").Value;
                    //Console.WriteLine("Specialty Config: <br/>" + specialtyConfig + "<br/>");
                    try
                    {
                        selectElement.SelectByValue(specialtyConfig);
                    }
                    catch (StaleElementReferenceException e)
                    {
                        Console.WriteLine(e.Message);
                        Logger.Error(e.Message);
                        specialtyDiv = _driver.GetElementById("specialty");
                        specialty = specialtyDiv.FindElement(By.Name("specialties_pipe"));
                        selectElement = new SelectElement(specialty);
                        selectElement.SelectByValue(specialtyConfig);
                    }
                    
                    Console.WriteLine("Specialty selected successfully..... ");
                    Logger.Info("Specialty selected successfully..... ");
                    _driver.wait.Until(wait =>
                    {
                        try
                        {
                            return _driver.IsClassNameDisplayed("fap-results");
                        }
                        catch (Exception)
                        {
                            _result = "Fail :: TestFap :: Exception looking for FAP results (specialty filter) " + DateTime.Now + "<br/>";
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result + "<br/>";
                            return false;
                        }
                    });
                    // verify all _results are specialty selected
                    var results = _driver.GetElementsByClassName("fap-results");
                    var match = true;
                    foreach (var result in results)
                    {
                        try
                        {
                            var drSpec = result.FindElement(By.ClassName("dr-specialty")).Text;
                            if (drSpec != specialtyConfig)
                            {
                                match = false;
                            }
                        }
                        catch (StaleElementReferenceException e)
                        {
                            Console.WriteLine(e.Message);
                            // refresh results
                            results = _driver.GetElementsByClassName("fap-results");
                            var drSpec = result.FindElement(By.ClassName("dr-specialty")).Text;
                            if (drSpec != specialtyConfig)
                            {
                                match = false;
                            }
                        }
                    }
                    if (!match)
                    {
                        _result = "******************************************************** <br/>";
                        _result += "Fail: FAP Specialty Filter Failed: Non Specialty results. <br/>";
                        _result += "******************************************************** <br/>";
                        Console.WriteLine(_result);
                        Logger.Error(_result);
                        _body += _result += "<br/>";
                    }
                    else
                    {
                        _result = "Pass: FAP Specialty Filter Test successful ";
                        Console.WriteLine(_result);
                        Logger.Info(_result);
                        _body += _result + "<br/>";
                    }
                }
                catch (Exception e)
                {
                    _result = "Fail :: TestFap :: Exception (Specialty) " + DateTime.Now + "<br/>";
                    _result += e.Message + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
                #endregion

                _driver.GetElementById("reset-filter-btn").Click();

                #region Gender
                try
                {
                    // get list of radio options
                    var gender = _driver.GetElementById("gender");
                    var gen = _config.GetSection("FAP").GetSection("Gender").Value;
                    gender.FindElement(By.CssSelector("input[value='" + gen + "']")).Click();

                    // check to make sure all _results are females
                    _driver.wait.Until(wait =>
                    {
                        try
                        {
                            return _driver.IsClassNameDisplayed("fap-results");
                        }
                        catch (Exception)
                        {
                            _result = "Fail :: TestFap :: Exception looking for FAP results (Gender filter) " +
                                      DateTime.Now + "<br/>";
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result + "<br/>";
                            return false;
                        }
                    });
                    var results = _driver.GetElementsByClassName("fap-results");
                    var match = true;
                    foreach (var result in results)
                    {
                        var genderElement = result.FindElement(By.ClassName("dr-gender")).Text;
                        if (genderElement != gen)
                        {
                            match = false;
                        }

                    }

                    if (!match)
                    {
                        _result = "******************************************************** <br/>";
                        _result +=
                            "Fail: FAP Gender Filter Failed: More than selected Gender showed in _results. <br/>";
                        _result += "******************************************************** <br/>";
                        Console.WriteLine(_result);
                        Logger.Error(_result);
                        _body += _result += "<br/>";
                    }
                    else
                    {
                        _result = "Pass: FAP Gender Filter Test successful ";
                        Console.WriteLine(_result);
                        Logger.Info(_result);
                        _body += _result + "<br/>";
                    }
                }
                catch (Exception e)
                {
                    _result = "Fail :: TestFap :: Exception (Gender) " + DateTime.Now + "<br/>";
                    _result += e.Message + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
                #endregion
                    
                _driver.GetElementById("reset-filter-btn").Click();

                #region Language Filter
                try
                {
                    Thread.Sleep(5000);
                    var languages = _driver.GetElementByName("languages_pipe");
                    var selectElement = new SelectElement(languages);
                    var language = _config.GetSection("FAP").GetSection("Language").Value;
                    selectElement.SelectByValue(language);
                    
                    // verify all _results are specialty selected
                    _driver.wait.Until(wait =>
                    {
                        try
                        {
                            return _driver.IsClassNameDisplayed("fap-results");
                        }
                        catch (Exception)
                        {
                            _result = "Fail :: TestFap :: Exception looking for FAP results (Language filter) " + DateTime.Now + "<br/>";
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result + "<br/>";
                            return false;
                        }
                    });
                    var results = _driver.GetElementsByClassName("fap-results");
                    var match = true;
                    foreach (var result in results)
                    {
                        var drLanguage = _driver.GetElementByClassNameForElement(result, "dr-languages");
                        if (!drLanguage.Text.Contains(language))
                        {
                            match = false;
                        }

                    }
                    if (!match)
                    {
                        _result = "******************************************************** <br/>";
                        _result += "Fail: FAP Language Filter Failed: Non Language _results. <br/>";
                        _result += "******************************************************** <br/>";
                        Console.WriteLine(_result);
                        Logger.Error(_result);
                        _body += _result += "<br/>";
                    }
                    else
                    {
                        _result = "Pass: FAP Language Filter Test successful ";
                        Console.WriteLine(_result);
                        Logger.Info(_result);
                        _body += _result + "<br/>";
                    }
                }
                catch (Exception e)
                {
                    _result = "Fail :: TestFap :: Exception (Language) " + DateTime.Now + "<br/>";
                    _result += e.Message + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
                #endregion

                _driver.GetElementById("reset-filter-btn").Click();
                
                #region Location Filter
                try
                {
                    _driver.ClickByCssSelector("input[name='location-filter']");
                    //_driver.FindElement(By.CssSelector("input[name='location-filter']")).Click();

                    if (_driver.IsClassNameDisplayed("location-controls"))
                    {
                        var dist = _driver.GetElementByName("distance");
                        var selectElement = new SelectElement(dist);
                        var miles = _config.GetSection("FAP").GetSection("Location").GetSection("Miles").Value;
                        selectElement.SelectByValue(miles);

                        var zip = _config.GetSection("FAP").GetSection("Location").GetSection("Zip").Value;
                        _driver.SendKeysById("zip", zip);//.SendKeys(zip);
                        _driver.ClickById("apply-zip-btn");

                        Console.WriteLine("Pausing for _results.... ");
                        Logger.Info("Pausing for _results....");
                        Thread.Sleep(5000);
                        _driver.wait.Until(wait =>
                        {
                            try
                            {
                                var theme  = _driver.GetElementByXPath("//div[@id='container']//div[@class='container']//div[@class='row']//div[@class='col-md-12']//div[@class='container bg-white theme-wrap']");
                                try
                                {
                                    var query = "//div[contains(@class, 'submit-progress hidden')]";

                                    var bar = _driver.GetElementsByXPathForElement(theme, query);
                                    return bar.Count != 0;
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                    Logger.Error(e.Message);
                                    return false;
                                }
                            }
                            catch (Exception e)
                            {
                                _result = "Fail :: TestFap :: Exception looking for FAP results (Location filter) " + DateTime.Now + "<br/>";
                                Console.WriteLine(_result);
                                Logger.Error(_result + " " + e.Message);
                                _body += _result + "<br/>";
                                return false;
                            }
                        });

                        // loop through and make sure the miles are <= 5 milese
                        // verify all _results are specialty selected
                        var results = _driver.GetElementsByClassName("fap-results");
                        var match = (from result in results select result.FindElement(By.TagName("li")) into li select li.FindElement(By.ClassName("row")) into row select row.FindElement(By.XPath("//div[@class='col-xs-4 col-xs-pull-8 col-sm-8 col-sm-push-4 col-md-push-0 col-md-pull-0 col-md-2']")) into mile select mile.FindElement(By.ClassName("distance-value")) into distance select distance.Text into value select value.Substring(0, value.Length - 2) into value select value.Trim()).Any(value => float.Parse(value) > int.Parse(miles));
                        if (match)
                        {
                            _result = "******************************************************** <br/>";
                            _result += "Fail: FAP Location Filter Failed. <br/>";
                            _result += "******************************************************** <br/>";
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result += "<br/>";
                        }
                        else
                        {
                            _result = "Pass: FAP Location Filter Test successful ";
                            Console.WriteLine(_result);
                            Logger.Info(_result);
                            _body += _result + "<br/>";
                        }
                    }
                }
                catch (Exception e)
                {
                    _result = "Fail :: TestFap :: Exception (Location) " + DateTime.Now + "<br/>";
                    _result += e.Message + "<br/>";
                    Console.WriteLine(_result);
                    _body += _result + "<br/>";
                    return false;
                }
                #endregion
            }
            catch (Exception e)
            {
                _result = "Fail :: TestFap :: Exception in FAP Test " + DateTime.Now + "<br/>";
                _result += e.Message + "<br/>";
                Console.WriteLine(_result);
                _body += _result + "<br/>";
                return false;
            }

            _result = "Pass :: TestFap :: Successful-- " + DateTime.Now;
            Console.WriteLine(_result);
            _body += _result + "<br/>";
            return true;
        }

        private static bool TestProviderType(string providerType)
        {
            // get drop down for ProviderType
            var provider = _driver.GetElementByName("providertype_s");
            var selectElement = new SelectElement(provider);
            selectElement.SelectByValue(providerType);

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                // verify all results are provider Type select4ed
                var results = _driver.GetElementsByClassName("fap-results");
                
                // test the Practice URL
                var practiceUrl = providerType switch
                {
                    "Ancillary" => results.First().FindElement(By.ClassName("dr-practice-name")),
                    "Physician" => results.First().FindElement(By.ClassName("dr-name")),
                    _ => null
                };
                if (practiceUrl!=null)
                {
                    _result = "Pass :: TestFap :: " + providerType + " Practice Url Found-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";

                    try
                    {
                        practiceUrl.Click();
                    }
                    catch (StaleElementReferenceException e)
                    {
                        Console.WriteLine(e.Message);
                        Logger.Error(e.Message);
                        results = _driver.GetElementsByClassName("fap-results");
                        practiceUrl = providerType switch
                        {
                            "Ancillary" => results.First().FindElement(By.ClassName("dr-practice-name")),
                            "Physician" => results.First().FindElement(By.ClassName("dr-name")),
                            _ => null
                        };
                        practiceUrl?.Click();
                    }
                    

                    //var tabs = _driver.WindowHandles;
                    var originalTab = _driver.SwitchNewTabs();
                    //_driver.SwitchTo().Window(tabs[1]);

;                    _driver.wait.Until(wait =>
                    {
                        try
                        {
                            return _driver.IsIdDisplayed("physician_details");
                        }
                        catch (Exception e)
                        {
                            _result = "Fail :: TestFap :: Exception occurred looking for Physician Details " + DateTime.Now + "<br/>";
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result + "<br/>";
                            return false;
                        }
                    });
                    _result = "Pass :: TestFap :: " + providerType + " Practice Profile Displayed -- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";

                    _driver.CloseDriver();
                    _driver.SwitchBackTabs(originalTab);
                    return true;
                }
                else
                {
                    // this is an error
                    _result = "Fail :: TestFap :: Provider Type Filter " + providerType + " Practice URL not found " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
        }
        private static bool HospitalLinks()
        {
            try
            {
                #region Baptist Clay Emergency Center
                
                var hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(0).Click();
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsDisplayedByTagElement(_driver.GetElementByClassName("physician-name"), "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("Baptist Emergency at Clay");
                
                var backLink = _driver.GetElementByClassName("go-back");
                var li = _driver.GetElementByTagName(backLink, "li");
                _driver.ClickBTagNameForElement(li, "a");
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: Baptist Emergency at Clay Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region Baptist Emergency at Town Center

                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(1).Click();
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsDisplayedByTagElement(_driver.GetElementByClassName("physician-name"), "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("Baptist Emergency at Town Center");

                backLink = _driver.GetElementByClassName("go-back")
                    .FindElement(By.TagName("li")).FindElement(By.TagName("a"));

                backLink.Click();  
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: Baptist Emergency at Town Center Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region Baptist Emergency at North
                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(2).Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var name = _driver.GetElementByClassName("physician-name");
                        return _driver.IsDisplayedByTagElement(name, "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("Baptist Emergency at North");
                var goBack = _driver.GetElementByClassName("go-back");
                var backLi = _driver.GetElementByTagName(goBack, "li");
                _driver.ClickByTagNameForElement(backLi, "a"); //backLink = _driver.FindElementByClassName("go-back")
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: Baptist Emergency at North Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region Baptist Medical Center Jacksonville

                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(3).Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var physicianName = _driver.GetElementByClassName("physician-name");
                        return _driver.IsDisplayedByTagElement(physicianName, "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("Baptist Medical Center Jacksonville");

                var back = _driver.GetElementByClassName("go-back");
                var jaxLi = _driver.GetElementByTagName(back, "li");
                _driver.ClickByTagNameForElement(jaxLi, "a");
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: Baptist Medical Center Jacksonville Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region Baptist Medical Center Beaches
                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(4).Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var name = _driver.GetElementByClassName("physician-name");
                        return _driver.IsDisplayedByTagElement(name, "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("Baptist Medical Center Beaches");
                var beachesBack = _driver.GetElementByClassName("go-back");
                var beachesLi = _driver.GetElementByTagName(beachesBack, "li");
                _driver.ClickByTagNameForElement(beachesLi, "a");
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: Baptist Medical Center Beaches Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region Baptist Medical Center Nassau

                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(5).Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var name = _driver.GetElementByClassName("physician-name");
                        return _driver.IsDisplayedByTagElement(name, "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("Baptist Medical Center Nassau");

                var nassauBack = _driver.GetElementByClassName("go-back");
                var nassauLi = _driver.GetElementByTagName(nassauBack, "li");
                _driver.ClickByTagNameForElement(nassauLi, "a");
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: Baptist Medical Center Nassau Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region Baptist Medical Center South

                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(6).Click();
                
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var name = _driver.GetElementByClassName("physician-name");
                        return _driver.IsDisplayedByTagElement(name, "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("Baptist Medical Center South");
                var southBack = _driver.GetElementByClassName("go-back");
                var southLi = _driver.GetElementByTagName(southBack, "li");
                _driver.ClickBTagNameForElement(southLi, "a");
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: Baptist Medical Center South Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region Flagler Hospital
                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                var link = hospitalLinks.First(x => x.Text == "Flagler Hospital");
                var clicker = _driver.GetElementByTagName(link, "a");// link.FindElement(By.TagName("a"));
                clicker.Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var name = _driver.GetElementByClassName("physician-name");
                        return _driver.IsDisplayedByTagElement(name, "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("Flagler Hospital");

                var flaglerBack = _driver.GetElementByClassName("go-back");
                var flaglerLi = _driver.GetElementByTagName(flaglerBack, "li");
                _driver.ClickBTagNameForElement(flaglerLi, "a");
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: Flagler Hospital Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region St. Vincent’s Medical Center Riverside
                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(8).Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var name = _driver.GetElementByClassName("physician-name");
                        return _driver.IsDisplayedByTagElement(name, "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("St. Vincent’s Medical Center Riverside");
                var riversideBack = _driver.GetElementByClassName("go-back");
                var riversideLi = _driver.GetElementByTagName(riversideBack, "li");
                _driver.ClickByTagNameForElement(riversideLi, "a");
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: St. Vincent’s Medical Center Riverside Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                #endregion
                #region St. Vincent’s Medical Center Southside

                hospitalLinks = _driver.GetElementsByClassName("hospitalsright");
                hospitalLinks.ElementAt(9).Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var name = _driver.GetElementByClassName("physician-name");
                        return _driver.IsDisplayedByTagElement(name, "h1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                DoesHospitalMatch("St. Vincent’s Medical Center Southside");
                var southsideBack = _driver.GetElementByClassName("go-back");
                var southsideLi = _driver.GetElementByTagName(southsideBack, "li");
                _driver.ClickBTagNameForElement(southsideLi, "a");
                
                _driver.wait.Until(wait =>
                {
                    try
                    {
                        return _driver.IsClassNameDisplayed("fap-results-container");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                _result = "Pass :: HospitalLinks :: St. Vincent’s Medical Center Southside Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";

                #endregion

                _result = "Pass :: HospitalLinks :: Success-- " + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
                return true;
            }
            catch (Exception e)
            {
                _result = "FAIL :: TestFap :: HospitalLinks :: Exception --" + DateTime.Now + "<br/>";
                _result += e.Message;
                Console.WriteLine(_result);
                Logger.Error(_result);
                _body += _result + "<br/>";
                return false;
            }
            
        }
        private static void DoesHospitalMatch(string hospitalName)
        {
            var name = _driver.GetElementByClassName("physician-name");
            var heading = _driver.GetElementByTagName(name, "h1").Text;
            if (heading.Equals(hospitalName))
            {
                _result = "Pass :: TestFap :: Hospital " + hospitalName + "("+ heading+")" + " link opened successfully --" + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Info(_result);
                _body += _result + "<br/>";
            }
            else
            {
                _result = "FAIL :: TestFap :: Hospital " + hospitalName + "("+ heading+")" + " link did not opened successfully --" + DateTime.Now;
                Console.WriteLine(_result);
                Logger.Error(_result);
                _body += _result + "<br/>";
            }
        }
        private static bool TestProviders()
        {
            try
            {
                var menu = GetTopMenuLinks();
                var menuIndexOne = menu.ElementAt(1);
                var li = _driver.GetElementByClassNameForElement(menuIndexOne, "dropbtn");
                var links = li.FindElements(By.TagName("a"));
                links.First().Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var displayed = _driver.IsIdDisplayed("content-left-column");
                        return displayed;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                   
                });

                var content = _driver.GetElementById("content-left-column");
                var providers = _driver.GetElementByClassNameForElement(content, "providers");
                var leftColumn = _driver.GetElementsByClassNameForElement(providers, "col-md-12");
                var heading = leftColumn.ElementAt(0);
                var providerHeading = _driver.GetElementByTagName(heading, "h2").Text;
                if (providerHeading.Equals("Our Providers"))
                {
                    _result = "Pass :: TestProviders :: Provider Heading Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestProviders :: Provider Heading not correct " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                    
                }
                var providerDetailsExist = leftColumn.ElementAt(0).FindElement(By.TagName("p")).Displayed;
                if (providerDetailsExist)
                {
                    _result = "Pass :: TestProviders :: Provider Heading Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                { 
                    _result = "Fail :: TestProviders :: Provider Details not displayed " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }

                var whyJoin = leftColumn.ElementAt(1).FindElement(By.TagName("h2")).Text;
                if (whyJoin.Equals("Why Join?"))
                {
                    _result = "Pass :: TestProviders :: Provider Why Join Header Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestProviders :: Provider Why Join Header not Displayed " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }

                var paragraphs = leftColumn.ElementAt(1).FindElements(By.TagName("p"));
                var textExist = false;
                foreach (var text in paragraphs)
                {
                    if (text.Text.Length > 0)
                        textExist = true;
                    if (textExist)
                        break;
                }

                if (textExist)
                {
                    _result = "Pass :: TestProviders :: Provider Why Join Text Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestProviders :: Provider Why Join Text not Displayed " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
                
                var success = TestFap();
                if (success)
                {
                    _result = "Pass :: TestProviders :: FAP Successful-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestProviders :: FAP Failed " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
            }
            catch (Exception e)
            {
                _result = "Fail :: TestProviders :: Exception -- " + DateTime.Now + "<br/>";
                _result += e.Message + "<br/>";
                Console.WriteLine(_result);
                Logger.Error(_result);
                _body += _result + "<br/>";
                return false;
            }

            _result = "Pass :: TestProviders :: Completed -- " + DateTime.Now;
            Console.WriteLine(_result);
            Logger.Info(_result);
            _body += _result + "<br/>";
            return true;
        }
        private static bool TestToolsAndResources()
        {
            try
            {
                var menu = GetTopMenuLinks();
                var menuIndexTwo = menu.ElementAt(2);
                var li = _driver.GetElementByClassNameForElement(menuIndexTwo, "dropbtn");
                var links = _driver.GetElementsByTagName(li, "a");
                
                links.First().Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var displayed = _driver.IsIdDisplayed("content-left-column"); //_driver.FindElementById("content-left-column").Displayed;
                        return displayed;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }

                });

                #region General Information
                var content = _driver.GetElementById("content-left-column").FindElements(By.ClassName("col-md-12"))[1];

                var contentHeading1 = content.FindElement(By.TagName("h2")).Text;
                if (contentHeading1.Equals("General Information"))
                {
                    _result = "Pass :: TestToolsAndResources :: Tools and Resources General Information Heading Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestToolsAndResources :: Tools and Resources General Information Heading not correct " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
                // get links
                var subLinks = _driver.GetElementsByTagName(content, "p");
                foreach (var subLink in subLinks)
                {
                    if (subLink.Text.Length <= 0) continue;
                    _driver.ClickByLinkText(subLink.Text);//.Click();

                    _result = "Pass :: TestToolsAndResources :: SubLink Opened Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                    var originalTab = _driver.SwitchNewTabs();
                    _driver.SwitchBackTabs(originalTab);
                }
                #endregion   
                
                #region Provider Tools & Resources

                //var content2 = _driver.GetElementsByClassName("col-md-12").ElementAt(2);
                var content2 = _driver.GetElementById("content-left-column").FindElements(By.ClassName("col-md-12"))[2];
                var contentHeading2 = _driver.GetElementByTagName(content2, "h2").Text;// content2.FindElement(By.TagName("h2")).Text;
                if (contentHeading2.Equals("Provider Tools & Resources"))
                {
                    _result = "Pass :: TestToolsAndResources :: Tools and Resources Provider Tool & Resources Heading Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestToolsAndResources :: Tools and Resources Provider Tool & Resources Heading not correct " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }

                var subLinks2 = _driver.GetElementsByTagName(content2, "p");// content2.FindElements(By.TagName("p"));
                foreach (var subLink in subLinks2)
                {
                    if (subLink.Text.Length <= 0) continue;
                    _driver.ClickByLinkText(subLink.Text);
                    Thread.Sleep(3000);
                    var index = _driver.GetNumTabs();

                    if (index <= 1) continue;
                    _result = "Pass :: TestToolsAndResources :: SubLink Opened Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                    var originalTabInstance = _driver.SwitchNewTabs();//_driver.CurrentWindowHandle;
                    _driver.ExecuteJs("window.close();");
                    _driver.SwitchBackTabs(originalTabInstance);
                }
                #endregion

                #region Employers & Payors Tools & Resources

                var leftColumnRes = _driver.GetElementById("content-left-column");
                var content3 = _driver.GetElementsByClassNameForElement(leftColumnRes, "col-md-12").ElementAt(3);

                var contentHeading3 = _driver.GetElementByTagName(content3, "h2").Text;// content3.FindElement(By.TagName("h2")).Text;
                if (contentHeading3.Equals("Employers & Payors Tools & Resources"))
                {
                    _result = "Pass :: TestToolsAndResources :: Tools and Resources Employers & Payors Tools & Resources Heading Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestToolsAndResources :: Tools and Resources Employers & Payors Tools & Resources Heading not correct " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }

                var subLinks3 = content3.FindElements(By.TagName("p"));
                foreach (var subLink in subLinks3)
                {
                    if (subLink.Text.Length <= 0) continue;
                    _driver.ClickByLinkText(subLink.Text);


                    _result = "Pass :: TestToolsAndResources :: SubLink Opened Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";

                    var originalTabInstance = _driver.SwitchNewTabs();//_driver.CurrentWindowHandle;
                    _driver.ExecuteJs("window.close();");
                    _driver.SwitchBackTabs(originalTabInstance);
                }
                #endregion
            }
            catch (Exception e)
            {
                _result = "Fail :: TestToolsAndResources :: Exception -- " + DateTime.Now + "<br/>";
                _result += e.Message + "<br/>";
                Console.WriteLine(_result);
                Logger.Error(_result);
                _body += _result + "<br/>";
                return false;
            }

            _result = "Pass :: TestToolsAndResources :: Completed -- " + DateTime.Now;
            Console.WriteLine(_result);
            Logger.Info(_result);
            _body += _result + "<br/>";
            return true;
        }
        private static bool TestEmployersPayors()
        {
            try
            {
                var menu = GetTopMenuLinks();
                var menuElementThree = menu.ElementAt(3);

                var li = _driver.GetElementByClassNameForElement(menuElementThree, "dropbtn");
                var links = _driver.GetElementsByTagName(li, "a");
                links.ElementAt(0).Click();

                _driver.wait.Until(wait =>
                {
                    try
                    {
                        var displayed = _driver.IsIdDisplayed("content-left-column");
                        return displayed;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }

                });
                var contentLeft = _driver.GetElementById("content-left-column");
                var providers = _driver.GetElementByClassNameForElement(contentLeft, "providers");
                var leftColumn = _driver.GetElementsByClassNameForElement(providers, "col-md-12");
                var colIndexZero = leftColumn.ElementAt(0);
                var heading1 = _driver.GetElementByTagName(colIndexZero, "h2").Text;
                if (heading1.Equals("Helping Payors & Local Employers"))
                {
                    _result = "Pass :: TestEmployersPayors :: Employers Payors Helping Payors & Local Employers Heading Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestEmployersPayors :: Employers Payors Helping Payors & Local Employers Heading not correct " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;

                }

                var colIndexOne = leftColumn.ElementAt(1);
                var paragraphs = _driver.GetElementsByTagName(colIndexOne, "p");
                var textExist = false;
                foreach (var text in paragraphs)
                {
                    if (text.Text.Length > 0)
                        textExist = true;
                    if (textExist)
                        break;
                }

                if (textExist)
                {
                    _result = "Pass :: TestEmployersPayors :: Employers Payors Helping Payors & Local Employers Text Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestEmployersPayors :: Employers Payors Helping Payors & Local Employers Text not Displayed " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }

                var heading2 = _driver.GetElementByTagName(colIndexOne, "h2").Text;
                if (heading2.Equals("Why switch to Prime Comp Network?"))
                {
                    _result = "Pass :: TestEmployersPayors :: Employers Payors Why Switch to Prime Comp Network Heading Displayed-- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestEmployersPayors :: Employers Payors Why Switch to Prime Comp Network Heading not correct " + DateTime.Now + "<br/>";
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;

                }

                var paragraphs2 = _driver.GetElementsByTagName(colIndexZero, "p");
                var textExist2 = false;
                foreach (var text in paragraphs2)
                {
                    if (text.Text.Length > 0)
                        textExist2 = true;
                    if (textExist2)
                        break;
                }

            }
            catch (Exception e)
            {
                _result = "Fail :: TestEmployersPayors :: Exception -- " + DateTime.Now + "<br/>";
                _result += e.Message + "<br/>";
                Console.WriteLine(_result);
                Logger.Error(_result);
                _body += _result + "<br/>";
                return false;
            }

            _result = "Pass :: TestEmployersPayors :: Completed -- " + DateTime.Now;
            Console.WriteLine(_result);
            Logger.Info(_result);
            _body += _result + "<br/>";
            return true;
        }
        //private static bool TestPcnNews()
        //{
        //    try
        //    {
        //        LoadPcnNews();
        //        var readMore = _driver.FindElementsByClassName("readmore");

        //        var link = readMore.First().FindElement(By.TagName("a"));
        //        var linkName = link.Text;
        //        link.Click();

        //        _wait.Until(wait =>
        //        {
        //            try
        //            {
        //                var displayed = _driver.FindElementByClassName("halfLeft").Displayed;
        //                return displayed;
        //            }
        //            catch (Exception e)
        //            {
        //                _result = "Fail :: TestPcnNews :: Wait -- " + DateTime.Now;
        //                Console.WriteLine(_result);
        //                _body += _result + "<br/>";
        //                return false;
        //            }
        //        });

        //        var pageDisplayed = PcnPageDisplayed(linkName);

        //        if (pageDisplayed)
        //        {
        //            _result = "Pass :: TestPcnNews :: Page Displayed-- " + DateTime.Now;
        //            Console.WriteLine(_result);
        //            _body += _result + "<br/>";
        //        }
        //        LoadPcnNews();
        //        if (_driver.FindElementByClassName("pagination").Displayed)
        //        {
        //            // Testing Pagination
        //            var links = _driver.FindElementByClassName("pagination").FindElements(By.TagName("a"));
        //            // click page 2
        //            if (links.Count > 0)
        //            {
        //                var linkText = links.First().Text;
        //                links.First().Click();

        //                var displayed = _driver.FindElementById("XB").Displayed;

        //                if (displayed)
        //                {
        //                    _result = "Pass :: TestPcnNews :: Pagination(Page Displayed)-- " + DateTime.Now;
        //                    Console.WriteLine(_result);
        //                    _body += _result + "<br/>";
        //                }
        //                _result = "Pass :: TestPcnNews :: Pagination (There are links)-- " + DateTime.Now;
        //                Console.WriteLine(_result);
        //                _body += _result + "<br/>";
        //            }
        //            else
        //            {
        //                _result = "Pass :: TestPcnNews :: Pagination (No links)-- " + DateTime.Now;
        //                Console.WriteLine(_result);
        //                _body += _result + "<br/>";
        //            }
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        _result = "Fail :: TestPcnNews :: Exception -- " + DateTime.Now + "<br/>";
        //        _result += e.Message + "<br/>";
        //        Console.WriteLine(_result);
        //        _body += _result + "<br/>";
        //        return false;
        //    }

        //    _result = "Pass :: TestPcnNews :: Completed -- " + DateTime.Now;
        //    Console.WriteLine(_result);
        //    _body += _result + "<br/>";
        //    return true;
        //}
        //private static bool PcnPageDisplayed(string linkName)
        //{
        //    try
        //    {
        //        if (_driver.FindElementByXPath("//div[@itemprop='headline']").Displayed
        //            && _driver.FindElementByClassName("halfLeft").Displayed
        //            && _driver.FindElementByXPath("//div[@itemprop='articleBody']").Displayed
        //            && _driver.FindElementByClassName("halfRight").Displayed)
        //        {
        //            _result = "Pass :: TestPcnNews :: PcnPageDisplayed -- " + DateTime.Now + "<br/>";
        //            _result += linkName + "<br/>";
        //            Console.WriteLine(_result);
        //            _body += _result + "<br/>";
        //            return true;
        //        }

        //        _result = "Fail :: TestPcnNews :: PcnPageDisplayed -- " + DateTime.Now + "<br/>";
        //        _result += linkName + "<br/>";
        //        Console.WriteLine(_result);
        //        _body += _result + "<br/>";
        //        return false;
        //    }
        //    catch (Exception e)
        //    {
        //        _result = "Fail :: TestPcnNews :: PcnPageDisplayed EXCEPTION -- " + DateTime.Now + "<br/>";
        //        _result += e.Message + "<br/>";
        //        Console.WriteLine(_result);
        //        _body += _result + "<br/>";
        //        return false;
        //    }

        //}
        //private static void LoadPcnNews()
        //{
        //    var menu = GetTopMenuLinks();
        //    var li = menu[1].FindElement(By.ClassName("dropbtn"));
        //    var links = li.FindElements(By.TagName("a"));
        //    links.First().Click();

        //    _wait.Until(wait =>
        //    {
        //        try
        //        {
        //            var displayed = _driver.FindElementById("XB").Displayed;
        //            return displayed;
        //        }
        //        catch (Exception)
        //        {
        //            _result = "Fail :: LoadPcnNews :: Wait -- " + DateTime.Now;
        //            Console.WriteLine(_result);
        //            _body += _result + "<br/>";
        //            return false;
        //        }
        //    });
        //}
        private static IReadOnlyCollection<IWebElement> GetTopMenuLinks()
        {
            var topNav = _driver.GetElementByClassName("top-nav");
            var navDivs = _driver.GetElementsByClassNameForElement(topNav, "dropdown");
            return navDivs;
        }
        private static void SendMail(string subject)
        {
            var mail = new SendMail();
            mail.SendEMail(subject, _body);
        }
        private static bool TestMainPage()
        {
            _result = "TestMainPage";
            Console.WriteLine(_result);
            Logger.Info(_result);
            _body += _result + "<br/>";

            try
            {
                _driver.wait.Until(webDriver =>
                {
                    try
                    {
                        var isDisplayed = _driver.IsClassNameDisplayed("logo");
                        return isDisplayed;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                });
                    var div = _driver.GetElementByXPath(
                        "//div[@id='container']//header[@class='container-fluid no-padding']//div[@id='myCarousel']");
                if (div.Displayed)
                {
                    _result = "Pass :: TestMainPage :: Carousel Displayed -- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                }
                else
                {
                    _result = "Fail :: TestMainPage :: Carousel Not Displayed -- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }

                var homefont = _driver.IsClassNameDisplayed("homefont");
                if (homefont)
                {
                    _result = "Pass :: TestMainPage :: Homefont Displayed -- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Info(_result);
                    _body += _result + "<br/>";
                    var content = _driver.GetElementByClassNameForElement(_driver.GetElementByClassName("homefont"), "space-after-content");
                    var columns = _driver.GetElementsByClassNameForElement(content, "col-md-4");
                    // are there 3 columns
                    if (columns.Count == 3)
                    {
                        _result = "Pass :: TestMainPage :: There are 3 columns displayed -- " + DateTime.Now;
                        Console.WriteLine(_result);
                        Logger.Info(_result);
                        _body += _result + "<br/>";
                    }
                    else
                    {
                        _result = "Fail :: TestMainPage :: There aren't 3 columns Displayed -- " + DateTime.Now;
                        Console.WriteLine(_result);
                        Logger.Error(_result);
                        _body += _result + "<br/>";
                        return false;
                    }
                    foreach (var column in columns)
                    {
                        // does the photo exist?
                        if (column.FindElement(By.ClassName("doc-img")).Displayed)
                        {
                            _result = "Pass :: TestMainPage :: Column Photo displayed -- " + DateTime.Now;
                            Console.WriteLine(_result);
                            Logger.Info(_result);
                            _body += _result + "<br/>";
                        }
                        else
                        {
                            _result = "Fail :: TestMainPage :: Column Photo not Displayed -- " + DateTime.Now;
                            Console.WriteLine(_result);
                            Logger.Info(_result);
                            _body += _result + "<br/>";
                            return false;
                        }
                        // does content exist
                        if (column.FindElement(By.ClassName("doc-content")).Displayed)
                        {
                            _result = "Pass :: TestMainPage :: Column Content displayed -- " + DateTime.Now;
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result + "<br/>";
                        }
                        else
                        {
                            _result = "Fail :: TestMainPage :: Column Content not Displayed -- " + DateTime.Now;
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result + "<br/>";
                            return false;
                        }
                    }

                    // login box displayed?
                    var rightColumn = _driver.GetElementById("right-column");
                    if (rightColumn.Displayed)
                    {
                        if (rightColumn.FindElement(By.ClassName("col-md-12")).Displayed)
                        {
                            _result = "Pass :: TestMainPage :: Login displayed -- " + DateTime.Now;
                            Console.WriteLine(_result);
                            Logger.Info(_result);
                            _body += _result + "<br/>";
                        }
                        else
                        {
                            _result = "Fail :: TestMainPage :: Login not Displayed -- " + DateTime.Now;
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result + "<br/>";
                            return false;
                        }

                        if (rightColumn.FindElement(By.ClassName("missionstatement")).Displayed)
                        {
                            _result = "Pass :: TestMainPage :: Mission Statement displayed -- " + DateTime.Now;
                            Console.WriteLine(_result);
                            Logger.Info(_result);
                            _body += _result + "<br/>";
                        }
                        else
                        {
                            _result = "Fail :: TestMainPage :: Mission Statement not Displayed -- " + DateTime.Now;
                            Console.WriteLine(_result);
                            Logger.Error(_result);
                            _body += _result + "<br/>";
                            return false;
                        }
                    }
                    // test footer
                    var footer = _driver.IsClassNameDisplayed("footer");
                    if (footer)
                    {
                        _result = "Pass :: TestMainPage :: Footer displayed -- " + DateTime.Now;
                        Console.WriteLine(_result);
                        Logger.Info(_result);
                        _body += _result + "<br/>";
                    }
                    else
                    {
                        _result = "Fail :: TestMainPage :: Footer not Displayed -- " + DateTime.Now;
                        Console.WriteLine(_result);
                        Logger.Error(_result);
                        _body += _result + "<br/>";
                        return false;
                    }
                }
                else
                {
                    _result = "Fail :: TestMainPage :: Homefont Not Displayed -- " + DateTime.Now;
                    Console.WriteLine(_result);
                    Logger.Error(_result);
                    _body += _result + "<br/>";
                    return false;
                }
            }
            catch (Exception e)
            {
                _result = "Fail :: TestMainPage :: Exception -- " + DateTime.Now + "<br/>";
                _result += e.Message + "<br/>";
                Console.WriteLine(_result);
                Logger.Error(_result);
                _body += _result + "<br/>";
                return false;
            }
            
            _result = "Pass :: TestMainPage :: Test completed successfully -- " + DateTime.Now;
            Console.WriteLine(_result);
            Logger.Info(_result);
            _body += _result + "<br/>";
            return true;
        }
        private static void BuildConfigs()
        {
            _url = _config["AppSettings:URL"];
            _email = _config["GeneralSettings:Email"];
            _mailTo = _config["GeneralSettings:EmailSetting:MailTo"];
        }
        private static void LoadSite()
        {
            _driver.LoadSite(_url);
        }
    }
}
