﻿using NLog;
using PortalMon.Models;
using SeleniumCommon;
using System;
using System.Threading;

namespace PortalMon
{
    class Program
    {
        //private static string _result;
        private static Mail m;
        private static Driver driver;
        private static Logger Logger = null;
       

        static void Main(string[] args)
        {
            try
            {
                Logger= NLog.LogManager.GetCurrentClassLogger();
                var config = new NLog.Config.LoggingConfiguration();
                var logFile = new NLog.Targets.FileTarget("logFile")
                {
                    FileName = $"Logs\\PortalMonitor-{DateTime.Today:MM-dd-yy}.log"
                };

                config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
                LogManager.Configuration = config;

                Console.WriteLine("Starting Test for Portal Monitor Portal Monitor : " + DateTime.Now);
                Logger.Info("Starting Test for Portal Monitor Portal Monitor : " + DateTime.Now);

                //Logger.Info("Starting Test for Portal Monitor...");

                var system = new Models.System(Logger);

                Logger.Info("System Model Created..");
                var configs = system.GetConfigs();
                
                driver = system.GetDriver();

                Logger.Info("** Starting Portal Monitor Testing **");

                #region load site
                Logger.Info("Loading Site");
                Logger.Info("Configs " + configs);
                Logger.Info("URL " + configs.GetPatientPortalURL());
                driver.LoadSite(configs.GetPatientPortalURL());
                #endregion

                #region regular patient Portal User
                Console.WriteLine("Checking Patient Portal");
                var normalUserPatientPortal = new PatientPortal(driver, m, system);
                var patientUrl = configs.GetPatientPortalURL();
                normalUserPatientPortal.checkPatientPortal(patientUrl);
                Logger.Info("Patient Portal Check Complete...");
                Console.WriteLine("Patient Portal Check Complete");
                #endregion

                #region admin patient Portal
                //Logger.Info("ADMIN URL " + configs.GetAdminPatientPortalURL());
                //driver.LoadSite(configs.GetAdminPatientPortalURL());
                //Console.WriteLine("Checking Admin Patient Portal");
                //var adminUserPatientPortal = new AdminPatientPortal(driver, m, system);
                //adminUserPatientPortal.checkPatientPortalAdmin();
                //Console.WriteLine("Admin Patient Portal Check Complete");
                #endregion

                #region staff patient Portal
                Logger.Info("STAFF URL " + configs.GetStaffPatientPortalURL());
                driver.LoadSite(configs.GetStaffPatientPortalURL());
                Console.WriteLine("Checking Staff Patient Portal");
                var staffUserPatientPortal = new StaffPatientPortal(driver, m, system);
                var staffpatientUrl = configs.GetStaffPatientPortalURL();
                staffUserPatientPortal.checkPatientPortalStaff(staffpatientUrl);
                Logger.Info("Staff Patient Portal Check Complete...");
                Console.WriteLine("Staff Patient Portal Check Complete");

                #endregion
                Logger.Info("====================================================================");
            }
            catch (Exception e)
            {
                Logger.Error("Error occurred " + e.Message);
                driver.CloseDriver();
                Environment.Exit(1);
            }

            driver.CloseDriver();
            Environment.Exit(0);
        }
    }
}
