﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PortalMon.Models
{
    public class Mail
    {
        public string Subject { get; set; }
        public bool IsBodyHtml { get; set; }
        public string Body { get; set; }
        public string To { get; set; }
        public string From { get; set; }
    }
}