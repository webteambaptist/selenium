﻿using NLog;
using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Text;

namespace PortalMon.Models
{
    public class System
    {
        private readonly Driver _driver;
        private readonly Configs _configs;
        private readonly Mail _m;

        public System(Logger logger)
        {
            if (_driver != null) return;
            logger.Info("Buildering Driver...");
            try
            {
            _driver = new Driver();
            }
            catch(Exception e)
            {
                logger.Error("Error building Driver " + e.Message);
            }
            logger.Info("Building Configs.... ");
            try
            {
 _configs = new Configs();
            }
           catch(Exception e)
            {
                logger.Error("Error creating configs " + e.Message);
            }
            //_m = new Mail("Portal Monitor Testing", _configs.GetMailTo(), _configs.GetFrom());
        }

        public Driver GetDriver()
        {
            return _driver;
        }
        public Configs GetConfigs()
        {
            return _configs;
        }
        //public Mail GetMail()
        //{
        //    return _m;
        //}
    }
}
