﻿using Newtonsoft.Json;
using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace PortalMon.Models
{
    public class SendMail
    {
        private string to;
        private string from;
        private string webTeamUsers;

        public SendMail()
        {
            var configs = new Configs();
            to = configs.GetMailTo();
            from = configs.GetFrom();
            webTeamUsers = configs.GetWebTeamUsers();
        }

        public static HttpWebResponse SendMailMessage(Mail m, string BaseUrl)
        {
            HttpWebResponse webResponse = null;
            string jo = JsonConvert.SerializeObject(m).ToString();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(BaseUrl);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 200000;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jo);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {

            }
            catch (Exception ex)
            {

            }

            return webResponse;
        }
        //public HttpWebResponse SendMailMessage(Mail m)
        //{
        //    HttpWebResponse webResponse = null;
        //    var json = JsonConvert.SerializeObject(m);
        //    var jo = JsonConvert.SerializeObject(m).ToString();
        //    try
        //    {
        //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(configs.mailMicroService);

        //        httpWebRequest.ContentType = "application/json";
        //        httpWebRequest.Method = "POST";
        //        httpWebRequest.Timeout = 200000;
        //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //        {
        //            streamWriter.Write(jo);
        //            streamWriter.Flush();
        //            streamWriter.Close();
        //        }
        //        webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //    }
        //    catch (WebException wex)
        //    {

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return webResponse;
        //}

        //public void SendEMail(string subject, string body)
        //{
        //    var msg = new MailMessage(from, to)
        //    {
        //        Subject = subject,
        //        Body = body,
        //        Sender = new MailAddress(from)
        //    };
        //    SendMailMessage(msg);
        //}

        //private void SendMailMessage(MailMessage m)
        //{
        //    try
        //    {
        //        m.IsBodyHtml = true;

        //        SmtpClient smtp = new SmtpClient
        //        {
        //            DeliveryMethod = SmtpDeliveryMethod.Network,
        //            Host = "smtp.bmcjax.com",
        //            Port = 25,
        //            EnableSsl = false
        //        };


        //        //if (string.IsNullOrEmpty(m.To.ToString())) return "NO SENDER"; //must have a sender

        //        smtp.Send(m);

        //    }
        //    catch (Exception)
        //    {

        //    }
        //}

        //public void SendEMailToWebTeam(string subject, string body)
        //{
        //    var msg = new MailMessage(from, webTeamUsers)
        //    {
        //        Subject = subject,
        //        Body = body,
        //        Sender = new MailAddress(from)
        //    };
        //    SendMailMessage(msg);
        //}
    }
}
