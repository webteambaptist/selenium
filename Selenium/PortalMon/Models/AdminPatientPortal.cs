﻿using NLog;
using OpenQA.Selenium;
using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PortalMon.Models
{
    public class AdminPatientPortal
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static string body = "You are receiving this message because the test account has failed to login.";
        static string subject = "LOGIN FAILURE - ADMIN PATIENT PORTAL";

        public AdminPatientPortal(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void checkPatientPortalAdmin()
        {
            var adminPatientPortalUser = _configs.GetAdminPatientPortalUser();
            var adminPatientPortalPW = _configs.GetAdminPatientPortalPW();
            var adminPatientPortalCheckValue = _configs.GetAdminPatientPortalCheckValue();

            try
            {
                Task.Delay(5000).Wait();

                d.SendKeysById("ctl04_FrmLogin_TxtUsername_internal", adminPatientPortalUser);
                d.SendKeysById("ctl04_FrmLogin_TxtPassword_internal", adminPatientPortalPW);
                d.ClickById("ctl04_BtnLogin");

                //wait to see in the event of page switching from successful login
                Task.Delay(15000).Wait();
                var pageTitle = d.driver.Title;

                if (pageTitle == adminPatientPortalCheckValue) return;

                Logger.Error(" User didnt log in :: " + body);
                var sendMail = new SendMail();

                //sendMail.SendEMail(subject, body);

            }
            catch (WebDriverTimeoutException wte)
            {
                Logger.Error("Error occurred " + wte.Message);

                var sendMail = new SendMail();
                //sendMail.SendEMail(subject, body + ":: " + wte.Message);

            }
            catch (Exception e)
            {
                var sendMail = new SendMail();
                //sendMail.SendEMailToWebTeam("Something went wrong in checkAdminPatientPortal", "Exception: " + e.Message);
            }
        }
    }
}
