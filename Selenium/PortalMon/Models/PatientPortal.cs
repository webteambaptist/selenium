﻿using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.UI;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NLog;
using System.Net.Mail;
using System.Net;

namespace PortalMon.Models
{
    public class PatientPortal
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static string body = "You are receiving this message because the test account has failed to login.";
        static string subject = "LOGIN FAILURE - PATIENT PORTAL";

        public PatientPortal(Driver driver, Mail mail, System s)
        {
            _configs = new Configs();
            system = s;
            m = mail;
            d = driver;
        }

        public void checkPatientPortal(string patientUrl)
        {
            var patientPortalUser = _configs.GetPatientPortalUser();
            var patientPortalPW = _configs.GetPatientPortalPW();
            var patientPortalCheckValue = _configs.GetPatientPortalCheckValue();

            try
            {
                Task.Delay(_configs.GetAfterLoginWait()).Wait();

                d.SendKeysById("userName", patientPortalUser);
                d.SendKeysById("password", patientPortalPW);
                d.ClickById("login");

                //wait to see in the event of page switching from successful login
                Task.Delay(_configs.GetAfterLoginWait()).Wait();
                var pageTitle = d.driver.Title;

                if (pageTitle == patientPortalCheckValue) return;

                Logger.Error(" User unable to log in :: " + body + ". :: URL: " + patientUrl);
                var sendMail = new SendMail();

                //MailMessage mm = new MailMessage(m.GetFrom(), m.GetTo());
                //mm.IsBodyHtml = true;
                //mm.Body = body + ". :: URL: " + patientUrl;
                //mm.Subject = subject;
                //sendMail.SendMailMessage(mm);

               var m = new Mail(subject, _configs.GetMailTo(), _configs.GetFrom());
                m.AddToBody(body + ". :: URL: " + patientUrl );

                var res = SendMail.SendMailMessage(m, _configs.GetMailMicroservice());
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                //sendMail.SendEMail(subject, body + ". :: URL: " + patientUrl);
            }
            catch (WebDriverTimeoutException wte)
            {
                Logger.Error("Error occurred logging into: " + patientUrl + " :: Exception: " + wte.Message);

                var sendMail = new SendMail();
                var m = new Mail(subject, _configs.GetMailTo(), _configs.GetFrom());
                m.AddToBody(body + ". The attempted login page was " + patientUrl + " :: Exception:  " + wte.Message );
                var res = SendMail.SendMailMessage(m, _configs.GetMailMicroservice());
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                //sendMail.SendEMail(subject, body + ". The attempted login page was " + patientUrl + " :: Exception:  " + wte.Message);

            }
            catch (Exception e)
            {
                var sendMail = new SendMail();
                 var m = new Mail("PortalMon: Something went wrong in checkPatientPortal", _configs.GetWebTeamUsers(), _configs.GetFrom());
                m.AddToBody("Exception: " + e.Message );
                
                var res = SendMail.SendMailMessage(m, _configs.GetMailMicroservice());
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                //sendMail.SendEMailToWebTeam("Something went wrong in checkPatientPortal", "Exception: " + e.Message);
            }
        }
    }
}
