﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PortalMon.Models
{
    public class Configs
    {
        private IConfigurationRoot _config;
        private string mailTo;
        private string mailToWebTeam;
        private string from;
        private string patientPortalUrl;
        private string patientPortalCheckVal;
        private string patientPortalUser;
        private string patientPortalPW;
        private string adminPatientPortalUrl;
        private string adminPatientPortalCheckVal;
        private string staffPatientPortalUrl;
        private string staffPatientPortalCheckVal;
        private string adminPatientPortalUser;
        private string adminPatientPortalPW;
        private string mailMicroService;
        private string afterLoginWait;


        public Configs()
        {
            //var directory = Directory.GetCurrentDirectory();
            var builder = new ConfigurationBuilder()
                //.SetBasePath(directory)
                .AddJsonFile("appsettings.json", true, true);

            _config = builder.Build();

            mailTo = _config.GetSection("EmailSetting").GetSection("MailTo").Value;
            mailToWebTeam = _config.GetSection("EmailSetting").GetSection("MailToWebTeam").Value;
            from = _config.GetSection("EmailSetting").GetSection("From").Value;
            mailMicroService = _config.GetSection("EmailSetting").GetSection("MailMicroService").Value;

            patientPortalUrl = _config.GetSection("AppSettings").GetSection("PATIENT_PORTAL_URL").Value;
            patientPortalCheckVal = _config.GetSection("AppSettings").GetSection("PATIENT_PORTAL_CHECKVAL").Value;
            patientPortalUser = _config.GetSection("AppSettings").GetSection("PATIENT_PORTAL_USER").Value;
            patientPortalPW = _config.GetSection("AppSettings").GetSection("PATIENT_PORTAL_PASS").Value;
            adminPatientPortalUrl = _config.GetSection("AppSettings").GetSection("ADMIN_PATIENT_PORTAL_URL").Value;
            adminPatientPortalCheckVal = _config.GetSection("AppSettings").GetSection("ADMIN_PATIENT_PORTAL_CHECKVAL").Value;
            staffPatientPortalUrl = _config.GetSection("AppSettings").GetSection("STAFF_PATIENT_PORTAL_URL").Value;
            staffPatientPortalCheckVal = _config.GetSection("AppSettings").GetSection("STAFF_PATIENT_PORTAL_CHECKVAL").Value;
            adminPatientPortalUser = _config.GetSection("AppSettings").GetSection("ADMIN_PATIENT_PORTAL_USER").Value;
            adminPatientPortalPW = _config.GetSection("AppSettings").GetSection("ADMIN_PATIENT_PORTAL_PASS").Value;
            afterLoginWait = _config.GetSection("WaitTimes").GetSection("AfterLoginWait").Value;
        }

        public int GetAfterLoginWait()
        {
            return Convert.ToInt32(afterLoginWait);
        }
        public string GetMailMicroservice()
        {
            return mailMicroService;
        }
        public string GetMailTo()
        {
            return mailTo;
        }
        public string GetWebTeamUsers()
        {
            return mailToWebTeam;
        }
        public string GetFrom()
        {
            return from;
        }

        public string GetPatientPortalURL()
        {
            return patientPortalUrl;
        }

        public string GetPatientPortalCheckValue()
        {
            return patientPortalCheckVal;
        }

        public string GetPatientPortalUser()
        {
            return patientPortalUser;
        }

        public string GetPatientPortalPW()
        {
            return patientPortalPW;
        }

        public string GetAdminPatientPortalURL()
        {
            return adminPatientPortalUrl;
        }

        public string GetAdminPatientPortalCheckValue()
        {
            return adminPatientPortalCheckVal;
        }

        public string GetStaffPatientPortalURL()
        {
            return staffPatientPortalUrl;
        }

        public string GetStaffPatientPortalCheckValue()
        {
            return staffPatientPortalCheckVal;
        }

        public string GetAdminPatientPortalUser()
        {
            return adminPatientPortalUser;
        }

        public string GetAdminPatientPortalPW()
        {
            return adminPatientPortalPW;
        }
    }
}
