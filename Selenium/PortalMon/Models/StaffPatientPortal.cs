﻿using NLog;
using OpenQA.Selenium;
using SeleniumCommon;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PortalMon.Models
{
    public class StaffPatientPortal
    {
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        private readonly System system;

        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static string body = "You are receiving this message because the test account has failed to login.";
        static string subject = "LOGIN FAILURE - ADMIN PATIENT PORTAL";

        public StaffPatientPortal(Driver driver, Mail m, System system)
        {
            _configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void checkPatientPortalStaff(string staffpatientUrl)
        {
            var staffPatientPortalUser = _configs.GetAdminPatientPortalUser();
            var staffPatientPortalPW = _configs.GetAdminPatientPortalPW();
            var staffPatientPortalCheckValue = _configs.GetStaffPatientPortalCheckValue();

            try
            {
                Task.Delay(_configs.GetAfterLoginWait()).Wait();

                d.SendKeysById("userName", staffPatientPortalUser);
                d.SendKeysById("password", staffPatientPortalPW);
                d.ClickById("login");

                //wait to see in the event of page switching from successful login
                Task.Delay(_configs.GetAfterLoginWait()).Wait();
                var pageTitle = d.driver.Title;

                if (pageTitle == staffPatientPortalCheckValue) return;

                Logger.Error(" User unable to log in :: " + body + ". :: URL: " + staffpatientUrl);
                var sendMail = new SendMail();
                var m = new Mail(subject, _configs.GetMailTo(), _configs.GetFrom());
                m.AddToBody(body + ". :: URL: " + staffpatientUrl );
                

                var res = SendMail.SendMailMessage(m, _configs.GetMailMicroservice());
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                //sendMail.SendEMail(subject, body + ". :: URL: " + staffpatientUrl);

            }
            catch (WebDriverTimeoutException wte)
            {
                Logger.Error("Error occurred logging into: " + staffpatientUrl + " :: Exception: " + wte.Message);

                var sendMail = new SendMail();
                var m = new Mail(subject, _configs.GetMailTo(), _configs.GetFrom());
                m.AddToBody(body + ". The attempted login page was " + staffpatientUrl + " :: Exception:  " + wte.Message );
                

                var res = SendMail.SendMailMessage(m, _configs.GetMailMicroservice());
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                //sendMail.SendEMail(subject, body + ". The attempted login page was " + staffpatientUrl + " :: Exception:  " + wte.Message);

            }
            catch (Exception e)
            {
                var sendMail = new SendMail();
                var m = new Mail("PortalMon: Something went wrong in checkPatientPortalStaff", _configs.GetWebTeamUsers(), _configs.GetFrom());
                m.AddToBody("Exception: " + e.Message );
                
                var res = SendMail.SendMailMessage(m, _configs.GetMailMicroservice());
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                //sendMail.SendEMailToWebTeam("Something went wrong in checkAdminPatientPortal", "Exception: " + e.Message);
            }
        }
    }
}
