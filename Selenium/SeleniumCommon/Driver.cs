﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace SeleniumCommon
{
    public class Driver
    {
        public ChromeDriver driver;
        public WebDriverWait wait;
        public Driver()
        {
            var options = new ChromeOptions();
            options.AddArguments("--no-sandbox"); // Bypass OS security model, MUST BE THE VERY FIRST OPTION
            options.AddArguments("headless"); // does not open browser
            //options.AddArguments("start-maximized"); // open Browser in maximized mode
            options.AddArguments("--window-size=1024,768");
            options.AddArguments("disable-infobars"); // disabling infobars
            options.AddArguments("--disable-extensions"); // disabling extensions
            options.AddArguments("--disable-gpu"); // applicable to windows os only
            options.AddArguments("--disable-dev-shm-usage"); // overcome limited resource problems
            //warning CS0618: 'DriverOptions.AddAdditionalCapability(string, object)' is obsolete:
            //'Use the temporary AddAdditionalOption method or the browser-specific method for adding additional options'
            options.AddAdditionalOption("useAutomationExtension", false);
            options.AddUserProfilePreference("download.prompt_for_download", false);
            var service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;

            driver = new ChromeDriver(service, options);
            wait = new WebDriverWait(driver, new TimeSpan(0, Convert.ToInt32(5), 0));
            
        }

        public IReadOnlyCollection<IWebElement> GetElementsByClassName(string className)
        {
            return driver.FindElements(By.ClassName(className));
        }

        public void CloseDriver()
        {
            driver.Close();
        }
        public void LoadSite(string url)
        {
            driver.Navigate().GoToUrl(url);
        }
        public IWebElement GetElementByName(string element)
        {
            return driver.FindElement(By.Name(element));
        }
        public IWebElement GetElementById(string id)
        {
            return driver.FindElement(By.Id(id));
        }
        public IWebElement GetElementByClassName(string className)
        {
            return driver.FindElement(By.ClassName(className));
        }
        public IWebElement GetElementByLinkText(string linkText)
        {
            return driver.FindElement(By.LinkText(linkText));
        }
        public IWebElement GetElementByXPath(string xpath)
        {
            return driver.FindElement(By.XPath(xpath));
        }
        public IWebElement GetElementByXPathForElement(IWebElement element, string xpath)
        {
            return element.FindElement(By.XPath(xpath));
        }
        public ReadOnlyCollection<IWebElement> GetElementsByXPathForElement(IWebElement element, string xpath)
        {
            return element.FindElements(By.XPath(xpath));
        }
        public bool IsIdDisplayed(string id)
        {
            try
            {
                return GetElementById(id).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public bool IsNameDisplayed(string name)
        {
            try
            {
                return GetElementByName(name).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public bool IsElementDisplayed(IWebElement element)
        {
            try
            {
                return element.Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public bool IsElementDisplayed(string element)
        {
            try
            {
                return GetElementByName(element).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public bool IsClassNameDisplayed(string className)
        {
            try
            {
                return GetElementByClassName(className).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public bool IsClassNameDisplayed(IWebElement element, string className)
        {
            try
            {
                return element.FindElement(By.ClassName(className)).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public bool IsDisplayedByName(string name)
        {
            try
            {
                return GetElementByName(name).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public bool IsDisplayedByLinkText(string linkText)
        {
            try
            {
                return GetElementByLinkText(linkText).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            
        }
        public bool IsDisplayedByXPath(string xpath)
        {
            try
            {
                return GetElementByXPath(xpath).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public void SendKeysByName(string name, string key)
        {
            GetElementByName(name).SendKeys(key);
        }
         public void SendKeysByClassName(IWebElement element, string className, string key)
        {
            element.FindElement(By.ClassName(className)).SendKeys(key);
        }
        public void SendKeysByClassName(string className, string key)
        {
            GetElementByClassName(className).SendKeys(key);
        }
        public void ClickReturnByClassName(string className)
        {
            GetElementByClassName(className).SendKeys(Keys.Return);
        }
        public void SendKeysById(string id, string key)
        {
            GetElementById(id).SendKeys(key);
        }
        public void SendKeysByXPath(string xpath, string key)
        {
            GetElementByXPath(xpath).SendKeys(key);
        }
        public string GetF5Key()
        {
            return Keys.F5;
        }
        public void SendKeysByKeyAction(Actions action, string key)
        {
            action.SendKeys(key);
        }
        public string GetAttributeByName(string name, string attribute)
        {
            return GetElementByName(name).GetAttribute(attribute);
        }
        public string GetAttributeByElement(IWebElement element, string attribute)
        {
            return element.GetAttribute(attribute);
        }
        public void ClearElementByName(string name)
        {
            GetElementByName(name).Clear();
        }
        public void ClearElementById(string id)
        {
            GetElementById(id).Clear();
        }
        public void ClickById(string id)
        {
            GetElementById(id).Click();
        }
        public void ClickByClassName(string className)
        {
            GetElementByClassName(className).Click();
        }
        public void ClickByName(string name)
        {
            GetElementByName(name).Click();
        }
        public void ClickByLinkText(string linkText)
        {
            GetElementByLinkText(linkText).Click();
        }
        public void NavigateToUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
        }
        public string SwitchNewTabs()
        {
            var originalTabInstance = driver.CurrentWindowHandle;

            // save a reference to our new tab's window handle, this would be the last entry in the WindowHandles collection
            var index = driver.WindowHandles.Count;
            var newTabInstance = driver.WindowHandles[index - 1];
            // switch our Webdriver to the new tab's window handle
            driver.SwitchTo().Window(newTabInstance);
            return originalTabInstance;
        }
        public void SwitchTabs()
        {
            var tab = driver.WindowHandles;
            driver.SwitchTo().Window(tab[1]);
            driver.Close();
            driver.SwitchTo().Window(tab[0]);
        }
        public void SwitchBackTabs(string originalTab)
        {
            // and switch our Webdriver back to the original tab's window handle
            driver.SwitchTo().Window(originalTab);
            // and have our Webdriver focus on the main document in the page to send commands to 
            driver.SwitchTo().DefaultContent();

            driver.SwitchTo().Window(driver.WindowHandles.First());
        }
        public SelectElement GetSelectElement(IWebElement webElement)
        {
            return new SelectElement(webElement);
        }
        public Actions GetActions()
        {
            return new Actions(this.driver);
        }
        public void KeyDown(Actions actionObject)
        {
            actionObject.KeyDown(Keys.Control).Perform();
        }
        public void KeyUp(Actions actionObject)
        {
            actionObject.KeyUp(Keys.Control).Perform();
        }
        public void ExecuteJs(string js)
        {
            driver.ExecuteJavaScript<object>(js);
        }
        public bool IsExecuteJs(string js)
        {
            return (bool) ((IJavaScriptExecutor) driver).ExecuteScript(js);
        }
        public void MaximizeWindow()
        {
            driver.Manage().Window.Maximize();
        }
    }
}
