﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumPP.Models
{
    public class RootObject
    {
        [JsonProperty("value")]
        public List<Value> Results { get; set; }
    }
    public class Value
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Title")]
        public string Title { get; set; }
    }
}
