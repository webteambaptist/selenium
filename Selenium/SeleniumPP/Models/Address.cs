﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumPP.Models
{
    public class Address
    {
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string latlon { get; set; }
    }
}
