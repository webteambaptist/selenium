﻿using System;
using System.Net.Http;
using SeleniumCommon;
using SeleniumPP.SeleniumModels;
using Driver = SeleniumPP.SeleniumModels.Driver;
using NLog;
namespace SeleniumPP
{
    class Program
    {
        private static string _result;
        private static Mail m;
        private static SendMail mail;
        private static Driver driver;
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static void Main(string[] args)
        {
            try
            {
                var config = new NLog.Config.LoggingConfiguration();
                var logFile = new NLog.Targets.FileTarget("logFile")
                {
                    FileName = @".\logs\SeleniumPP.log"
                };

                config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
                LogManager.Configuration = config;

                Logger.Info("Test started...");

                var system = new SeleniumModels.System();
                
                Logger.Info("System Model Created..");
                var configs = system.GetConfigs();
                driver = system.GetDriver();
                m = system.GetMail();
                var auth = new Authentication(driver, m);
                mail = new SendMail();
                
                driver.MaximizeWindow();
                _result = "** Starting Physicians Portal Regression Testing **";
                Logger.Info("** Starting Physicians Portal Regression Testing **");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                #region loadSite
                //// Load Site
                _result = "** Loading Site **";
                Logger.Info("Loading Site");
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Configs " + configs);
                Logger.Info("URL " + configs.GetLoginUrl());
                driver.LoadSite(configs.GetLoginUrl());

                _result = "Site Loaded";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                
                #endregion
                #region login
                Logger.Info("Logging in");
                _result = "** Logging In **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                
                var success =  auth.Login();
                if (!success)
                {
                    mail.SendEMail("PP Regression Testing Failed Logging In", m.GetBody());
                    driver.CloseDriver();
                    Environment.Exit(0);
                    return;
                }
                Logger.Info("Logged in");
                _result = "Finished Login";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                #endregion
                #region Validate search

                _result = "** Validating Search Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var search = new Search(driver, m);
                search.ValidateSearch();

                _result = "Search Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Search complete");
                #endregion
                #region HeaderLinks
                _result = "** Header Links Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var header = new Header(driver, m);
                header.TestHeader();

                _result = "Header Links Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Header test completed");
                #endregion
                #region Informatics
                _result = "** Informatics Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var informatics = new Informatics(driver, m);
                informatics.TestInfomatics();
                
                _result = "Informatics Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Informatics test completed");
                #endregion
                #region Forms
                _result = "** Forms Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var forms = new Forms(driver, m, system);
                forms.TestForms();

                _result = "Forms Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Forms test completed");
                #endregion
                #region Find a Doctor
                _result = "** Find a Doctor Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var fap = new FAP(driver, m, system);
                fap.TestFap();
                
                _result = "Find a Doctor Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("FAP test completed");
                #endregion
                #region Health Science Library

                _result = "** Health Science Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var healthScience = new HealthScience(driver, m, system);
                healthScience.TestHealthSciences();

                _result = "Health Science Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Health science finished");
                #endregion
                #region App Catalog

                _result = "** App Catalog Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

               var appCatelog = new AppCatalog(driver, m, system);
               appCatelog.TestAppCatalog();

                _result = "App Catalog Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("App catalog test complete");
                #endregion
                #region Quick Links
                _result = "** Quick Links Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

               var quickLinks = new QuickLinks(driver, m, system);
               quickLinks.TestQuickLinks();

                _result = "Quick Links Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Quick links finish");
                #endregion

                #region GetHelp
                _result = "** Get Help Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var getHelp = new GetHelp(driver, m, system);
                getHelp.TestGetHelp();

                _result = "Get Help Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Get Help completed");
                #endregion
                #region InfoHub

                _result = "** InfoHub Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var infoHub = new InfoHub(driver, m, system);
                infoHub.TestInfoHub();

                _result = "InfoHub Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("Info hub finished");
                #endregion

                #region ContactUs
                _result = "** ContactUs Started **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");

                var ContactUs = new ContactUs(driver, m, system);
                ContactUs.TestContactUs();

                _result = "ContactUs Completed";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                Logger.Info("ContactUs finished");
                #endregion

                #region logout
                _result = "** Logging Out **";
                Console.WriteLine(_result);
                m.AddToBody(_result + "<br/>");
                success = auth.Logout();

                if (!success)
                {
                    mail.SendEMail("PP Regression Testing Failed Logging Out", m.GetBody());
                }
                #endregion

                mail.SendEMail("PP Regression Finished", m.GetBody());
            }
            catch (Exception e)
            {
                Logger.Error("Error occurred " + e.Message);
                driver.CloseDriver();
                
                _result = "An Exception occurred in Program::Main " + e.Message;
                m.AddToBody(_result + "<br/>");
                mail.SendEMail("PP Regression Testing Failed (Exception)", m.GetBody());
                Environment.Exit(1);
                return;
            }
            
            
            driver.CloseDriver();
            Environment.Exit(0);
            return;
        }
    }
}