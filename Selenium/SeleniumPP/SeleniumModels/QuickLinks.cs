﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class QuickLinks
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly System system;
        public QuickLinks(Driver driver, Mail m, System system)
        {
            configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }
        public void TestQuickLinks()
        {
            var result = "Starting TestQuickLinks";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result += "<br/>");
            try
            {
                d.NavigateToUrl(configs.GetHomeUrl());
                
                var rightColumn = d.GetRightColumn();
                var exTab = d.GetElementByIdForElement(rightColumn, "exTab1");
                var navPills = d.GetElementByClassNameForElement(exTab, "nav-pills");
                var lis = d.GetElementsByTagName(navPills, "li");
                lis.ElementAt(1).Click();

                #region AddLink

                try
                {
                    // Add Link
                    var tabContent = d.GetElementByClassNameForElement(exTab, "tab-content");
                    var tabPane = d.GetElementByIdForElement(tabContent, "two-a");
                    var ul = d.GetElementByTagName(tabPane, "ul");
                    var addLinkLi = d.GetElementByClassNameForElement(ul, "addlink_li");
                    lis = d.GetElementsByTagName(addLinkLi, "li");
                    var li = lis.ElementAt(1);
                    li.Click();

                    Task.Delay(10000).Wait();
                    var modalExists = d.IsIdDisplayed("universalModal");
                    if (modalExists)
                    {
                        result = "Pass: Modal popped up for adding Quick Link successful ";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");

                        const string linkName = "Yahoo";
                        d.SendKeysByClassName("link_name", linkName);
                        d.SendKeysByClassName("link_address", "www.yahoo.com");
                        d.ClickByClassName("confirm-btn");

                        Task.Delay(10000).Wait();
                        // Verify that the link was added

                        var field = d.GetElementByClassName("input_fields_wrap");
                        var divs = d.GetElementsByClassNameForElement(field, "addlink_dynlist");

                        var linkFound = false;
                        var divList = d.GetQuickLinks(divs, linkName);
                        if (divList.Count > 0) linkFound = true;

                        if (divs.Select(div => d.GetElementByTagName(div, "li")).Select(linkLi => d.GetElementByClassNameForElement(linkLi, "addlink-a").Text).Any(linkText => linkText == linkName))
                        {
                            linkFound = true;
                        }
                        if (linkFound)
                        {
                            result = "Pass: Quick Link was added successfully ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");

                            #region Delete Link

                            // delete link
                            d.ClickByClassName("addlink_manage-txt");
                            field = d.GetElementByClassName("input_fields_wrap"); // rebuild in case lost
                            divs = d.GetElementsByClassNameForElement(field, "addlink_dynlist");

                            foreach (var div in divs)
                            {
                                var linkLi = d.GetElementByTagName(div, "li");
                                var linkText = d.GetElementByClassNameForElement(linkLi, "addlink-a").Text;

                                if (linkText != linkName) continue;
                                d.GetElementByClassNameForElement(div, "remove_field").Click();
                                break;
                            }

                            d.ClickByClassName("addlink_manage-txt");
                            field = d.GetElementByClassName("input_fields_wrap"); // rebuild in case lost
                            divs = d.GetElementsByClassNameForElement(field, "addlink_dynlist");
                            // verify it was deleted

                            divList = d.GetQuickLinks(divs, linkName);
                            if (divList.Count > 0)
                            {
                                result = "Fail: Quick Link did not delete. <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }
                            else
                            {
                                result = "Pass: Quick Link was deleted successfully ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            #endregion
                        }
                        else
                        {
                            result = "Fail: Quick Link was not added. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: Modal didn't come up when clicking Add Link. <br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                }
                catch (Exception e)
                {
                    result = "EXCEPTION: Exception occurred while testing Quick Links :: AddLink . <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                    return;
                }

                #endregion
                #region Helpful Links
                try
                {
                    rightColumn = d.GetRightColumn();
                    exTab = d.GetElementByIdForElement(rightColumn, "exTab1");
                    navPills = d.GetElementByClassNameForElement(exTab, "nav-pills");
                    lis = d.GetElementsByTagName(navPills, "li");
                    lis.ElementAt(1).Click();
                    var links = d.GetElementsByClassName("bhlinks");
                    var linkCount = 0;
                    while (linkCount < links.Count)
                    {
                        var link = d.GetElementByClassNameForElement(links.ElementAt(linkCount), "addlink-a");
                        var linkText = link.Text;
                        var url = d.GetAttributeByElement(link, "href");
                        link.Click();

                        var numLinks = d.GetNumTabs();
                        if (numLinks > 1)
                        {
                            d.SwitchNewTabs();
                            if (url == d.GetUrl())
                            {
                                result = "Pass: " + linkText + " link opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "QuickList :: Helpful link not validated . <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }
                            d.SwitchTabs();
                        }
                        linkCount++;
                        links = d.GetElementsByClassName("bhlinks");
                    }
                }
                catch (Exception e)
                {
                    result = "EXCEPTION: Exception occurred while testing Quick Links :: Helful Links . <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }
                #endregion
                #region Events Calendar

                try
                {
                    rightColumn = d.GetRightColumn();
                    var row = d.GetElementByClassNameForElement(rightColumn, "row");
                    var calendar = d.GetElementByClassNameForElement(row, "upcoming-events");

                    var heading = d.GetElementByTagName(calendar, "h3");
                    
                    if (heading.Displayed)
                    {
                        var bgWhite = d.GetElementByClassNameForElement(calendar, "bg-white");
                        var links = d.GetElementsByTagName(bgWhite, "a");
                        var linksCounter = 0;
                        while(linksCounter < links.Count)
                        {
                            try
                            {
                                var upcomingEvents = d.GetElementByClassNameForElement(links.ElementAt(linksCounter),
                                    "upcoming-event");
                                var upcomingInfo = d.GetElementByClassNameForElement(upcomingEvents, "upcoming-info");
                                var title = d.GetElementByClassNameForElement(upcomingInfo, "upcoming-title").Text;
                                links.ElementAt(linksCounter).Click();
                                
                                var centerColumn = d.GetCenterColumn();
                                row = d.GetElementByClassNameForElement(centerColumn, "row");
                                var colMd = d.GetElementByClassNameForElement(row, "col-md-4");
                                var eventTitle = d.GetElementByIdForElement(colMd, "event-title").Text;
                                
                                if (title.Equals(eventTitle))
                                {
                                    result = "Pass: Quick Links Calendar Event " + title + " exists";
                                    Console.WriteLine(result);
                                    Logger.Info(result);
                                    m.AddToBody(result + "<br/>");
                                }
                                else
                                {
                                    result = "Fail: Quick Links Calendar Event " + title + " does not exists";
                                    Console.WriteLine(result);
                                    Logger.Error(result);
                                    m.AddToBody(result + "<br/>");
                                }
                                d.BackPage();
                                linksCounter++; // increase the counter to get next link
                                // click QuickLinks again
                                rightColumn = d.GetRightColumn();
                                exTab = d.GetElementByIdForElement(rightColumn, "exTab1");
                                navPills = d.GetElementByClassNameForElement(exTab, "nav-pills");
                                lis = d.GetElementsByTagName(navPills, "li");
                                lis.ElementAt(1).Click();
                                // find the events and get links
                                row = d.GetElementByClassNameForElement(rightColumn, "row");
                                calendar = d.GetElementByClassNameForElement(row, "upcoming-events");
                                bgWhite = d.GetElementByClassNameForElement(calendar, "bg-white");
                                links = d.GetElementsByTagName(bgWhite, "a");
                            }
                            catch (Exception e)
                            {
                                result = "EXCEPTION: Exception occurred while testing Quick Links Calendar Clicks. <br/>";
                                result += e.Message;
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                                
                                linksCounter++;
                                bgWhite = d.GetElementByClassNameForElement(calendar, "bg-white");
                                links = d.GetElementsByTagName(bgWhite, "a");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "EXCEPTION: Exception occurred while testing Quick Links :: Calendar . <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }

                #endregion
            }
            catch (Exception e)
            {
                result = "EXCEPTION: Exception occurred while testing Quick Links. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
                return;
            }
            result = "Pass: Quick Links Tests completed successful ";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
        }
    }
}