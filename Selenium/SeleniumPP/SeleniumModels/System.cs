﻿
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class System 
    {
        private readonly Driver _driver;
        private readonly Configs _configs;
        private readonly Mail _m;
        
        public System()
        {
            if (_driver != null) return;
            _driver = new Driver();
            _configs = new Configs();
            _m = new Mail("PP Regression Testing", _configs.GetMailTo(), _configs.GetFrom());
        }
        public Driver GetDriver()
        {
            return _driver;
        }
        public Configs GetConfigs()
        {
            return _configs;
        }
        public Mail GetMail()
        {
            return _m;
        }
        public bool CheckFileDownloaded(string extension)
        {
            try
            {
                Task.Delay(10000).Wait();
                //_driver.ImplicitWait(20);
                var directory = new DirectoryInfo(@"C:\Users\tanth001\Downloads");
                var files = directory.GetFiles();
                if (files.Length == 0) return false;
                var file = files.OrderByDescending(x => x.LastAccessTime).First();
                if (!file.Extension.Equals(extension)) return false;
                file.Delete();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public static void ValidateHelpfulLinks(int id, string url)
        //{
        //    var bhLinks = _driver.FindElements(By.ClassName("bhlinks"));
        //    var helpfulLink = bhLinks[id].FindElement(By.ClassName("addlink-a"));
        //    helpfulLink.Click();

        //    Console.WriteLine("Sleeping for 5 seconds");
        //    Thread.Sleep(5000);

        //    var tab = _driver.WindowHandles;
        //    if (tab.Count <= 1) return;
        //    // switch to second tab
        //    _driver.SwitchTo().Window(tab[1]);
        //    var title = _driver.Title;
        //    if (_driver.Url.Contains(url))
        //    {
        //        _result = "Pass: " + title + " link opened successful ";
        //        Console.WriteLine(_result);
        //        m.AddToBody(_result + "<br/>");
        //    }
        //    else
        //    {
        //        _result = "******************************************************** <br/>";
        //        _result += "Fail: " + title + " Link not opened. <br/>";
        //        _result += "******************************************************** <br/>";
        //        Console.WriteLine(_result);
        //        _body += _result += "<br/>";
        //    }
        //    _driver.Close();
        //    _driver.SwitchTo().Window(tab[0]);
        //}
    }
}
