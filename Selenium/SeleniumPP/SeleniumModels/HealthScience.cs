﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using OpenQA.Selenium.Internal;
using SeleniumCommon;
using Exception = System.Exception;

namespace SeleniumPP.SeleniumModels
{
    public class HealthScience
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly System system;
        private readonly string name;
        private readonly string email;
        private readonly string profession;
        private readonly string dept;
        private readonly string location;
        private readonly string year;
        private readonly string phone;
        private readonly string searchRequest;
        private readonly string journalProfession;
        private readonly string journalLocation;
        private readonly string journalRequest;
        private readonly string bookRequest;
        public HealthScience(Driver driver, Mail m, System system)
        {
            configs = new Configs();
            this.system = system;
            this.m = m;
            d = driver;
            name = configs.GetName();
            email = configs.GetEmail();
            profession = configs.GetProfession();
            dept = configs.GetDept();
            location = configs.GetLocation();
            year = configs.GetYear();
            phone = configs.GetPhone();
            journalProfession = configs.GetJournalProfession();
            journalLocation = configs.GetJournalLocation();
            journalRequest = configs.GetJournalRequest();
            bookRequest = configs.GetBookRequest();
            searchRequest = configs.GetSearchRequest();
        }

        public void TestHealthSciences()
        {
            var result = "Starting Health Science Tests";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            try
            {
                var lis = d.GetNavIconsLis();
                lis.ElementAt(3).Click();
                
                #region Top Scroll Bar

                var slickList = d.GetSlickList();

                #region Ovid Medline

                try
                {
                    var slideZero = d.GetElementByXPathForElement(slickList,
                        "//div[@class='slick-track']//div[@aria-describedby='slick-slide00']");
                    var link = d.GetLinkByElementOrderedByHref(slideZero);
                    var imgTag = d.GetElementByTagName(link, "img");
                    var src = d.GetAttributeByElement(imgTag, "src");

                    // get link image name
                    if (src.Contains("/-/media/Images/Medical-Library/LogoOvidMedLine"))
                    {
                        link.Click();
                        
                        var tab = d.GetNumTabs();
                        if (tab > 1)
                        {
                            // bpp test
                            // switch to second tab
                            d.SwitchNewTabs();
                            var u = d.GetUrl();
                            if (u.Contains("ovidsp")) /// changed to just ovidsp because the url changes
                            {
                                result = "Pass: Health Science Library: Ovid Medline Opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "Fail: Health Science Library: Ovid Medline Link not opened. <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }
                            d.SwitchTabs();
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Fail: An exception occurred :: Ovid Medline" + e.Message ;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }
                #endregion
                #region ClinicalKey

                try
                {

                    slickList = d.GetSlickList();
                    var slideOne = d.GetElementByXPathForElement(slickList, "//div[@aria-describedby='slick-slide01']");
                    var link = d.GetElementByTagName(slideOne, "a");
                    var imgTag = d.GetElementByTagName(link, "img");
                    var src = d.GetAttributeByElement(imgTag, "src");

                    if (src.Contains("/-/media/Images/Medical-Library/logo-clinicalkey"))
                    {
                        link.Click();
                        Console.WriteLine("Pausing for 5 seconds");
                        Logger.Info(result);
                        var tab = d.GetNumTabs();
                        if (tab > 1)
                        {
                            // bpp test
                            // switch to second tab
                            d.SwitchNewTabs();
                            var u = d.GetUrl();
                            if (u.Contains("https://www.clinicalkey.com"))
                            {
                                result = "Pass: Health Science Library: ClinicalKey Opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "Fail: Health Science Library: ClinicalKey Link not opened. <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }

                            d.SwitchTabs();
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Fail: An exception occurred :: ClicalKey" + e.Message ;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }

                #endregion
                #region Medline

                try
                {
                    slickList = d.GetSlickList();
                    var slideTwo = d.GetElementByXPathForElement(slickList, "//div[@aria-describedby='slick-slide02']");
                    var link = d.GetElementByTagName(slideTwo, "a");
                    var imgTag = d.GetElementByTagName(link, "img");
                    var src = d.GetAttributeByElement(imgTag, "src");
                    if (src.Contains("/-/media/Images/Medical-Library/Medline-175"))
                    {
                        link.Click();
                        
                        var tab = d.GetNumTabs();
                        if (tab > 1)
                        {
                            // bpp test
                            // switch to second tab
                            d.SwitchNewTabs();
                            var u = d.GetUrl();
                            if (u.Contains("http://web.b.ebscohost.com/ehost/search/advanced") ||
                                u.Contains("http://web.a.ebscohost.com/ehost/search/advanced"))
                            {
                                result = "Pass: Health Science Library: Medline (with full text) Opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result =
                                    "Fail: Health Science Library: Medline (with full text) Link not opened. <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }

                            d.SwitchTabs();
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Fail: An exception occurred :: Medline " + e.Message ;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }

                #endregion
                #region PublMed

                try
                {
                    slickList = d.GetSlickList();
                    var slideThree =
                        d.GetElementByXPathForElement(slickList, "//div[@aria-describedby='slick-slide03']");
                    var link = d.GetElementByTagName(slideThree, "a");
                    var imgTag = d.GetElementByTagName(link, "img");
                    var src = d.GetAttributeByElement(imgTag, "src");

                    if (src.Contains("/-/media/Images/Medical-Library/logo-pubmed"))
                    {
                        link.Click();
                       
                        var tab = d.GetNumTabs();
                        if (tab > 1)
                        {
                            // bpp test
                            // switch to second tab
                            d.SwitchNewTabs();
                            var u = d.GetUrl();
                            if (u.Contains("pubmed"))
                            {
                                result = "Pass: Health Science Library: PubMed Opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "Fail: Health Science Library: PubMed Link not opened. <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }

                            d.SwitchTabs();
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Fail: An exception occurred :: PublMed" + e.Message ;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }
                #endregion
                // click to the next 
                d.ClickByClassName("slick-next");
                
                #region EBSCO

                try
                {
                    slickList = d.GetSlickList();
                    var slideFour =
                        d.GetElementByXPathForElement(slickList, "//div[@aria-describedby='slick-slide04']");
                    var link = d.GetElementByTagName(slideFour, "a");
                    var imgTag = d.GetElementByTagName(link, "img");
                    var src = d.GetAttributeByElement(imgTag, "src");

                    if (src.Contains("/-/media/Images/Medical-Library/logo-ebsco"))
                    {
                        link.Click();
                        Console.WriteLine("Pausing for 5 seconds");
                        
                        var tab = d.GetNumTabs();
                        if (tab > 1)
                        {
                            // bpp test
                            // switch to second tab
                            d.SwitchNewTabs();
                            var u = d.GetUrl();
                            if (u.Contains("https://search.ebscohost.com/"))
                            {
                                result = "Pass: Health Science Library: EBSCO Opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "Fail: Health Science Library: EBSCO Link not opened. <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }
                            d.SwitchTabs();
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Fail: An exception occurred :: EBSCO" + e.Message ;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }

                #endregion
                #region Cochrane

                try
                {
                    slickList = d.GetSlickList();
                    var slideFive =
                        d.GetElementByXPathForElement(slickList, "//div[@aria-describedby='slick-slide05']");
                    var link = d.GetElementByTagName(slideFive, "a");
                    var imgTag = d.GetElementByTagName(link, "img");
                    var src = d.GetAttributeByElement(imgTag, "src");

                    if (src.Contains("/-/media/Images/Medical-Library/CochraneCollection-175"))
                    {
                        link.Click();
                        Console.WriteLine("Pausing for 5 seconds");
                        
                        var tab = d.GetNumTabs();
                        if (tab > 1)
                        {
                            // bpp test
                            // switch to second tab
                            d.SwitchNewTabs();
                            var u = d.GetUrl();
                            if (u.Contains("http://web.b.ebscohost.com/ehost/search/advanced") ||
                                u.Contains("http://web.a.ebscohost.com/ehost/search/advanced"))
                            {
                                result = "Pass: Health Science Library: Cochrane Opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "Fail: Health Science Library: Cochrane Link not opened. <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }

                            d.SwitchTabs();
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Fail: An exception occurred :: Cochrane" + e.Message ;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }
                #endregion
                #endregion
                #region Library Search Request
                try
                {
                    lis = d.GetNavIconsLis();
                    lis.ElementAt(3).Click();
                    
                    var buttons = d.GetElementsByClassName("btn-library");
                    buttons.First().Click();
                    if (d.GetElementByClassName("pp-header").Text == "Library Search Request")
                    {
                        result = "Pass: Health Science Library:Library Search Request loaded successfully ";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");

                        // Name
                        d.SendKeysById("Name", name);
                        // Email
                        d.SendKeysById("Email", email);
                        // Profession
                        var element = d.GetElementById("SelectedProfession");
                        var se = d.GetSelectElement(element);
                        se.SelectByText(profession);

                        // Dept
                        d.SendKeysById("Dept", dept);
                        // Location
                        element = d.GetElementById("SelectedLocation");
                        se = d.GetSelectElement(element);
                        se.SelectByText(location);

                        // Search Request
                        d.SendKeysById("SearchRequest", searchRequest);
                        // Search Qualifications
                        var input = d.GetElementsByName("QualificationIds");
                        input.ElementAt(2).Click();
                        // Ages
                        input = d.GetElementsByName("Ages");
                        input.ElementAt(2).Click();

                        // Years to be covered
                        d.SendKeysByName("YearsToBeCovered", year);
                        // Kind of Search you want
                        input = d.GetElementsByName("Kinds");
                        input.ElementAt(2).Click();

                        // Purpose of the search/Audience
                        input = d.GetElementsByName("Audiences");
                        input.ElementAt(1).Click();

                        // Delivery Information
                        input = d.GetElementsByName("DeliveryTypes");
                        input.ElementAt(1).Click();
                        
                        // Phone
                        d.SendKeysById("PhoneNumber",phone);
                        d.ClickByClassName("btn-default");
                        
                        var leftColumn = d.GetElementById("left-column");
                        var form = d.GetElementByClassNameForElement(leftColumn, "col-md-12");
                        var tag = d.GetElementByTagName(form, "p");
                        var text = tag.Text;

                        if (text.Equals("Thank you for filling in the form."))
                        {
                            result = "Pass: Health Science Library: Submit Library Search Request successful ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Thank you not displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                        // Journal Articles test
                        var rightColumn = d.GetRightColumn();
                        var accordion = d.GetElementByIdForElement(rightColumn, "accordion-details-related");
                        var articles = d.GetElementByClassNameForElement(accordion, "marginB-40");
                        var button = d.IsClassNameDisplayed(articles, "btn-library");
                        if (button)
                        {
                            result = "Pass: Health Science Library: Journal button Displayed ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Journal Button not displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }

                        var journalText = d.IsDisplayedByTagElement(articles, "p");
                        if (journalText)
                        {
                            result = "Pass: Health Science Library: Journal Text Displayed ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Journal Text Not displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                        // helpful resources
                        var relatedHeader = d.GetElementByClassNameForElement(accordion, "related-links-header")
                            .Displayed;
                        if (relatedHeader)
                        {
                            result = "Pass: Health Science Library: Helpful Resources displayed ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Helpful resources not displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }

                        var ul = d.GetElementByClassNameForElement(accordion, "ul-landing-related");
                        var li = d.IsDisplayedByTagElement(ul, "li");
                        if (li)
                        {
                            result = "Pass: Health Science Library: Helpful Resources contains links ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Helpful resources do not contain links. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: failed to load Library Search Request Health Science. <br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                }
                catch (Exception e)
                {
                    result = "Fail: An exception occurred :: Library Search Request" + e.Message ;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }
                #endregion
                #region Request Journal Articles
                try
                {
                    lis = d.GetNavIconsLis();
                    lis.ElementAt(3).Click();
                    
                    var buttons = d.GetElementsByClassName("btn-library");
                    buttons.Skip(1).First().Click();
                    if (d.GetElementByClassName("pp-header").Text == "Request Journal Articles")
                    {
                        result = "Pass: Request Journal Articles loaded successfully ";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");

                        // Name
                        d.SendKeysById("Name", name);
                        // Email
                        d.SendKeysById("Email", email);
                        // Profession
                        var element = d.GetElementById("SelectedProfession");
                        var se = d.GetSelectElement(element);
                        se.SelectByText(journalProfession);

                        // Dept
                        d.SendKeysById("Dept", dept);
                        // Location
                        element = d.GetElementById("SelectedLocation");
                        se = d.GetSelectElement(element);
                        se.SelectByText(journalLocation);

                        // Journal Request
                        d.SendKeysById("JournalArticleRequest", journalRequest);
                        d.SendKeysById("BookRequests", bookRequest);
                        
                        // Delivery Information
                        var input = d.GetElementsByName("DeliveryTypes");
                        input.ElementAt(1).Click();
                        
                        // Phone
                        d.SendKeysById("PhoneNumber",phone);
                        d.ClickByClassName("btn-default");
                        
                        var leftColumn = d.GetElementById("left-column");
                        var form = d.GetElementByClassNameForElement(leftColumn, "col-md-12");
                        var tag = d.GetElementByTagName(form, "ul");
                        var liTag = d.GetElementByTagName(tag, "li");
                        var text = liTag.Text;

                        if (text.Equals("Thank you for your submission!"))
                        {
                            result = "Pass: Health Science Library: Submit Library Search Request successful ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: No thank you displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                        // Journal Articles test
                        var rightColumn = d.GetRightColumn();
                        var accordion = d.GetElementByIdForElement(rightColumn, "accordion-details-related");
                        var articles = d.GetElementByClassNameForElement(accordion, "marginB-40");
                        var button = d.IsClassNameDisplayed(articles, "btn-library");
                        if (button)
                        {
                            result = "Pass: Health Science Library: Library Search Request Button Displayed ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Library Search Request Button not displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }

                        var libraryText = d.IsDisplayedByTagElement(articles, "p");
                        if (libraryText)
                        {
                            result = "Pass: Health Science Library: Library Request Text Displayed ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Library Request  Not displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                        // helpful resources
                        var relatedHeader = d.GetElementByClassNameForElement(accordion, "related-links-header")
                            .Displayed;
                        if (relatedHeader)
                        {
                            result = "Pass: Health Science Library: Helpful Resources displayed ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Helpful resources not displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }

                        var ul = d.GetElementByClassNameForElement(accordion, "ul-landing-related");
                        var li = d.IsDisplayedByTagElement(ul, "li");
                        if (li)
                        {
                            result = "Pass: Health Science Library: Helpful Resources contains links ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: Helpful resources do not contain links. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                    }
                    else
                    {
                        result = "Fail: failed to load Library Search Request Health Science. <br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                }
                catch (Exception e)
                {
                    result = "Fail: An exception occurred :: Library Search Request" + e.Message ;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }
                #endregion
            }
            catch (Exception e)
            {
                result = "Fail: An exception occurred in Health Science" + e.Message ;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
            }
        }
    }
}
