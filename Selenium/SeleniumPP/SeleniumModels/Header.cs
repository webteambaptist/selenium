﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class Header
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        public Header(Driver driver, Mail mail)
        {
            d = driver;
            configs = new Configs();
            m = mail;
        }

        public void TestHeader()
        {
            var result = "Starting Header Link Checks";
            m.AddToBody(result += "<br/>");
            try
            {
                d.LoadSite(configs.GetHomeUrl());
                var navbar = d.GetInnerNavbar();
                var dropdowns = d.GetElementsByClassNameForElement(navbar, "dropdown");
                if (dropdowns.Count <= 0) return;
                var ddCounter = 0;
                var linkCounter = 0;
                while(ddCounter<dropdowns.Count)
                {
                    // click drop down
                    dropdowns.ElementAt(ddCounter).Click();
                    // get drop down 
                    var dd = d.GetElementByClassNameForElement(dropdowns.ElementAt(ddCounter), "dropdown-content");
                    // get links
                    var links = d.GetLinksByElement(dd);
                    linkCounter = 0; // reset it
                    while(linkCounter<links.Count)
                    {
                        // click link
                        links.ElementAt(linkCounter).Click();
                        if (d.GetNumTabs() > 1)
                        {
                            // lets get the title of the window
                            var title = d.GetTabTitle();
                            if (title != null)
                            {
                                // link opened successfully
                                result = "Pass: " + title + " site opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            
                            // we will need to close the tab
                            d.SwitchTabs();
                            linkCounter++;
                        }
                        else
                        {
                            var headerName = d.GetElementByClassName("pp-header");
                            if (d.IsClassNameDisplayed("pp-header"))
                            {
                               result = "Pass: " + headerName.Text + " link opened successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "******************************************************** <br/>";
                                result += "Fail: " + headerName.Text + " Link didn't open. <br/>";
                                result += "******************************************************** <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }

                            if (!d.IsIdDisplayed("MainPanel"))
                            {
                                d.BackPage();
                            }
                            navbar = d.GetInnerNavbar();
                            dropdowns = d.GetElementsByClassNameForElement(navbar, "dropdown");
                            // click drop down
                            dropdowns.ElementAt(ddCounter).Click();
                            dd = d.GetElementByClassNameForElement(dropdowns.ElementAt(ddCounter), "dropdown-content");
                            // get links
                            links = d.GetLinksByElement(dd);
                            linkCounter++;
                        }
                    }
                    ddCounter++;
                }
               
            }
            catch (Exception e)
            {
                result = "******************************************************** <br/>";
                result += "EXCEPTION: Exception occurred while testing HeaderLinks. <br/>";
                result += "******************************************************** <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
            }
        }
    }
}
