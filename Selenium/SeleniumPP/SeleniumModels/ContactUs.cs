﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using NLog.Layouts;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class ContactUs
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly System system;
        public ContactUs(Driver driver, Mail m, System system)
        {
            configs = new Configs();
            this.system = system;
            this.m = m;
            d = driver;
        }
        public void TestContactUs()
        {
            var result = "Starting to test Contact Us Form";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result += "<br/>");
            d.NavigateToUrl(configs.GetHomeUrl());
            try
            {
                var footer = d.GetElementByClassName("footer-ul-1li");
                var link = d.GetLinkByElementOrderedByHref(footer);
                link.Click();

                if (d.IsClassNameDisplayed("pp-header"))
                {
                    result = "Pass: ContactUs Displayed successful ";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");

                    //d.SendKeysByName("FirstName", "Tammi");
                    var contactFormValues = configs.getContactFormValues();

                    // firstname
                    d.SendKeysByName(contactFormValues[0][0], contactFormValues[0][1]);
                    // lastname
                    d.SendKeysByName(contactFormValues[1][0], contactFormValues[1][1]);
                    // email
                    d.SendKeysByName(contactFormValues[2][0], contactFormValues[2][1]);
                    // subject
                    d.SendKeysByName(contactFormValues[3][0], contactFormValues[3][1]);
                    // Comments
                    d.SendKeysByName(contactFormValues[4][0], contactFormValues[4][1]);
                    d.ClickById("btnSubmit");

                    var textLeft = d.GetElementByClassName("text-left");
                    
                    if (textLeft.Text.Equals("Thank you for your submission. A physician portal administrator will review your submission and get back to you. Sincerely, Baptist Jax Portal Admin"))
                    {
                        result += "Passed Contact Us Test";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result += "<br/>");
                    }
                    else
                    {
                        result += "Failed to contact us page TEST";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                }
                else
                {
                    result += "Failed to display contact us page";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }

            }
            catch (Exception e)
            {
                result = "EXCEPTION: Exception occurred while testing GetHelp. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
            }
        }
    }
}
