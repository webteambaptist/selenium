﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;
using SeleniumPP.Models;

namespace SeleniumPP.SeleniumModels
{
    public class Configs
    {
        private IConfigurationRoot _config;
        private string mailTo;
        private string from;
        private string loginUrl;
        private string homeUrl;
        private readonly string username;
        private string password;
        private SearchValues searchValues;
        private string fapSearch;
        private string fapSpecialty;
        private string fapGender;
        private string fapLanguage;
        private string fapAffiliation;
        private string fapLocationMiles;
        private string fapLocationZip;
        private string name;
        private string email;
        private string profession;
        private string dept;
        private string location;
        private string year;
        private string phone;
        private string searchRequest;
        private string journalRequest;
        private string bookRequest;
        private string journalProfession;
        private string journalLocation;
        private string contactUsFirstName;
        private string contactUsLastName;
        private string contactUsEmail;
        private string contactUsSubject;
        private string contactUsComments;

        
        public Configs()
        {
            var directory = Directory.GetCurrentDirectory();
            var builder = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("appsettings.json", true, true);
            _config = builder.Build();
            mailTo = _config.GetSection("EmailSetting").GetSection("MailTo").Value;
            from = _config.GetSection("EmailSetting").GetSection("From").Value;
            loginUrl = _config.GetSection("AppSettings").GetSection("PP_URL").Value;
            homeUrl = _config.GetSection("AppSettings").GetSection("HOME_URL").Value;
            username = _config.GetSection("Login").GetSection("username").Value;
            password = _config.GetSection("Login").GetSection("password").Value;
            searchValues = new SearchValues
            {
                all = _config.GetSection("Search").GetSection("All").Value,
                people = _config.GetSection("Search").GetSection("People").Value,
                events = _config.GetSection("Search").GetSection("Events").Value,
                articles = _config.GetSection("Search").GetSection("Articles").Value,
                documents = _config.GetSection("Search").GetSection("Documents").Value,
                pages = _config.GetSection("Search").GetSection("Pages").Value
            };
            fapSearch = _config.GetSection("FAP").GetSection("Search").Value;
            fapSpecialty = _config.GetSection("FAP").GetSection("Specialty").Value;
            fapGender = _config.GetSection("FAP").GetSection("Gender").Value;
            fapLanguage = _config.GetSection("FAP").GetSection("Language").Value;
            fapAffiliation = _config.GetSection("FAP").GetSection("Affiliation").Value;
            fapLocationMiles = _config.GetSection("FAP").GetSection("Location").GetSection("Miles").Value;
            fapLocationZip = _config.GetSection("FAP").GetSection("Location").GetSection("Zip").Value;
            name = _config.GetSection("Library Search Request").GetSection("Name").Value;
            email = _config.GetSection("Library Search Request").GetSection("Email").Value;
            profession = _config.GetSection("Library Search Request").GetSection("Profession").Value;
            dept = _config.GetSection("Library Search Request").GetSection("Dept").Value;
            location = _config.GetSection("Library Search Request").GetSection("Location").Value;
            year = _config.GetSection("Library Search Request").GetSection("Year").Value;
            phone = _config.GetSection("Library Search Request").GetSection("Phone").Value;
            searchRequest = _config.GetSection("Library Search Request").GetSection("Search Request").Value;
            journalRequest = _config.GetSection("Request Journal Articles").GetSection("Journal Request").Value;
            bookRequest = _config.GetSection("Request Journal Articles").GetSection("Book Request").Value;
            journalProfession = _config.GetSection("Request Journal Articles").GetSection("Profession").Value;
            journalLocation = _config.GetSection("Request Journal Articles").GetSection("Location").Value;
            contactUsFirstName = _config.GetSection("Contact Us").GetSection("FirstName").Value;
            contactUsLastName = _config.GetSection("Contact Us").GetSection("LastName").Value;
            contactUsEmail = _config.GetSection("Contact Us").GetSection("Email").Value;
            contactUsSubject = _config.GetSection("Contact Us").GetSection("Subject").Value;
            contactUsComments = _config.GetSection("Contact Us").GetSection("Comments").Value;
        }

        public string[][] getContactFormValues()
        {
            string[][] contactUs = new string[4][];
            contactUs[0][0] = "FirstName";
            contactUs[0][1] = this.contactUsFirstName;
            contactUs[1][0] = "LastName";
            contactUs[1][1] = this.contactUsLastName;
            contactUs[2][0] = "Email";
            contactUs[2][1] = this.contactUsEmail;
            contactUs[3][0] = "Subject";
            contactUs[3][1] = this.contactUsSubject;
            contactUs[4][0] = "Comments";
            contactUs[4][1] = this.contactUsComments;

            return contactUs;
        }
        public string GetJournalRequest()
        {
            return journalRequest;
        }
        public string GetBookRequest()
        {
            return bookRequest;
        }
        public string GetJournalProfession()
        {
            return journalProfession;
        }
        public string GetJournalLocation()
        {
            return journalLocation;
        }
        public string GetUserName()
        {
            return username;
        }
        public string GetPassword()
        {
            return password;
        }
        public string GetMailTo()
        {
            return mailTo;
        }
        public string GetLoginUrl()
        {
            return loginUrl;
        }
        public string GetHomeUrl()
        {
            return homeUrl;
        }
        public string GetFrom()
        {
            return from;
        }
        public SearchValues GetSearchValues()
        {
            return searchValues;
        }
        public string GetFapSpecialty()
        {
            return fapSpecialty;
        }
        public string GetFapSearch()
        {
            return fapSearch;
        }

        public string GetFapGender()
        {
            return fapGender;
        }

        public string GetFapLanguage()
        {
            return fapLanguage;
        }

        public string GetFapAffiliation()
        {
            return fapAffiliation;
        }
        public string GetFapLocationMiles()
        {
            return fapLocationMiles;
        }
        public string GetFapLocationZip()
        {
            return fapLocationZip;
        }
        public string GetName()
        {
            return name;
        }
        public string GetEmail()
        {
            return email;
        }

        public string GetProfession()
        {
            return profession;
        }
        public string GetDept()
        {
            return dept;
        }

        public string GetLocation()
        {
            return location;
        }
        public string GetYear()
        {
            return year;
        }
        public string GetPhone()
        {
            return phone;
        }

        public string GetSearchRequest()
        {
            return searchRequest;
        }
    }

    public class SearchValues
    {
        internal string all;
        internal string people;
        internal string events;
        internal string articles;
        internal string documents;
        internal string pages;
    }
}
