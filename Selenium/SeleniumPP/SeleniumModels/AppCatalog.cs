﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class AppCatalog
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly System system;
        public AppCatalog(Driver driver, Mail m, System system)
        {
            configs = new Configs();
            this.system = system;
            this.m = m;
            this.d = driver;
        }

        public void TestAppCatalog()
        {
            var result = "Starting TestAppCatalog";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result += "<br/>");
            try
            {
                d.NavigateToUrl(configs.GetHomeUrl());
                Task.Delay(10000).Wait();
                var rightColumn = d.GetRightColumn();
                var exTabs = d.GetElementByIdForElement(rightColumn, "exTab1");
                var tabContent = d.GetElementByClassNameForElement(exTabs, "tab-content");
                var oneA = d.GetElementByIdForElement(tabContent, "one-a");
                var flexDiv = d.GetElementByClassNameForElement(oneA, "appcenter-flex-div");
                var apps = d.GetElementsByClassNameForElement(flexDiv, "appcenter-flex-child");
      
                var basisEmr = apps.ElementAt(0);

                var link = d.GetElementByTagName(basisEmr, "a");
                link.Click();
                
                var downloaded = system.CheckFileDownloaded(".ica");

                if (downloaded)
                {
                    result = "Pass: Ica file downloaded successfully";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
                else
                {
                    result = "Fail: ICA file didn't download from App Catalog. <br/>";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }

                result = "Pass: App Catalog Tests completed successful ";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }
            catch (Exception e)
            {
                result = "EXCEPTION: Exception occurred while testing Search. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
            }

        }
    }
}
