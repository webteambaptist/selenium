﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Org.BouncyCastle.Bcpg;
using SeleniumCommon;
using SeleniumPP.Models;

namespace SeleniumPP.SeleniumModels
{
    public class FAP
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly System system;
        public FAP(Driver driver, Mail m, System system)
        {
            configs = new Configs();
            this.system = system;
            this.m = m;
            d = driver;
        }

        public void TestFap()
        {
            var result = "Starting Testing FAP";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result += "<br/>");
            try
            {
                Task.Delay(10000).Wait();
                var lis = d.GetNavIconsLis();
                lis.ElementAt(2).Click();
                
                var form = d.GetElementById("fap");
                var input = configs.GetFapSearch();
                d.SendKeysByClassName(form, "ui-autocomplete-input", input);
                d.ClickReturnByClassName("ui-autocomplete-input");
                d.wait.Until(wd =>
                {
                    try
                    {
                        return d.IsClassNameDisplayed("fap-btn");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickByClassName("fap-btn");

                d.wait.Until(wd =>
                {
                    try
                    {
                        return d.IsClassNameDisplayed("fap-result");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                if (d.IsClassNameDisplayed("fap-result"))
                {
                    result = "Pass: FAP results Displayed successful ";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");

                    #region Profile

                    try
                    {
                        var urls = d.GetElementsByClassName("dr-url");
                        urls.First().Click();
                        d.wait.Until(wd =>
                        {
                            try
                            {
                                return d.IsClassNameDisplayed("profile-h1");
                            }
                            catch (Exception)
                            {
                                return false;
                            }
                        });
                        if (d.IsClassNameDisplayed("profile-h1"))
                        {
                            result = "Pass: FAP Profile Displayed successful ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");

                            var header = d.GetElementByClassName("profile-h1").Text;

                            if (header.Contains("Deborah Abram"))
                            {
                                result = "Pass: FAP Profile For Deborah Abrams Displayed successful ";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }

                            var bio = d.GetElementByClassName("toggle-bio");
                            if (d.IsElementDisplayed(bio))
                            {
                                bio.Click();
                                
                                var bioDetails = d.GetElementByClassName("hidden-biography");
                                if (d.IsElementDisplayed(bioDetails))
                                {
                                    result = "Pass: FAP Profile Bio Details Displayed successful ";
                                    Console.WriteLine(result);
                                    Logger.Info(result);
                                    m.AddToBody(result + "<br/>");

                                    if (bioDetails.Text.Contains(
                                        "is a medical doctor associated with Baptist Health who's specialties include"))
                                    {
                                        result = "Pass: FAP Bio Details for " + input + " Displayed successful ";
                                        Console.WriteLine(result);
                                        Logger.Info(result);
                                        m.AddToBody(result + "<br/>");
                                    }
                                    else
                                    {
                                        result = "Fail: FAP BioDetails for " + input + " failed to display. <br/>";
                                        Console.WriteLine(result);
                                        m.AddToBody(result += "<br/>");
                                    }
                                }
                                else
                                {
                                    result = "Fail: FAP BioDetails Failed to display. <br/>";
                                    Console.WriteLine(result);
                                    Logger.Error(result);
                                    m.AddToBody(result += "<br/>");
                                }
                            }
                            else
                            {
                                result = "Fail: Incorrect FAP Profile displayed. <br/>";
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result += "<br/>");
                            }
                        }
                        else
                        {
                            result = "Fail: FAP Profile did not Display. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                        d.BackPage();
                    }
                    catch (Exception ex)
                    {
                        result = "EXCEPTION :: Exception occurred in :: Profile ::. <br/>";
                        result += ex.Message;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                }
                else
                {
                    result = "Fail: results not displayed after Search FAP. <br/>";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }

                result = "FAP Search for Deborah Abram Completed";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                #endregion

                #region Filters

                #region Specialty Filter

                try
                {
                    // go back to FAP page and start over
                    lis = d.GetNavIconsLis();
                    lis.ElementAt(2).Click();
                    Task.Delay(10000).Wait();
                    // get drop down for Specialty
                    var specialty = d.GetElementByName("specialties_pipe");
                    var selectElement = d.GetSelectElement(specialty);
                    var specialtyConfig = configs.GetFapSpecialty();
                    selectElement.SelectByValue(specialtyConfig);
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    // verify all results are specialty selecte
                    var results = d.GetFapResults();
                    var match = true;

                    var resultCounter = 0;
                    while (resultCounter < results.Count)
                    {
                        var element = results.ElementAt(resultCounter);
                        var infoLeft = d.GetDrInfoLeft(element);
                        var drSpec = d.GetElementByClassNameForElement(infoLeft, "dr-specialty");
                        if (drSpec.Text != specialtyConfig)
                        {
                            match = false;
                        }

                        resultCounter++;
                        results = d.GetFapResults();
                    }
                    if (!match)
                    {
                        result = "Fail: FAP Specialty Filter Failed: Non Specialty results. <br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                    else
                    {
                        result = "Pass: FAP Specialty Filter Test successful ";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Specialty Filter ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result += "<br/>");
                }

                #endregion

                #region Gender Filter

                try
                {
                    d.ClickFapResetButton();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    // get list of radio options
                    var gender = d.GetElementById("gender");
                    var gen = configs.GetFapGender();
                    d.ClickByCssSelectorForElement(gender, "input[value='" + gen + "']");
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    // check to make sure all results are females
                    var results = d.GetFapResults();

                    var match = true;
                    var resultCounter = 0;
                    while (resultCounter < results.Count)
                    {
                        var element = results.ElementAt(resultCounter);
                        var infoLeft = d.GetDrInfoLeft(element);

                        var genderElement = d.GetElementByClassNameForElement(infoLeft, "dr-gender").Text;
                        if (genderElement != gen)
                        {
                            match = false;
                        }

                        resultCounter++;
                        results = d.GetFapResults();
                    }

                    if (!match)
                    {
                        result = "Fail: FAP Gender Filter Failed: More than selected Gender showed in results. <br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                    else
                    {
                        result = "Pass: FAP Gender Filter Test successful ";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Gender Filter ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }

                #endregion

                #region Language Filter

                try
                {
                    d.ClickFapResetButton();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var languages = d.GetElementByName("languages_pipe");
                    var selectElement = d.GetSelectElement(languages);
                    var language = configs.GetFapLanguage();
                    selectElement.SelectByValue(language);
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });

                    // verify all results are specialty selected
                    var results = d.GetFapResults();
                    var match = true;
                    var resultCounter = 0;
                    while (resultCounter < results.Count)
                    {
                        var element = results.ElementAt(resultCounter);
                        var infoLeft = d.GetDrInfoLeft(element);
                        var drLanguage = infoLeft.Text;
                        if (!drLanguage.Contains(language))
                        {
                            match = false;
                        }

                        resultCounter++;
                        results = d.GetFapResults();
                    }
                    if (!match)
                    {
                        result = "Fail: FAP Language Filter Failed: Non Language results. <br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                    else
                    {
                        result = "Pass: FAP Language Filter Test successful ";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Language Filter ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }


                #endregion

                #region Hospital Filter

                try
                {
                    //reset filters
                    d.ClickFapResetButton();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var ul = d.GetFapPagination();
                    var last = d.GetElementByClassNameForElement(ul, "last");
                    var a = d.GetLinkByElementOrderedByHref(last);
                    a.Click();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var results = d.GetFapResults();
                    var lastProvider = results.Last();
                    var resultNum = d.GetElementByClassNameForElement(lastProvider, "result-num");
                    var resultText = resultNum.Text;
                    var sub = resultText.Substring(0, resultText.Length - 1); // strip off the .
                    var beforeCount = int.Parse(sub);

                    var hospitals = d.GetElementByName("hospitalaffiliations_pipe");
                    var selectElement = d.GetSelectElement(hospitals);
                    var affiliation = configs.GetFapAffiliation();
                    selectElement.SelectByValue(affiliation);
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });

                    // ul
                    ul = d.GetFapPagination();
                    last = d.GetElementByClassNameForElement(ul, "last");
                    a = d.GetLinkByElementOrderedByHref(last);
                    a.Click();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    results = d.GetFapResults();
                    lastProvider = results.Last();
                    resultNum = d.GetElementByClassNameForElement(lastProvider, "result-num");
                    resultText = resultNum.Text;
                    sub = resultText.Substring(0, resultText.Length - 1); // strip off the .
                    var afterCount = int.Parse(sub);

                    if (beforeCount > afterCount)
                    {
                        result = "Pass: FAP Hospital Filter Test successful ";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                    else
                    {
                        result = "Fail: FAP Hospital Filter Failed: Non Hospital results. <br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Hospital Filter ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }


                #endregion

                #region Location Filter

                try
                {
                    d.ClickFapResetButton();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var checkbox = d.GetElementById("location-filter");
                    checkbox.Click();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    if (d.IsClassNameDisplayed("location-controls"))
                    {
                        var dist = d.GetElementByName("distance");
                        var selectElement = d.GetSelectElement(dist);
                        var miles = configs.GetFapLocationMiles();
                        selectElement.SelectByValue(miles);
                        d.wait.Until(wd =>
                        {
                            try
                            {
                                return !d.IsClassNameDisplayed("submit-progress");
                            }
                            catch (Exception)
                            {
                                return false;
                            }
                        });
                        var zip = configs.GetFapLocationZip();
                        d.SendKeysById("zip", zip);
                        d.ClickById("apply-zip-btn");
                        
                        d.wait.Until(wd =>
                        {
                            try
                            {
                                return !d.IsClassNameDisplayed("submit-progress");
                            }
                            catch (Exception)
                            {
                                return false;
                            }
                        });

                        // loop through and make sure the miles are <= 5 milese
                        // verify all results are specialty selected
                        var results = d.GetFapResults();
                        var match = false;
                        var resultCounter = 0;
                        while (resultCounter < results.Count)
                        {
                            var element = results.ElementAt(resultCounter);
                            var row = d.GetElementByClassNameForElement(element, "row");
                            var mile = d.GetElementByXPathForElement(row,
                                "//div[@class='col-xs-4 col-xs-pull-8 col-sm-8 col-sm-push-4 col-md-push-0 col-md-pull-0 col-md-2']");
                            var distance = d.GetElementByClassNameForElement(mile, "distance-value");
                            var value = distance.Text;
                            value = value.Substring(0, value.Length - 2);
                            value = value.Trim();
                            if (float.Parse(value) > int.Parse(miles))
                            {
                                match = true;
                                break;
                            }

                            resultCounter++;
                            results = d.GetFapResults();
                        }

                        if (match)
                        {
                            result = "Fail: FAP Location Filter Failed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                        else
                        {
                            result = "Pass: FAP Location Filter Test successful ";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Location Filter ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }
                #endregion
                
                #endregion
                #region GetDirections
                try
                {
                    d.ClickFapResetButton();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return !d.IsClassNameDisplayed("submit-progress");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var results = d.GetFapResults();
                    // only going to check 1 result
                    var element = results.ElementAt(0);
                    var infoRight = d.GetDrInfoRight(element);
                    var address = new Address
                    {
                        address1 = d.GetElementByXPathForElement(infoRight, "//div/span[@class='dr-address-1']")
                            .Text,
                        address2 = d.GetElementByXPathForElement(infoRight, "//div/span[@class='dr-address-2']")
                            .Text,
                        city = d.GetElementByXPathForElement(infoRight, "//div/span[@class='dr-city']").Text,
                        state = d.GetElementByXPathForElement(infoRight, "//div/span[@class='dr-state']").Text,
                        zip = d.GetElementByXPathForElement(infoRight, "//div/span[@class='dr-zip']").Text
                    };
                    
                    d.GetElementByClassNameForElement(results.ElementAt(0), "dr-get-directions").Click();
                    d.SwitchNewTabs();
                    d.wait.Until(wd =>
                    {
                        try
                        {
                            return d.IsClassNameDisplayed("tactile-searchbox-input");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var searchBox = d.GetElementsByClassName("tactile-searchbox-input");
                    //second search box (destination)
                    var destination = d.GetAttributeByElement(searchBox.ElementAt(1), "aria-label");
                    //var destination = d.GetElementByTagName(searchBox, "aria-label");
                    var elems = destination.Split(",");
                    var streetAddress = elems[0].Replace("Dr", "Drive")
                        .Replace("Ave", "Avenue")
                        .Replace("Rd", "Road")
                        .Replace("Pl", "Place")
                        .Replace("St", "Street")
                        .Replace("Blvd", "Boulevard")
                        .Replace("Ave S", "Avenue South");
                    if (streetAddress.Contains(address.address1))
                    {
                        result = "Pass: FAP GetDirections Street Address matches ";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                    else
                    {
                        result = "Fail: FAP GetDirections Did not open correct map. <br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }
                    d.SwitchTabs();
                }
                catch (Exception e)
                {
                    result = "EXCEPTION :: Exception occurred in :: Get Directions ::. <br/>";
                    result += e.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }
                #endregion
            }
            catch (Exception e)
            {
                result = "EXCEPTION: Exception occurred while testing FAP. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
            }
        }
    }
}
