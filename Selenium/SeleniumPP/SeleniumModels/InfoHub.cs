﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class InfoHub
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly System system;
        public InfoHub(Driver driver, Mail m, System system)
        {
            configs = new Configs();
            this.system = system;
            this.m = m;
            d = driver;
        }
        public void TestInfoHub()
        {
            var result = "Starting InfoHub Tests";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            d.NavigateToUrl(configs.GetHomeUrl());
            try
            {
                #region Date
                var leftColumn = d.GetElementById("left-column"); 
                var infoHubs = d.GetElementsByIdForElement(leftColumn, "info-hub-div");
                var infoCounter = 0;
                while (infoCounter < infoHubs.Count)
                {
                    try
                    {
                        // does row exist
                        var hub = infoHubs.ElementAt(infoCounter);
                        var rowExist = d.GetElementByClassNameForElement(hub, "row").Displayed;
                        if (rowExist)
                        {
                            result = "Pass: InfoHub content Displayed";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: InfoHub content not displayed. <br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                        hub = infoHubs.ElementAt(infoCounter);
                        // link test
                        var link = d.GetElementByTagName(hub, "a");
                        var linkUrl =d.GetAttributeByElement(link, "href");
                        link.Click();
                        var tabs = d.GetNumTabs();
                        if (tabs > 1)
                        {
                            d.SwitchNewTabs();
                        }

                        var url = d.GetUrl();
                        if (url == linkUrl)
                        {
                            result = "Pass: InfoHub link passed " + url;
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Fail: InfoHub link didn't pass . <br/>" + linkUrl + "<br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result += "<br/>");
                        }
                        if (tabs > 1)
                        {
                            d.SwitchTabs();
                        }
                        else
                        {
                            d.BackPage();
                        }
                        leftColumn = d.GetElementById("left-column"); 
                        infoHubs = d.GetElementsByIdForElement(leftColumn, "info-hub-div");
                        infoCounter++;
                    }
                    catch (Exception e)
                    {
                        result = "EXCEPTION: InfoHub link didn't pass. <br/>";
                        result += e.Message;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result += "<br/>");
                    }

                    
                    
                }
                
                #endregion
                //#region tasklist

                //leftColumn = d.GetElementById("left-column");
                //var task = d.GetElementByClassNameForElement(leftColumn, "blue-ribbon").Text;
                
                //if (task.Contains("Task List"))
                //{
                //    result = "Pass: InfoHub Task List Displayed";
                //    Console.WriteLine(result);
                //    m.AddToBody(result + "<br/>");
                //}
                //else
                //{
                //    result = "Fail: InfoHub Task List not displayed. <br/>";
                //    Console.WriteLine(result);
                //    m.AddToBody(result += "<br/>");
                //}
                //#endregion

                result = "Pass: InfoHub Tests completed successful ";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }
            catch (Exception e)
            {
                result = "EXCEPTION: Exception occurred while testing InfoHub. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
            }
            
        }
        
    }
}