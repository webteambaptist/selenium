﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class Search
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        public Search(Driver driver, Mail mail)
        {
            d = driver;
            configs = new Configs();
            m = mail;
        }
        public void ValidateSearch()
        {
            var result = "Search :: ValidateSearch started";
            var searchValues = configs.GetSearchValues();
            m.AddToBody(result + "<br/>");
            try
            {
                if (d.IsClassNameDisplayed("gs-filter"))
                {
                    d.ClickByClassName("gs-filter");

                    // get search drop down
                    var dropdown = d.GetElementByClassName("dropdown-menu");
                    var options = d.GetElementsByTagName(dropdown, "li");

                    #region ALL

                    try
                    {
                        var all = options.Where(x => x.Text == "All").First();
                        
                        all.Click();
                        d.SendKeysById("gs-input", searchValues.all);
                        d.ClickByClassName("gs-submit");

                        d.wait.Until(x =>
                        {
                            try
                            {
                                return d.IsClassNameDisplayed("show-on-results");
                            }
                            catch (Exception)
                            {
                                return false;
                            }
                        });
                        var results = d.GetElementById("results");
                        if (results.Text.ToUpper().Contains("ERROR"))
                        {
                            result = searchValues.all + " was entered.<br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "All Search Test Successful!";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Exception occurred testing All " + e.Message + "<br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                    }
                    #endregion
                    d.ClickByClassName("gs-filter");
                    dropdown = d.GetElementByClassName("dropdown-menu");
                    options = d.GetElementsByTagName(dropdown, "li");
                    #region People

                    try
                    {
                        var people = options.Where(x => x.Text == "People").First();
                        people.Click();
                        d.ClearElementById("gs-input");
                        d.SendKeysById("gs-input", searchValues.people);
                        d.ClickByClassName("gs-submit");

                        d.wait.Until(x =>
                        {
                            try
                            {
                                return d.IsClassNameDisplayed("show-on-results");
                            }
                            catch (Exception)
                            {
                                return false;
                            }
                        });
                        var results = d.GetElementById("results");
                        if (results.Text.ToUpper().Contains("ERROR"))
                        {
                            result = searchValues.people + " was entered.<br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "People Search Test Successful!";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Exception occurred testing People " + e.Message + "<br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                    }
                    #endregion
                    d.ClickByClassName("gs-filter");
                    dropdown = d.GetElementByClassName("dropdown-menu");
                    options = d.GetElementsByTagName(dropdown, "li");
                    #region Events
                    try
                    {
                        var events = options.Where(x => x.Text == "Events").First();
                        events.Click();
                        d.ClearElementById("gs-input");
                        d.SendKeysById("gs-input", searchValues.events);
                        d.ClickByClassName("gs-submit");

                        var results = d.GetElementById("results");
                        if (results.Text.ToUpper().Contains("ERROR"))
                        {
                            result = "ERROR :: EVENTS + .<br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Events Search Test Successful!";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Exception occurred testing Events " + e.Message + "<br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                    }
                    #endregion
                    d.ClickByClassName("gs-filter");
                    dropdown = d.GetElementByClassName("dropdown-menu");
                    options = d.GetElementsByTagName(dropdown, "li");
                    #region Articles
                    try
                    {
                        var articles = options.Where(x => x.Text == "Articles").First();
                        articles.Click();
                        d.ClearElementById("gs-input");
                        d.SendKeysById("gs-input", searchValues.articles);
                        d.ClickByClassName("gs-submit");

                        var results = d.GetElementById("results");
                        if (results.Text.ToUpper().Contains("ERROR"))
                        {
                            result += "ERROR :: " + searchValues.articles + " was entered.<br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "All Articles Test Successful!";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Exception occurred testing Articles " + e.Message + "<br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                    }
                    #endregion
                    d.ClickByClassName("gs-filter");
                    dropdown = d.GetElementByClassName("dropdown-menu");
                    options = d.GetElementsByTagName(dropdown, "li");
                    #region Documents
                    try
                    {
                        var documents = options.Where(x => x.Text == "Documents").First();
                        documents.Click();
                        d.ClearElementById("gs-input");
                        d.SendKeysById("gs-input", searchValues.documents);
                        d.ClickByClassName("gs-submit");

                        var results = d.GetElementById("results");
                        if (results.Text.ToUpper().Contains("ERROR"))
                        {
                            result += "ERROR :: " + searchValues.documents + " was entered.<br/>";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Documents Search Test Successful!";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Exception occurred testing Documents " + e.Message + "<br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                    }
                    #endregion
                    d.ClickByClassName("gs-filter");
                    dropdown = d.GetElementByClassName("dropdown-menu");
                    options = d.GetElementsByTagName(dropdown, "li");
                    #region Pages
                    try
                    {
                        var pages = options.Where(x => x.Text == "Pages").First();
                        pages.Click();
                        d.ClearElementById("gs-input");
                        d.SendKeysById("gs-input", searchValues.pages);
                        d.ClickByClassName("gs-submit");

                        var results = d.GetElementById("results");
                        if (results.Text.ToUpper().Contains("ERROR"))
                        {
                            result += "ERROR :: " + searchValues.pages + " was entered.<br/>";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        else
                        {
                            result = "Pages Search Test Successful!";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Exception occurred testing Pages " + e.Message + "<br/>";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                    }
                    #endregion
                    d.LoadSite(configs.GetHomeUrl());
                }
            }
            catch (Exception e)
            {
                result = "EXCEPTION: Exception occurred while testing Search. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
            }

        }
    }
}
