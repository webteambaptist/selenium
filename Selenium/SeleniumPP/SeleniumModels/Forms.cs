﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using NLog;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class Forms
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly System system;
        public Forms(Driver driver, Mail m, System system)
        {
            configs = new Configs();
            this.system = system;
            this.m = m;
            d = driver;
        }

        public void TestForms()
        {
            var result = "Starting testing Forms";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result += "<br/>");
            try
            {
                var lis = d.GetNavIconsLis();
                lis.ElementAt(1).Click();

                var element = d.GetElementByClassName("content-text");
                var isTitleDisplayed = d.IsDisplayedByTagElement(element, "h1");
                if (!isTitleDisplayed)
                {
                    result = "Fail: Forms DT Power Plans title not displayed. <br/>";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }

                var title = d.GetElementByTagName(element, "h1");
                if (title.Text != "Downtime Forms/PowerPlans")
                {
                    result = "Fail: Forms Page didn't display properly ";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return;
                }
                result = "Pass: Forms site opened successful ";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                // test all ul links
                // wait for page to load
                d.wait.Until(x =>
                {
                    try
                    {
                        return d.IsClassNameDisplayed("content-text");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                var content = d.GetElementByClassName("content-text");
                var uls = d.GetElementsByTagName(content, "ul");
                
                var ulCounter = 0;
                result = "**** Starting UL Links **** ";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
                while (ulCounter < uls.Count)
                {
                    try
                    {
                        // for each ul, let's get lis
                        var links = d.GetElementsByTagName(uls.ElementAt(ulCounter), "li");
                        var liCounter = 0;
                        while (liCounter < links.Count)
                        {
                            try
                            {
                                var linkText = d.GetAttributeByElement(links.ElementAt(liCounter), "href");
                                // is there a link
                                var isLink = d.IsDisplayedByTagElement(links.ElementAt(liCounter), "a");
                                if (isLink)
                                {
                                    // get link
                                    var link = d.GetElementByTagName(links.ElementAt(liCounter), "a");
                                    var linkName = d.GetAttributeByElement(link, "href");
                                    link.Click();

                                    // tab opened
                                    if (d.GetNumTabs() > 1)
                                    {
                                        d.SwitchNewTabs();
                                        var headingExist = d.IsClassNameDisplayed("pp-header");
                                        if (headingExist)
                                        {
                                            var heading = d.GetElementByClassName("pp-header");
                                            result = "Pass: " + heading.Text + " opened successfully ";
                                            Console.WriteLine(result);
                                            Logger.Info(result);
                                            m.AddToBody(result + "<br/>");
                                        }
                                        else
                                        {
                                            result = "Fail: " + d.GetUrl() + "<br/>";
                                            Console.WriteLine(result);
                                            Logger.Error(result);
                                            m.AddToBody(result += "<br/>");
                                        }

                                        d.SwitchTabs(); // go back to original tab and close this one
                                    }
                                    // tab didn't open
                                    else
                                    {

                                        // check to see if it's a download link
                                        var downloaded = system.CheckFileDownloaded(".pdf");
                                        if (downloaded)
                                        {
                                            result = "Pass: " + link.Text + " downloaded PDF successfully ";
                                            Console.WriteLine(result);
                                            Logger.Info(result);
                                            m.AddToBody(result + "<br/>");
                                        }
                                        // it's not a file
                                        else
                                        {
                                            var url = d.GetUrl();
                                            var match = url.Equals(linkText);
                                            if (match)
                                            {
                                                result = "Pass: " + linkText + " opened successfully ";
                                                Console.WriteLine(result);
                                                Logger.Info(result);
                                                m.AddToBody(result + "<br/>");
                                            }
                                            else
                                            {
                                                // not a pp page so let's just validate hte url is valid
                                                if (linkName == d.GetUrl())
                                                {
                                                    result = "Pass: " + linkName + " opened successfully ";
                                                    Console.WriteLine(result);
                                                    Logger.Info(result);
                                                    m.AddToBody(result + "<br/>");
                                                }
                                                else
                                                {
                                                    result = "Fail: " + d.GetUrl() + "<br/>";
                                                    Console.WriteLine(result);
                                                    Logger.Error(result);
                                                    m.AddToBody(result += "<br/>");
                                                }

                                            }
                                            d.BackPage();
                                        }
                                    }
                                }

                                liCounter++;
                                lis = d.GetNavIconsLis();
                                lis.ElementAt(1).Click();
                                content = d.GetElementByClassName("content-text");
                                uls = d.GetElementsByTagName(content, "ul");
                                links = d.GetElementsByTagName(uls.ElementAt(ulCounter), "li");
                            }
                            catch (Exception e)
                            {
                                result = "FAIL: Exception occurred checking li links " + e.Message;
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result + "<br/>");
                                liCounter++;
                                lis = d.GetNavIconsLis();
                                lis.ElementAt(1).Click();
                                content = d.GetElementByClassName("content-text");
                                uls = d.GetElementsByTagName(content, "ul");
                                links = d.GetElementsByTagName(uls.ElementAt(ulCounter), "li");
                            }
                        }
                        ulCounter++;
                        lis = d.GetNavIconsLis();
                        lis.ElementAt(1).Click();
                        content = d.GetElementByClassName("content-text");
                        uls = d.GetElementsByTagName(content, "ul");
                    }
                    catch (Exception e)
                    {
                        result = "FAIL: Exception occurred checking ul links " + e.Message;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                        ulCounter++;
                        content = d.GetElementByClassName("content-text");
                        uls = d.GetElementsByTagName(content, "ul");
                    }
                }

                // test all ps links
                lis = d.GetNavIconsLis();
                lis.ElementAt(1).Click();

                content = d.GetElementByClassName("content-text");
                var ps = d.GetElementsByTagName(content, "p");
                var pCounter = 0;
                result = "**** Starting P Links **** ";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
                while (pCounter < ps.Count)
                {
                    try
                    {
                        var links = d.GetLinksByElement(ps.ElementAt(pCounter));
                        var linkCounter = 0;
                        while (linkCounter < links.Count)
                        {
                            var linkText = d.GetAttributeByElement(links.ElementAt(linkCounter), "href");
                            var name = links.ElementAt(linkCounter).Text;
                           // var aHref = d.GetElementByLinkText()
                            links.ElementAt(linkCounter).Click();
                             // new tab opened
                            if (d.GetNumTabs() > 1)
                            {
                                d.SwitchNewTabs();
                                var headingExist = d.IsClassNameDisplayed("pp-header");
                                if (headingExist)
                                {
                                    var heading = d.GetElementByClassName("pp-header");
                                    result = "Pass: " + heading.Text + " opened successfully ";
                                    Console.WriteLine(result);
                                    Logger.Info(result);
                                    m.AddToBody(result + "<br/>");
                                }
                                else
                                {
                                    result = "Fail: " + d.GetUrl() + "<br/>";
                                    Console.WriteLine(result);
                                    Logger.Error(result);
                                    m.AddToBody(result += "<br/>");
                                }

                                d.SwitchTabs(); // go back to original tab and close this one
                            }
                            // not a new tab
                            else
                            {
                                // check to see if it's a download link
                                var downloaded = system.CheckFileDownloaded(".pdf");
                                if (downloaded)
                                {
                                    result = "Pass: " + name + " downloaded PDF successfully ";
                                    Console.WriteLine(result);
                                    Logger.Info(result);
                                    m.AddToBody(result + "<br/>");
                                }
                                // not a pdf download
                                else
                                {
                                    var url = d.GetUrl();
                                    var match = url.Equals(linkText);
                                    if (match)
                                    {
                                        result = "Pass: " + linkText + " opened successfully ";
                                        Console.WriteLine(result);
                                        Logger.Info(result);
                                        m.AddToBody(result + "<br/>");
                                    }
                                    else
                                    {
                                        result = "Fail: " + d.GetUrl() + "<br/>";
                                        Console.WriteLine(result);
                                        Logger.Error(result);
                                        m.AddToBody(result += "<br/>");
                                    }

                                    d.BackPage();
                                }
                            }

                            linkCounter++;
                            lis = d.GetNavIconsLis();
                            lis.ElementAt(1).Click();
                            content = d.GetElementByClassName("content-text");
                            ps = d.GetElementsByTagName(content, "p");
                            links = d.GetElementsByTagName(ps.ElementAt(pCounter),"a");
                        }

                        pCounter++;
                        lis = d.GetNavIconsLis();
                        lis.ElementAt(1).Click();
                        content = d.GetElementByClassName("content-text");
                        ps = d.GetElementsByTagName(content, "p");
                    }
                    catch (Exception e)
                    {
                        result = "FAIL: Exception occurred checking p links " + e.Message;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                        pCounter++;
                        ps = d.GetElementsByTagName("p"); // go on to the next one
                    }
                }
            }
            catch (Exception e)
            {
                result = "Fail: An exception occurred in Getting Forms " + e.Message ;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
            }
        }
    }   
}
