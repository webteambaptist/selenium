﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class GetHelp
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly System system;
        public GetHelp(Driver driver, Mail m, System system)
        {
            configs = new Configs();
            this.system = system;
            this.m = m;
            d = driver;
        }

        public void TestGetHelp()
        {
            var result = "TestGetHelp Started";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result += "<br/>");
            d.NavigateToUrl(configs.GetHomeUrl());
            try
            {
                var rightColumn = d.GetRightColumn();
                var h3 = d.GetElementByClassNameForElement(rightColumn, "marginT-40");
                
                // test Get Help exists
                var helpText = h3.Text;
                if (helpText.Equals("Get Help"))
                {
                    result = "Pass: GetHelp Section Displayed successful ";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
                else
                {
                    result = "Fail: Get Help section missing. <br/>";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                }
                result = "Pass: GetHelp Tests completed successful ";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }
            catch (Exception e)
            {
                result = "EXCEPTION: Exception occurred while testing GetHelp. <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
            }

        }
    }
}
