﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class Informatics
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        public Informatics(Driver driver, Mail mail)
        {
            d = driver;
            configs = new Configs();
            m = mail;
        }

        public void TestInfomatics()
        {
            var result = "Infomatics Test Started";
            m.AddToBody(result += "");
            try
            {
                var lis = d.GetNavIconsLis();
                

                if (!d.IsClassNameDisplayed("pp-header")) return;
                var header = d.GetElementByClassName("pp-header");
                if (!header.Text.Equals("Physician Informatics Team")) return;
                result = "Pass: Informatics site opened successful ";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                // Is headding for Page there
                var contentText = d.GetElementByClassName("content-text");
                if (string.IsNullOrEmpty(contentText.Text))
                {
                    result = "Fail: Informatics Content at top of Page doesn't have text";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                }

                var providerCount = 0;
                var providers = d.GetInformaticsProviders();
                while (providerCount < providers.Count)
                {
                    var fail = false;
                    try
                    {
                        var provider = providers.ElementAt(providerCount);
                        // does Name show up
                        var name = d.GetElementByIdForElement(provider, "title");
                        var nameText = name.Text;
                        if (string.IsNullOrEmpty(nameText))
                        {
                            result = "Fail: Informatics Provider is missing name";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                            fail = true;
                        }
                        // does Title show up
                        var title = d.GetElementByIdForElement(provider, "heading");
                        if (string.IsNullOrEmpty(title.Text))
                        {
                            result = "Fail: Informatics Content at top of Page doesn't have text (" + nameText + ")" ;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                            fail = true;
                        }

                        // does photo appear
                        var photo = d.GetElementByClassNameForElement(name, "img-responsive");
                        if (!photo.Displayed)
                        {
                            result = "Fail: Informatics photo is missing (" + nameText + ")" ;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                            fail = true;
                        }
                        // does description show up
                        var desc = d.GetElementByIdForElement(provider, "content");
                        if (!desc.Displayed)
                        {
                            result = "Fail: Informatics description missing (" + nameText + ")" ;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                            fail = true;
                        }
                        else
                        {
                            var linkExists = d.IsDisplayedByTagElement(desc, "a");

                            if (linkExists)
                            {

                                d.GetElementByTagName(desc, "a").Click();

                                var docHeading = d.GetElementById("doc-heading");

                                if (string.IsNullOrEmpty(docHeading.Text))
                                {
                                    result = "Fail: Informatics Provider Profile is missing";
                                    Console.WriteLine(result);
                                    Logger.Error(result);
                                    m.AddToBody(result + "<br/>");
                                    fail = true;
                                }

                                var profileTitle = d.GetElementById("doc-title");

                                if (string.IsNullOrEmpty(profileTitle.Text))
                                {
                                    result = "Fail: Informatics Provider Profile is missing name";
                                    Console.WriteLine(result);
                                    Logger.Error(result);
                                    m.AddToBody(result + "<br/>");
                                    fail = true;
                                }

                                var photoDiv = d.GetElementByTagName(profileTitle, "img");
                                if (!photoDiv.Displayed)
                                {
                                    result = "Fail: Informatics Profile photo is missing (" + nameText + ")";
                                    Console.WriteLine(result);
                                    Logger.Error(result);
                                    m.AddToBody(result + "<br/>");
                                    fail = true;
                                }

                                var descDiv = d.GetElementById("doc-content");
                                var paragraphs = d.GetElementsByTagName(descDiv, "p");
                                var textPassed = paragraphs.Any(x => !string.IsNullOrEmpty(x.Text));
                                if (!textPassed)
                                {
                                    result = "Fail: Informatics description missing (" + nameText + ")";
                                    Console.WriteLine(result);
                                    Logger.Error(result);
                                    m.AddToBody(result + "<br/>");
                                    fail = true;
                                }

                                // documents
                                var documentSection = d.GetElementById("document-section");
                                var isDocHeading = d.IsDisplayedByTagElement(documentSection, "h3");

                                if (isDocHeading)
                                {
                                    // if there are documents lets make sure there are some
                                    var docDisplayed = d.IsIdDisplayed("documents");
                                    if (docDisplayed)
                                    {
                                        // are there any documents
                                        var documents = d.GetElementsByTagName(d.GetElementById("documents"), "a")
                                            .Count;
                                        if (documents == 0)
                                        {
                                            result = "Fail: Informatics Profile documents missing (" + nameText + ")";
                                            Console.WriteLine(result);
                                            Logger.Error(result);
                                            m.AddToBody(result + "<br/>");
                                            fail = true;
                                        }
                                    }
                                }

                                if (!fail)
                                {
                                    result = "PASS :: Informatics Provider passed ("+nameText + ")";
                                    Console.WriteLine(result);
                                    Logger.Info(result);
                                    m.AddToBody(result + "<br/>");
                                }
                            }
                            else
                            {
                                result = "Fail: Informatics description missing (" + nameText + ")" ;
                                Console.WriteLine(result);
                                Logger.Error(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }

                        d.BackPage();
                        providerCount++;
                        providers = d.GetInformaticsProviders();
                    }
                    catch (Exception e)
                    {
                        result = "Fail: Informatics Provider failed with exception " + e.Message ;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                                
                        d.BackPage();
                        providerCount++;
                        providers = d.GetInformaticsProviders();
                    }
                }
            }
            catch (Exception e)
            {
                result = "******************************************************** <br/>";
                result += "EXCEPTION: Exception occurred while testing Informatics. <br/>";
                result += "******************************************************** <br/>";
                result += e.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
            }
        }
    }
}