﻿using System;
using NLog;
using SeleniumCommon;

namespace SeleniumPP.SeleniumModels
{
    public class Authentication
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs configs;
        private readonly Mail m;
        public Authentication(Driver driver, Mail mail)
        {
            d = driver;
            configs = new Configs();
            m = mail;
        }
        public bool Login()
        {
            var result = "Authentication :: Login started"; 
            m.AddToBody(result + "<br/>");
            try
            {
                d.ClickById("loginButton");
                d.wait.Until(wd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("ContentPlaceHolder1_MFALoginControl1_UserIDView_txtUserid_UiInput");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.SendKeysById("ContentPlaceHolder1_MFALoginControl1_UserIDView_txtUserid_UiInput", configs.GetUserName());
                d.SendKeysById("ContentPlaceHolder1_MFALoginControl1_UserIDView_tbxPassword_UiInput", configs.GetPassword());
                
                d.ClickByClassName("btn-primary");
                d.wait.Until(wd =>
                {

                    try
                    {
                        return d.IsDisplayedByName("logout");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                if (d.IsDisplayedByName("logout"))
                {
                    result = "Pass: Login successful";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");

                    return true;
                }
                
                result = "******************************************************** <br/>";
                result += "Fail: Unable to login";
                result += "******************************************************** <br/>";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result += "<br/>");
                return false;
            }
            catch (Exception ex)
            {
                result = "******************************************************** <br/>";
                result += "EXCEPTION: Issue Logging In-- " + DateTime.Now;
                result += "******************************************************** <br/>";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
        }
        public bool Logout()
        {
            var result = "Authentication :: Logout";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            try
            {
                d.ClickByName("logout");

                if (d.IsDisplayedByName("logout"))
                {
                    result = "******************************************************** <br/>";
                    result += "Fail: Unable to Logout";
                    result += "******************************************************** <br/>";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result += "<br/>");
                    return false;
                }
                result = "Pass: Logout successful";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return true;

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("no such element"))
                {
                    result = "Pass: Logout successful";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return true;
                }
                result = "******************************************************** <br/>";
                result += "EXCEPTION: Issue Logging Out-- " + DateTime.Now;
                result += "******************************************************** <br/>";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
        }
    }
}
