﻿using SeleniumBPP.Models;
using System;
using System.Net.Mail;
using NLog;
using SeleniumBPP.SeleniumModels;


namespace SeleniumBPP
{
    class Program
    {
        private static Logger Logger = null;
        private static void Main()
        {
            var system = new SeleniumModels.System();
            var configs = system.GetConfigs();
            var driver = system.GetDriver();
            var m = system.GetMail();
            try
            {
                Logger= NLog.LogManager.GetCurrentClassLogger();
                var config = new NLog.Config.LoggingConfiguration();
                var logFile = new NLog.Targets.FileTarget("logFile")
                {
                    FileName = $"Logs\\SeleniumBPP-{DateTime.Today:MM-dd-yy}.log"
                };

                config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
                LogManager.Configuration = config;

                m.AddToBody("Regression test for site: " + configs.bppUrl + "<br/>");
                Console.WriteLine("Regression test for site: " + configs.bppUrl);
                Logger.Info("Regression test for site: " + configs.bppUrl);
                var success = true;
                
                Console.WriteLine("Starting Baptist Physician Partners Application Regression Testing.");
                Logger.Info("Starting Baptist Physician Partners Application Regression Testing.");
                driver.LoadSite(configs.bppUrl);
                
                var newMember = new NewMember(driver, configs, m);
                system.RemoveDataFromDb(newMember);// remove from db before running in case it wasn't cleaned up before
                success = newMember.MemberTest();
                var legalEntityGuid = newMember.GetLegalEntityGuid();
             
                var result = "------------------------------------";
                Console.WriteLine(result);
                m.AddToBody(result);
                
                if (success)
                {
                    driver.LoadSite(configs.bppUrl);
                    var returnMember = new ReturningMember(driver, configs, m, legalEntityGuid);
                    success = returnMember.MemberTest();
                }
                
                result = "------------------------------------";
                Console.WriteLine(result);
                m.AddToBody(result);

                var survey = new Survey(driver, configs, m);
                if (success)
                {
                    success = survey.SurveyTest();
                }
                result = "------------------------------------";
                Console.WriteLine(result);
                m.AddToBody(result);

                if (success)
                {
                    var admin = new Admin(driver, configs, m);
                    success = admin.AdminTest();
                }

                result = "------------------------------------";
                Console.WriteLine(result);
                m.AddToBody(result);


                if (success)
                {
                    success = survey.SurveyArchiveList();
                }

                result = "------------------------------------";
                Console.WriteLine(result);
                m.AddToBody(result);
               
                if (!success)
                {
                    result = "Fail: Test Failed (Read full Log)-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error("Fail: Test Failed (Read full Log)-- " + DateTime.Now);
                    m.AddToBody(result + "<br/>");

                }

                system.RemoveDataFromDb(newMember);
                
                var sendMail = new SendMail();
                
                MailMessage mm = new MailMessage(m.GetFrom(), m.GetTo());
                mm.IsBodyHtml = true;
                mm.Body = m.GetBody();
                mm.Subject = m.GetSubject();
                sendMail.SendMailMessage(mm);
                
                result = "If Console does not close automatically in a few seconds, you may now close it.";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result);

                driver.CloseDriver();
                Environment.Exit(0);
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred in Program " + e.Message);
                driver.CloseDriver();
                Environment.Exit(1);
            }
        }
    }
}
