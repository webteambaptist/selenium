﻿using System;
using System.Net.Mail;
using System.Collections.Generic;
using System.Text;

namespace SeleniumBPP
{
    public class Mail
    {
        private string Subject { get;set; }
        private bool IsBodyHtml { get; set; }
        private string Body { get; set; }
        private string To { get; set;}
        private string From { get; set; }

        public Mail(string subject, string to, string from)
        {
            Subject = subject;
            IsBodyHtml = true;
            To = to;
            From = from;
        }

        public string GetFrom()
        {
            return this.From;
        }
        public string GetTo()
        {
            return this.To;
        }

        public string GetSubject()
        {
            return Subject;
        }
        public void AddToBody(string text)
        {
            Body += text;
        }

        public string GetBody()
        {
            return Body;
        }
    }
}
