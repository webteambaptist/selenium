﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using NLog;
using SeleniumBPP.Models;
using SeleniumCommon;

namespace SeleniumBPP.SeleniumModels
{
    public class System
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver driver;
        private readonly Configs configs;
        private readonly Mail m;
        
        public System()
        {
            if (driver != null) return;
            driver = new Driver();
            configs = new Configs();
            m = new Mail("BPP Regression Testing", configs.mailTo, configs.@from);
        }
        public Driver GetDriver()
        {
            return driver;
        }

        public Configs GetConfigs()
        {
            return configs;
        }
        public Mail GetMail()
        {
            return m;
        }
        public void RemoveDataFromDb(NewMember newMember)
        {
            var result = "RemoveDataFromDB :: Page testing completed. Now removing data from db.";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            using var bppContext = new BPPContext();
            var newMemberGuid = newMember.GetNewMemberGuid();
            var legalEntityGuid = newMember.GetLegalEntityGuid();
            try
            {

                var newMemberApplication = bppContext.NewMemberApplication.FirstOrDefault(x => x.Guid == newMemberGuid && x.Email == configs.email);
                if (newMemberApplication != null)
                {
                    bppContext.NewMemberApplication.Remove(newMemberApplication);
                    bppContext.SaveChanges();
                    result = "Pass :: RemoveDataFromDB :: NotMemberApplication Table item has been removed from db";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
                else
                {
                    result = "RemoveDataFromDB :: There is no data to remove from the NewMemberApplication Table";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: RemoveDataFromDB :: Issue deleting item from NotMemberApplication table";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
            }

            //Returning Member Table
            try
            {
                var returningMember = bppContext.ReturningMemberApplication.Where(x => x.Email == configs.email).ToList();
                if (returningMember.Count > 0)
                {
                    foreach (var entry in returningMember)
                    {
                        bppContext.ReturningMemberApplication.Remove(entry);
                    }
                    bppContext.SaveChanges();

                    result = "Pass :: RemoveDataFromDB :: ReturningMemberApplication Table item has been removed from db";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
                else
                {
                    result = "RemoveDataFromDB :: There is no data to remove from the NewMemberApplication Table";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: RemoveDataFromDB :: Issue deleting item from NotMemberApplication table";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
            }

            //Legal Entity Table
            try
            {
                var taxId = bppContext.LegalEntity.First(x => x.TaxId == configs.taxId);
                if (taxId != null)
                {
                    bppContext.LegalEntity.Remove(taxId);
                    bppContext.SaveChanges();
                    result = "Pass :: RemoveDataFromDB :: LegalEntity Table item has been removed from db (TaxId)";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }

                var legalEntity = bppContext.LegalEntity.FirstOrDefault(x => x.LegalEntityId == legalEntityGuid);
                if (legalEntity != null)
                {
                    bppContext.LegalEntity.Remove(legalEntity);
                    bppContext.SaveChanges();

                    result = "Pass :: RemoveDataFromDB :: LegalEntity Table item has been removed from db (Legal Entity ID)";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
                else
                {
                    result = "RemoveDataFromDB :: There is no data to remove from the LegalEntity Table";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: RemoveDataFromDB :: Issue deleting item from LegalEntity table";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
            }

            //Practice Table
            var practices = bppContext.Practice.Where(x => x.LegalEntityId == legalEntityGuid).ToList();
            try
            {
                if (practices.Count > 0)
                {
                    foreach (var practice in practices)
                    {
                        //Location Table
                        var practiceGuid = practice.PracticeGuid;
                        var locations = bppContext.Location.Where(x => x.PracticeGuid == practiceGuid).ToList();
                        try
                        {
                            if (locations.Count>0)
                            {
                                foreach (var location in locations)
                                {
                                    bppContext.Location.Remove(location);
                                    bppContext.SaveChanges();
                                }
                                result = "Pass :: RemoveDataFromDB :: Location Table item has been removed from db";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "RemoveDataFromDB :: There is no data to remove from the Locations Table";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: RemoveDataFromDB :: Issue deleting item(s) from Locations table";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }
                        bppContext.Practice.Remove(practice);
                        bppContext.SaveChanges();

                        result = "Pass :: RemoveDataFromDB :: Practice item has been removed from db";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                else
                {
                    result = "RemoveDataFromDB :: There is no data to remove from the Practice Table";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: RemoveDataFromDB :: Issue deleting item(s) from Practice table";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
            }

            var p = new List<Providers>();

            var provider1 = bppContext.Providers.FirstOrDefault(x => x.Npinumber == configs.provider1Npi);
            if (provider1 != null)
            {
                p.Add(provider1);
                result = "RemoveDataFromDB :: A Provider has been added for removal: " + provider1.FirstName + " " + provider1.LastName;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }

            var provider2 = bppContext.Providers.FirstOrDefault(x => x.Npinumber == configs.provider2Npi);
            if (provider2 != null)
            {
                p.Add(provider2);
                result = "RemoveDataFromDB :: A Provider has been added for removal: " + provider2.FirstName + " " + provider2.LastName;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }

            var provider3 = bppContext.Providers.FirstOrDefault(x => x.Npinumber == configs.provider3Npi);
            if (provider3 != null)
            {
                p.Add(provider3);
                result = "RemoveDataFromDB :: A Provider has been added for removal: " + provider3.FirstName + " " + provider3.LastName;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }

            var provider4 = bppContext.Providers.FirstOrDefault(x => x.Npinumber == configs.provider4Npi);
            if (provider4 != null)
            {
                p.Add(provider4);
                result = "RemoveDataFromDB :: A Provider has been added for removal: " + provider4.FirstName + " " + provider4.LastName;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }

            var provider5 = bppContext.Providers.FirstOrDefault(x => x.Npinumber == configs.provider5Npi);
            if (provider5 != null)
            {
                p.Add(provider5);
                result = "RemoveDataFromDB :: A Provider has been added for removal: " + provider5.FirstName + " " + provider5.LastName;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }

            //Provider Update Submitter
            try
            {
                var providerUpdateSubmitter = bppContext.ProviderUpdateSubmitter.FirstOrDefault(x => x.SubmittersEmailAddress == configs.email);
                if (providerUpdateSubmitter != null)
                {
                    bppContext.ProviderUpdateSubmitter.Remove(providerUpdateSubmitter);
                    bppContext.SaveChanges();

                    result = "Pass :: RemoveDataFromDB :: ProviderUpdateSubmitter Table item has been removed from db";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
                else
                {
                    result = "RemoveDataFromDB :: There is no data to remove from the ProviderUpdateSubmitter Table";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: RemoveDataFromDB :: Issue deleting item from ProviderUpdateSubmitter table";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
            }

            //Provider Update Staging
            try
            {
                var providerName = configs.provider1FirstName + " " + configs.provider1LastName;
                var providerUpdateStaging = bppContext.ProviderUpdateStaging.FirstOrDefault(x => x.ProviderName == providerName);
                if (providerUpdateStaging != null)
                {
                    bppContext.ProviderUpdateStaging.Remove(providerUpdateStaging);
                    bppContext.SaveChanges();

                    result = "Pass :: RemoveDataFromDB :: ProviderUpdateStaging Table item has been removed from db";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
                else
                {
                    result = "RemoveDataFromDB :: There is no data to remove from the ProviderUpdateStaging Table";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: RemoveDataFromDB :: Issue deleting item from ProviderUpdateStaging table";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
            }

            if (p.Count == 0)
            {
                result = "RemoveDataFromDB :: There are no Providers to remove at this time.";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }
            else
            {
                RemoveProviderData(p);
            }
        }
        
        private void RemoveProviderData(List<Providers> p)
        {
            var result = "";
            foreach (var provider in p)
            {
                using var bppContext = new BPPContext();
                if (provider != null)
                {
                    try
                    {
                        //Providers Table
                        try
                        {
                            bppContext.Providers.Remove(provider);
                            bppContext.SaveChanges();

                            result = "Pass :: RemoveProviderData :: Provider Table item has been removed from db";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: RemoveProviderData :: Issue deleting item(s) from Providers table";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }

                        //Provider Practice Matrix Table
                        try
                        {
                            var ppm = bppContext.ProviderPracticeMatrix.Where(x => x.Npinumber == provider.Npinumber).ToList();
                            if (ppm.Count > 0)
                            {
                                foreach (var item in ppm)
                                {
                                    bppContext.ProviderPracticeMatrix.Remove(item);
                                    bppContext.SaveChanges();
                                }
                                result = "Pass :: RemoveProviderData :: ProviderPracticeMatrix item(s) has been removed from db";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "RemoveProviderData :: No data to remove in ProviderPracticeMatrix Table";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: RemoveProviderData :: Issue deleting item(s) from ProviderPractiveMatrix table";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }

                        //Provider Ancillary Table
                        try
                        {
                            var pa = bppContext.ProviderAncillary.Where(x => x.Npinumber == provider.Npinumber).ToList();
                            if (pa.Count > 0)
                            {
                                foreach (var item in pa)
                                {
                                    bppContext.ProviderAncillary.Remove(item);
                                    bppContext.SaveChanges();
                                }
                                result = "Pass :: RemoveProviderData :: ProviderAncillary item(s) has been removed from db";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "RemoveProviderData :: No data to remove in ProviderAncillary Table";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: RemoveProviderData :: Issue deleting item(s) from ProviderAncillary table";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }

                        //Provider Facilities Table
                        try
                        {
                            var pf = bppContext.ProviderFacilities.Where(x => x.Npinumber == provider.Npinumber).ToList();
                            if (pf.Count > 0)
                            {
                                foreach (var item in pf)
                                {
                                    bppContext.ProviderFacilities.Remove(item);
                                    bppContext.SaveChanges();
                                }
                                result = "Pass :: RemoveProviderData :: ProviderFacilities item(s) has been removed from db";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "RemoveProviderData :: No data to remove in ProviderFacilities Table";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: RemoveProviderData :: Issue deleting item(s) from ProviderFacilities table";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }

                        //Provider Locations Table
                        try
                        {
                            var pl = bppContext.ProviderLocations.Where(x => x.Npinumber == provider.Npinumber).ToList();
                            if (pl.Count > 0)
                            {
                                foreach (var item in pl)
                                {
                                    bppContext.ProviderLocations.Remove(item);
                                    bppContext.SaveChanges();
                                }
                                result = "Pass :: RemoveProviderData :: ProviderLocations item(s) has been removed from db";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "RemoveProviderData :: No data to remove in ProviderLocations Table";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: RemoveProviderData :: Issue deleting item(s) from ProviderLocations table";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }

                        //Provider Practice Workflow Table
                        try
                        {
                            var ppw = bppContext.ProviderPracticeWorkflow.Where(x => x.Npinumber == provider.Npinumber).ToList();
                            if (ppw.Count > 0)
                            {
                                foreach (var item in ppw)
                                {
                                    bppContext.ProviderPracticeWorkflow.Remove(item);
                                    bppContext.SaveChanges();
                                }
                                result = "Pass :: RemoveProviderData :: ProviderPracticeWorkflow item(s) has been removed from db";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "RemoveProviderData :: No data to remove in ProviderPracticeWorkflow Table";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: RemoveProviderData :: Issue deleting item(s) from ProviderPracticeWorkflow table";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }

                        //Provider Specialty Matrix Table
                        try
                        {
                            var psm = bppContext.ProviderSpecialtyMatrix.Where(x => x.Npinumber == provider.Npinumber).ToList();
                            if (psm.Count > 0)
                            {
                                foreach (var item in psm)
                                {
                                    bppContext.ProviderSpecialtyMatrix.Remove(item);
                                    bppContext.SaveChanges();
                                }
                                result = "Pass :: RemoveProviderData :: ProviderSpecialtyMatrix item(s) has been removed from db";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                            else
                            {
                                result = "RemoveProviderData :: No data to remove in ProviderPracticeWorkflow Table";
                                Console.WriteLine(result);
                                Logger.Info(result);
                                m.AddToBody(result + "<br/>");
                            }
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: RemoveProviderData :: Issue deleting item(s) from ProviderPracticeWorkflow table";
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Fail :: RemoveProviderData :: Issue deleting item from Provider tables";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");

                        result = ex.Message;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                    }
                }
                else
                {
                    result = "RemoveProviderData :: No data to remove for the provider provided";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
            }
        }
        
    }
}
