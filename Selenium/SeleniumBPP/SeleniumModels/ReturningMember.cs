﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using NLog;
using SeleniumBPP.Helper;
using SeleniumCommon;
using SeleniumBPP.Models;
namespace SeleniumBPP.SeleniumModels
{
    public class ReturningMember : Member
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        //private Guid _returningMemberGuid;
        private readonly Driver driver;
        private readonly Configs configs;
        private readonly Mail m;
        private readonly Guid legalEntityGuid;
        public ReturningMember(Driver driver, Configs configs, Mail mail, Guid legalEntityGuid) : base(driver, configs, mail)
        {
            this.driver = driver;
            this.configs = configs;
            m = mail;
            this.legalEntityGuid = legalEntityGuid;
        }

        public bool MemberTest()
        {
            var success = ReturningMemberApplication();
            if (!success) return success;
            
            // Add Practice WF
            success = ReturningMemberApplicationAddPractice();
            if (!success) return success;
            success = ReturningMemberPracticeNamesInformation();
            if (!success) return success;
            success = ReturningMemberLocationsInformation(configs.location3LocationName, configs.location3Address1, configs.location3City,
                configs.location3OfficeManagerName, configs.location3OfficeManagerPhoneNumber,
                configs.location3Phone, configs.location3Zip);
            if (!success) return success;
            success = ReturningMemberProvidersInformation(false);
            if (!success) return success;
            success = JoinderAgreementDownload();
            if (!success) return success;
            
            // Add Location WF
            success = ReturningMemberApplication();
            if (!success) return success;
            success = ReturningMemberApplicationAddLocation();
            if (!success) return success;
            success = ReturningMemberLocationsInformation(configs.location4LocationName, configs.location4Address1, configs.location4City,
                configs.location4OfficeManagerName, configs.location4OfficeManagerPhoneNumber,
                configs.location4Phone, configs.location4Zip);
            if (!success) return success;
            success = ReturningMemberProvidersInformation(false);
            if (!success) return success;
            success = JoinderAgreementDownload();
            if (!success) return success;
            
            // Add Provider WF
            success = ReturningMemberApplication();
            if (!success) return success;
            success = ReturningMemberApplicationAddProvider();
            if (!success) return success;
            success = ReturningMemberProvidersInformation(true);
            if (!success) return success;
            success = JoinderAgreementDownload();
            
            return success;
        }
        private bool ReturningMemberApplication()
        {
            configs.link = null;
            configs.returningLink = null;

            const string result = "ReturningMemberApplication :: Opening Add To My Group page.";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            driver.NavigateToUrl(configs.addToMyGroupUrl);
            driver.wait.Until(webDriver =>
            {

                try
                {
                    return driver.IsDisplayedByName("_ReturningMember.TaxID");
                }
                catch (Exception)
                {
                    return false;
                }
            });
            
            driver.SendKeysByName("_ReturningMember.TaxID",configs.taxId);
            driver.SendKeysByName("_ReturningMember.POCEmail", configs.email);
            return true;
        }
        private bool ReturningMemberApplicationAddPractice()
        {
            var result = "ReturningMemberApplicationAddPractice :: Starting Returning Member Application - Practice Workflow.";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var selectionId = 0;
            
            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("addPractice");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("addPractice");
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnSaveReturningMember");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSaveReturningMember");
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnCloseModal");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnCloseModal");
                
                using var bppContext = new BPPContext();
                selectionId = 3;

                try
                {
                    
                    var returningMember = bppContext.ReturningMemberApplication.FirstOrDefault(x => x.LeGuid == legalEntityGuid && x.AppStep == selectionId);
                    if (returningMember != null)
                    {
                        var data = returningMember;

                        try
                        {
                            var encrypted = Crypto.Encrypt(data.Guid.ToString());
                            //_returningMemberGuid = data.Guid;

                            result = "ReturningMemberApplication_AddPractice :: Retrieved Returning Member Guid";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");

                            configs.returningLink = configs.bppUrl + "/Application/GetAppStep?ActivationKey=" + encrypted + "&SelectionOption=" + selectionId;

                            result = "Pass :: ReturningMemberApplication_AddPractice :: Returning Member Application Page Submission complete -- " + DateTime.Now;
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: ReturningMemberApplication_AddPractice :: Attempting to open Practice Information page from New Member Application page.-- " + DateTime.Now;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                            return false;
                        }
                    }
                    else
                    {
                        result = "Fail :: ReturningMemberApplication_AddPractice :: Attempting to open Practice Information page from New Member Application page.-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    result = "Fail :: ReturningMemberApplication_AddPractice :: Attempting to open Practice Information page from New Member Application page.-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
            }
            catch (Exception ex)
            {
                result = "Fail: Issue submitting Returning Member Application-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }

            result = "Moving on to next page: Add Practice to Returning Member page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Error(result);
            m.AddToBody(result + "<br/>");
            return true;
        } 
        private bool ReturningMemberPracticeNamesInformation()
        {
            var result = "ReturningMemberPracticeNamesInformation :: " +
                "Opening Returning Member Practice Name(s) Information page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            driver.NavigateToUrl(configs.returningLink);
            
            var id = 0;
            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("practiceName-" + id);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.SendKeysById("practiceName-" + id, configs.practiceName2);
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnSavePracticeName");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSavePracticeName");
            }
            catch (Exception ex)
            {
                result = "Fail :: ReturningMemberPracticeNamesInformation :: Issue submitting Practice Name portion of application - Add Practice Workflow Practice Name Info";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                return false;
            }

            result = "ReturningMemberPracticeNamesInformation :: Moving on to next page: Retuning Member Location(s) page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            return true;
        }
        private bool ReturningMemberLocationsInformation(string locationName, string address1, string city,
            string officeManagerName, string officeManagerPhoneNumber, string phone, string zip)
        {
            var result = "ReturningMemberLocationsInformation :: Opening New Member Location(s) Information page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var id = 0;
            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsDisplayedByName("practicesMulti-" + id);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                var practices = driver.GetElementByName("practicesMulti-" + id);
                var selectElement = driver.GetSelectElement(practices);

                using var bppContext = new BPPContext();
                var selectedPractice = bppContext.Practice.Where(x => x.PracticeName == configs.practiceName2).OrderByDescending(x => x.CreatedDt).Select(x => x.PracticeGuid).FirstOrDefault();

                selectElement.SelectByValue(selectedPractice.ToString());
                driver.SendKeysById("locationName-" + id, locationName);
                driver.SendKeysByName("Address1-" + id, address1);
                driver.SendKeysByName("City-" + id, city);
                driver.SendKeysByName("Zip-" + id, zip);
                driver.SendKeysByName("Phone-" + id, phone);
                driver.SendKeysByName("OfficeManagerName-" + id, officeManagerName);
                driver.SendKeysByName("OfficeManagerPhoneNumber-" + id, officeManagerPhoneNumber);
                driver.SendKeysByName("OfficeManagerEmail-" + id, configs.email);
            }
            catch (Exception ex)
            {
                result = "Fail :: ReturningMemberLocationsInformation :: Issue inputting Location Information into application";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                return false;
            }

            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnSaveLocationsStep3");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSaveLocationsStep3");
            }
            catch (Exception)
            {
                var element1 = driver.GetElementById("btnSaveLocationsStep3");
                var actions1 = driver.GetActions();
                actions1.MoveToElement(element1).Click().Perform();
            }
            result = "ReturningMemberLocationsInformation :: Moving on to next page: Provider(s) page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
        private bool ReturningMemberProvidersInformation(bool loadSite)
        {

            var result = "ReturningMemberProvidersInformation :: Opening ReturningMember_PWorkflow_ProvidersInformation-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var id = 0;
            if (loadSite)
            {
                driver.NavigateToUrl(configs.returningLink);
            }
            
            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsDisplayedByName("ProviderfirstNameMulti-" + id);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.SendKeysByName("ProviderfirstNameMulti-" + id, configs.provider3FirstName);
                driver.SendKeysByName("ProviderlastNameMulti-" + id, configs.provider3LastName);
                var Degree = driver.GetElementByName("ProviderDegree-" + id);
                var degreeElement = driver.GetSelectElement(Degree);
                degreeElement.SelectByValue(configs.provider3DegreeValue);

                result = "ReturningMemberProvidersInformation :: Testing NPI Number";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                driver.SendKeysByName("ProvidernpiNumber-" + id, configs.provider3Npi);
                driver.SendKeysByName("ProviderPersonalEmail-" + id, configs.email);

                result = "ReturningMemberProvidersInformation :: NPI Number does exist in the NPPES Registry.";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                driver.SendKeysByName("ProviderPersonalPhone-" + id, configs.provider3Phone);
                try
                {
                    driver.wait.Until(webDriver =>
                    {

                        try
                        {
                            return driver.IsDisplayedByName("ProviderprimarySpecialtyMulti-" + id);
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var primarySpecialty = driver.GetElementByName("ProviderprimarySpecialtyMulti-" + id);
                    var primarySpecialtyElement = driver.GetSelectElement(primarySpecialty);
                    primarySpecialtyElement.SelectByValue(configs.provider3PrimarySpecialtyValue);
                }
                catch (Exception ex)
                {
                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = "Fail :: ReturningMemberProvidersInformation :: Exception occurred selecting Primary Specialty - Practice Workflow Provider Information";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
                //});

                using var bppContext = new BPPContext();
                var practiceNames3 = driver.GetElementByName("PracticeNames-" + id);
                var practiceNameElement3 =  driver.GetSelectElement(practiceNames3);

                var selectedPractice3 = bppContext.Practice.Where(x => x.PracticeName == configs.practiceName2).OrderByDescending(x => x.CreatedDt).Select(x => x.PracticeGuid).FirstOrDefault();

                practiceNameElement3.SelectByValue(selectedPractice3.ToString());

                try
                {
                    var primaryLocation3 = driver.GetElementByName("ProviderPrimaryLocationMulti-" + id);
                    var primaryLocationElement3 = driver.GetSelectElement(primaryLocation3);

                    var selectedLocation3 = bppContext.Location.Where(x => x.LocationName == configs.location3LocationName).OrderByDescending(x => x.CreatedDt).Select(x => x.Id).FirstOrDefault();

                    primaryLocationElement3.SelectByValue(selectedLocation3.ToString());

                    var primarySpecialty3 = driver.GetElementByName("ProviderprimarySpecialtyMulti-" + id);
                    var primarySpecialtyElement3 = driver.GetSelectElement(primarySpecialty3);
                    primarySpecialtyElement3.SelectByValue(configs.provider3PrimarySpecialtyValue);
                }
                catch (Exception ex)
                {
                    result = "Fail :: ReturningMemberProvidersInformation :: Exception occurred selecting Primary Location/Specialty 3 - Practice Workflow Provider Information";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
                
                try
                {
                    driver.wait.Until(webDriver =>
                    {

                        try
                        {
                            return driver.IsIdDisplayed("btnSaveProviders");
                        }
                        catch (Exception)
                        {
                            return true;
                        }
                    });
                    driver.ClickById("btnSaveProviders");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception  " + e.Message);
                    Logger.Error(result);
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: ReturningMemberProvidersInformation :: Exception Occurred - Practice Workflow Provider Information";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }

            result = "ReturningMemberProvidersInformation :: Moving on to next page: Joinder Agreement page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
        private bool ReturningMemberApplicationAddLocation()
        {
            var result = "ReturningMemberApplicationAddLocation :: Starting Returning Member Application - Location Workflow.";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var selectionId = 0;
            
            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("addLocation");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("addLocation");
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnSaveReturningMember");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSaveReturningMember");
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnCloseModal");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnCloseModal");

                using var bppContext = new BPPContext();
                selectionId = 4;

                try
                {
                    var returningMember = bppContext.ReturningMemberApplication.Where(x => x.LeGuid == legalEntityGuid && x.AppStep == selectionId).OrderByDescending(x => x.CreateDt).FirstOrDefault();
                    if (returningMember != null)
                    {
                        var data = returningMember;

                        try
                        {
                            var encrypted = Crypto.Encrypt(data.Guid.ToString());
                            //_returningMemberGuid = data.Guid;

                            result = "ReturningMemberApplicationAddLocation :: Retrieved Returning Member Guid";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");

                            configs.returningLink = configs.bppUrl + "/Application/GetAppStep?ActivationKey=" + encrypted + "&SelectionOption=" + selectionId;

                            result = "Pass :: ReturningMemberApplicationAddLocation :: Returning Member Application Page Submission complete -- " + DateTime.Now;
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: ReturningMemberApplicationAddLocation :: Attempting to open Practice Information page from New Member Application page. - location workflow-- " + DateTime.Now;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                            return false;
                        }
                    }
                    else
                    {
                        result = "Fail :: ReturningMemberApplicationAddLocation :: Returning Member Record not found - location workflow-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    result = "Fail :: ReturningMemberApplicationAddLocation :: Exception occurred Returning Member Application- location workflow-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: ReturningMemberApplicationAddLocation :: Exception occurred - location workflow-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            result = "ReturningMemberApplicationAddLocation :: Moving on to next page: Add Location to Returning Member page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Error(result);
            m.AddToBody(result + "<br/>");
            driver.NavigateToUrl(configs.returningLink);
            return true;
        }
        private bool ReturningMemberApplicationAddProvider()
        {
            var result = "ReturningMemberApplicationAddProvider :: Starting Returning Member Application - Provider Workflow.";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var selectionId = 0;
           
            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("addProvider");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("addProvider");
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnSaveReturningMember");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSaveReturningMember");
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnCloseModal");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnCloseModal");

                using var bppContext = new BPPContext();
                selectionId = 5;

                try
                {
                    var returningMember = bppContext.ReturningMemberApplication.FirstOrDefault(x => x.LeGuid == legalEntityGuid && x.AppStep == selectionId);
                    if (returningMember != null)
                    {
                        var data = returningMember;

                        try
                        {
                            var encrypted = Crypto.Encrypt(data.Guid.ToString());
                            //_returningMemberGuid = data.Guid;
                            result = "ReturningMemberApplicationAddProvider :: Retrieved Returning Member Guid";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");

                            configs.returningLink = configs.bppUrl + "/Application/GetAppStep?ActivationKey=" + encrypted + "&SelectionOption=" + selectionId;

                            result = "Pass :: ReturningMemberApplicationAddProvider :: Returning Member Application Page Submission complete -- " + DateTime.Now;
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");
                        }
                        catch (Exception ex)
                        {
                            result = "Fail :: ReturningMemberApplicationAddProvider :: Exception Occurred while retrieving return _link. - provider workflow-- " + DateTime.Now;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");

                            result = ex.Message;
                            Console.WriteLine(result);
                            Logger.Error(result);
                            m.AddToBody(result + "<br/>");
                            return false;
                        }
                    }
                    else
                    {
                        result = "Fail :: ReturningMemberApplicationAddProvider :: Returning Member not found. - provider workflow-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    result = "Fail :: ReturningMemberApplicationAddProvider :: Exception occurred. See exception details - provider workflow-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: ReturningMemberApplicationAddProvider :: Issue submitting Returning Member Application- provider workflow submission-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            result = "ReturningMemberApplicationAddProvider :: Moving on to next page: Add Practice to Returning Member page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Error(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
    }
}
