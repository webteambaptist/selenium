﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SeleniumBPP.SeleniumModels
{
        
    public class Configs
    {
        public IConfigurationRoot config;
        public string link;
        public string returningLink;
       
        public string bppUrl;
        public string surveyUrl;
        public string adminUrl;
        public string archiveUrl;
        public string addToMyGroupUrl;

        public string adminUsername;
        public string adminPassword;
        
        public string fortyMinutes;
        public string threeMinutes;
        public string oneMin;

        public string sleepTime;
        public string submitterName;
        public string submitterPhone;
        public string notAMemberName;
        public string email;
        public string mailTo;
        public string notAMemberUrl;
        public string legalEntityName;
        public string existingTaxId;
        public string taxId;
        public string legalEntityAddress1;
        public string legalEntityAddress2;
        public string legalEntityCity;
        public string legalEntityZip;
        public string legalEntityPocName;
        public string legalEntityPocPhoneNumber;
        public string legalEntityCapName;
        public string practiceName1;
        public string practiceName2;
        public string location1LocationName;
        public string location1Address1;
        public string location1Address2;
        public string location1City;
        public string location1Zip;
        public string location1Phone;
        public string location1OfficeManagerName;
        public string location1OfficeManagerPhoneNumber;
        public string location2LocationName;
        public string location2Address1;
        public string location2City;
        public string location2Zip;
        public string location2Phone;
        public string location2OfficeManagerName;
        public string location2OfficeManagerPhoneNumber;
        public string location3LocationName;
        public string location3Address1;
        public string location3City;
        public string location3Zip;
        public string location3Phone;
        public string location3OfficeManagerName;
        public string location3OfficeManagerPhoneNumber;
        public string location4LocationName;
        public string location4Address1;
        public string location4City;
        public string location4Zip;
        public string location4Phone;
        public string location4OfficeManagerName;
        public string location4OfficeManagerPhoneNumber;
        public string provider1FirstName;
        public string provider1MaidenName;
        public string provider1LastName;
        public string provider1Aka;
        public string provider1Npi;
        public string provider1Phone;
        public string provider1PrimarySpecialtyValue;
        public string provider1DegreeValue;
        public string provider2FirstName;
        public string provider2LastName;
        public string provider2Npi;
        public string provider2Phone;
        public string provider2PrimarySpecialtyValue;
        public string provider2DegreeValue;
        public string provider3FirstName;
        public string provider3LastName;
        public string provider3Npi;
        public string provider3Phone;
        public string provider3PrimarySpecialtyValue;
        public string provider3DegreeValue;
        public string provider4FirstName;
        public string provider4LastName;
        public string provider4Npi;
        public string provider4Phone;
        public string provider4PrimarySpecialtyValue;
        public string provider4DegreeValue;
        public string provider5FirstName;
        public string provider5LastName;
        public string provider5Npi;
        public string provider5Phone;
        public string provider5PrimarySpecialtyValue;
        public string provider5DegreeValue;

        public string surveyStatusChangeReason;
        public string surveyStatusChange;
        public string updateWorkList;
        public string masterLookupUrl;
        public string mailMicroService;
        public string from;
        public Configs()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(path:"appsettings.json", optional: true, reloadOnChange: true);
            config = builder.Build();
            bppUrl = config["AppSettings:BPP_URL"];
            surveyUrl = bppUrl + config["AppSettings:Survey_URL"];
            adminUrl = bppUrl + config["AppSettings:AdminLogin_URL"];
            archiveUrl = bppUrl + config["AppSettings:SurveyArchiveList_URL"];
            addToMyGroupUrl = bppUrl + config["AppSettings:AddToMyGroup_URL"];
            
            adminUsername = config["AppSettings:Admin:Username"];
            adminPassword = config["AppSettings:Admin:Password"];

            submitterName = config["GeneralSettings:Name"];
            submitterPhone = config["GeneralSettings:PhoneNumber"];
            notAMemberName = config["AppSettings:NotAMember_Name"];
            email = config["GeneralSettings:Email"];
            mailTo = config["GeneralSettings:EmailSetting:MailTo"];
            mailMicroService = config["GeneralSettings:EmailSetting:MailMicroService"];
            from = config["GeneralSettings:EmailSetting:From"];
            notAMemberUrl = bppUrl + config["AppSettings:NotAMember_URL"];
            legalEntityName = config["AppSettings:LegalEntity:LE_Name"];
            existingTaxId = config["AppSettings:LegalEntity:ExistingTaxID"];
            taxId = config["AppSettings:LegalEntity:TaxID"];
            legalEntityAddress1 = config["AppSettings:LegalEntity:LE_Address1"];
            legalEntityAddress2 = config["AppSettings:LegalEntity:Address2"];
            legalEntityCity = config["AppSettings:LegalEntity:LE_City"];
            legalEntityZip = config["AppSettings:LegalEntity:LE_Zip"];
            legalEntityPocName = config["AppSettings:LegalEntity:POCName"];
            legalEntityPocPhoneNumber = config["AppSettings:LegalEntity:POCPhoneNumber"];
            legalEntityCapName = config["AppSettings:LegalEntity:CAPName"];
            practiceName1 = config["AppSettings:PracticeNames:PracticeName1"];
            practiceName2 = config["AppSettings:PracticeNames:PracticeName2"];
            location1LocationName = config["AppSettings:Locations:Location1:LocationName"];
            location1Address1 = config["AppSettings:Locations:Location1:Address1"];
            location1Address2 = config["AppSettings:Locations:Location1:Address2"];
            location1City = config["AppSettings:Locations:Location1:City"];
            location1Zip = config["AppSettings:Locations:Location1:Zip"];
            location1Phone = config["AppSettings:Locations:Location1:Phone"];
            location1OfficeManagerName = config["AppSettings:Locations:Location1:OfficeManagerName"];
            location1OfficeManagerPhoneNumber = config["AppSettings:Locations:Location1:OfficeManagerPhoneNumber"];
            location2LocationName = config["AppSettings:Locations:Location2:LocationName"];
            location2Address1 = config["AppSettings:Locations:Location2:Address1"];
            location2City = config["AppSettings:Locations:Location2:City"];
            location2Zip = config["AppSettings:Locations:Location2:Zip"];
            location2Phone = config["AppSettings:Locations:Location2:Phone"];
            location2OfficeManagerName = config["AppSettings:Locations:Location2:OfficeManagerName"];
            location2OfficeManagerPhoneNumber = config["AppSettings:Locations:Location2:OfficeManagerPhoneNumber"];
            location3LocationName = config["AppSettings:Locations:Location3:LocationName"];
            location3Address1 = config["AppSettings:Locations:Location3:Address1"];
            location3City = config["AppSettings:Locations:Location3:City"];
            location3Zip = config["AppSettings:Locations:Location3:Zip"];
            location3Phone = config["AppSettings:Locations:Location3:Phone"];
            location3OfficeManagerName = config["AppSettings:Locations:Location3:OfficeManagerName"];
            location3OfficeManagerPhoneNumber = config["AppSettings:Locations:Location3:OfficeManagerPhoneNumber"];
            location4LocationName = config["AppSettings:Locations:Location4:LocationName"];
            location4Address1 = config["AppSettings:Locations:Location4:Address1"];
            location4City = config["AppSettings:Locations:Location4:City"];
            location4Zip = config["AppSettings:Locations:Location4:Zip"];
            location4Phone = config["AppSettings:Locations:Location4:Phone"];
            location4OfficeManagerName = config["AppSettings:Locations:Location4:OfficeManagerName"];
            location4OfficeManagerName = config["AppSettings:Locations:Location4:OfficeManagerName"];
            location4OfficeManagerPhoneNumber = config["AppSettings:Locations:Location4:OfficeManagerPhoneNumber"];
            provider1FirstName = config["AppSettings:Providers:Provider1:FirstName"];
            provider1MaidenName = config["AppSettings:Providers:Provider1:MaidenName"];
            provider1LastName = config["AppSettings:Providers:Provider1:LastName"];
            provider1Aka = config["AppSettings:Providers:Provider1:AlsoKnownAs"];
            provider1Npi = config["AppSettings:Providers:Provider1:ValidNPI"];
            provider1Phone = config["AppSettings:Providers:Provider1:Phone"];
            provider1PrimarySpecialtyValue = config["AppSettings:Providers:Provider1:PrimarySpecialtyValue"];
            provider1DegreeValue = config["AppSettings:Providers:Provider2:DegreeValue"];
            provider2FirstName = config["AppSettings:Providers:Provider2:FirstName"];
            provider2LastName = config["AppSettings:Providers:Provider2:LastName"];
            provider2Npi = config["AppSettings:Providers:Provider2:ValidNPI"];
            provider2Phone = config["AppSettings:Providers:Provider2:Phone"];
            provider2PrimarySpecialtyValue = config["AppSettings:Providers:Provider2:PrimarySpecialtyValue"];
            provider2DegreeValue = config["AppSettings:Providers:Provider2:DegreeValue"];
            provider3FirstName = config["AppSettings:Providers:Provider3:FirstName"];
            provider3LastName = config["AppSettings:Providers:Provider3:LastName"];
            provider3Npi = config["AppSettings:Providers:Provider3:ValidNPI"];
            provider3Phone = config["AppSettings:Providers:Provider3:Phone"];
            provider3PrimarySpecialtyValue = config["AppSettings:Providers:Provider3:PrimarySpecialtyValue"];
            provider3DegreeValue = config["AppSettings:Providers:Provider3:DegreeValue"];
            provider4FirstName = config["AppSettings:Providers:Provider4:FirstName"];
            provider4LastName = config["AppSettings:Providers:Provider4:LastName"];
            provider4Npi = config["AppSettings:Providers:Provider4:ValidNPI"];
            provider4Phone = config["AppSettings:Providers:Provider4:Phone"];
            provider4PrimarySpecialtyValue = config["AppSettings:Providers:Provider4:PrimarySpecialtyValue"];
            provider4DegreeValue = config["AppSettings:Providers:Provider4:DegreeValue"];
            provider5FirstName = config["AppSettings:Providers:Provider5:FirstName"];
            provider5LastName = config["AppSettings:Providers:Provider5:LastName"];
            provider5Npi = config["AppSettings:Providers:Provider5:ValidNPI"];
            provider5Phone = config["AppSettings:Providers:Provider5:Phone"];
            provider5PrimarySpecialtyValue = config["AppSettings:Providers:Provider5:PrimarySpecialtyValue"];
            provider5DegreeValue = config["AppSettings:Providers:Provider5:DegreeValue"];

            surveyStatusChangeReason = config["GeneralSettings:Survey:StatusChangeReason"]; ;
            surveyStatusChange = config["GeneralSettings:Survey:StatusChange"];
            updateWorkList = bppUrl + config["AppSettings:UpdateWorklist_URL"];
            masterLookupUrl = bppUrl + config["AppSettings:MasterLookup_URL"];

            
        }
}
}
