﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using SeleniumBPP.Helper;
using SeleniumCommon;
using SeleniumBPP.Models;
namespace SeleniumBPP.SeleniumModels
{
    public class NewMember : Member
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver driver;
        private readonly Configs configs;
        private readonly Mail m;
        public NewMember(Driver driver, Configs configs, Mail mail) : base(driver, configs, mail)
        {
            this.driver = driver;
            this.configs = configs;
            m = mail;
        }

        public bool MemberTest()
        {
            var success = NewMemberApplication();
            if (!success) return success;
            success = LegalEntityInformation();
            if (!success) return success;
            success = LegalEntityDownload();
            if (!success) return success;
            success = PracticeNamesInformation();
            if (!success) return success;
            success = LocationsInformation();
            if (!success) return success;
            success = ProvidersInformation();
            if (!success) return success;
            success = JoinderAgreementDownload();
            return success;
        }
        public bool NewMemberApplication()
        {
            var result = "";
            try
            {
                result = "NewMemberApplication :: " + "Opening New Member Application page.";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                driver.NavigateToUrl(configs.notAMemberUrl);

                driver.wait.Until(webDriver =>
                {
                    try
                    {
                        return driver.IsIdDisplayed("Name");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.SendKeysById("Name", configs.notAMemberName);
                driver.SendKeysById("Email", configs.email);
                Console.WriteLine("Name and Email entered " + configs.notAMemberName + " ::" + configs.email);
                Logger.Info("Name and Email entered " + configs.notAMemberName + " ::" + configs.email);
                driver.wait.Until(webDriver =>
                {
                    try
                    {
                        return driver.IsIdDisplayed("btnNewMemberRequest");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnNewMemberRequest");

                driver.wait.Until(webDriver =>
                {
                    try
                    {
                        return driver.IsIdDisplayed("btnCloseModal");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnCloseModal");
                // Now Validate the Entry 
                using var bppContext = new BPPContext();
                var data = bppContext.NewMemberApplication.FirstOrDefault(x => x.Name == configs.notAMemberName
                                                                               && x.Email == configs.email &&
                                                                               x.AppStep == 1 && x.LeGuid == null);

                if (data != null)
                {
                    try
                    {
                        var encrypted = Crypto.Encrypt(data.Guid.ToString());
                        SetNewMemberGuid(data.Guid);
                        result = "NewMemberApplication :: Retrieved New Member Guid";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");

                        configs.link = configs.bppUrl + "/Application/GetAppStep?ActivationKey=" + encrypted;
                        result = "Pass :: NewMemberApplication :: Page Submission complete -- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                    catch (Exception ex)
                    {
                        result = "Fail :: NewMemberApplication :: Attempting to open Legal Entity Information page.-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");

                        result = ex.Message;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");

                        return false;
                    }
                }
                else
                {
                    //todo: Return fail message and where it failed to the console.
                    result = "Fail :: NewMemberApplication :: Attempt to return data from db-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: NewMemberApplication :: Issue submitting New Member Application-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }

            result = "NewMemberApplication :: Completed-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
        public bool LegalEntityInformation()
        {
            var result = "LegalEntityInformation :: Opening Group/Legal Entity Information page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            try
            {
                driver.NavigateToUrl(configs.link);
            }
            catch (Exception ex)
            {
                result = "Fail :: LegalEntityInformation :: The _link you are attempting to access is not valid.-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                return false;
            }
            try
            {
                driver.wait.Until(webDriver =>
                {
                    try
                    {
                        return driver.IsElementDisplayed("_Entity.LE_Name");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.SendKeysByName("_Entity.LE_Name",configs.legalEntityName);
                
                result = "Testing existing TIN Number";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                driver.SendKeysByName("_Entity.TaxID", configs.existingTaxId);
                var taxId = driver.GetAttributeByName("_Entity.TaxID", "value");

                using (var context = new BPPContext())
                {
                    var leData = context.LegalEntity.FirstOrDefault(x => x.TaxId == taxId);

                    if (leData != null)
                    {
                        result = "TIN already exists in db";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");

                        result = "LegalEntityInformation :: Removing existing TIN for this would have prevented user from continuing to the next step in the process.";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");

                        driver.ClearElementByName("_Entity.TaxID");
                        driver.SendKeysByName("_Entity.TaxID", configs.taxId);
                        
                        Console.WriteLine("New TIN inputted");
                        Logger.Info("New TIN inputted");
                    }
                    else
                    {
                        result = "LegalEntityInformation :: TIN does not exist in db";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                        return false;
                    }
                }
                driver.SendKeysByName("_Entity.LE_Address1", configs.legalEntityAddress1);
                driver.SendKeysByName("_Entity.LE_City", configs.legalEntityCity);
                driver.SendKeysByName("_Entity.LE_Zip", configs.legalEntityZip);
                
                driver.SendKeysByName("_Entity.POCName", configs.legalEntityPocName);
                driver.SendKeysByName("_Entity.POCPhoneNumber", configs.legalEntityPocPhoneNumber);
                driver.SendKeysByName("_Entity.POCEmail", configs.email);
                
                driver.SendKeysByName("_Entity.CAPName", configs.legalEntityCapName);
                driver.SendKeysByName("_Entity.CAPAddress1", configs.legalEntityAddress1);
                driver.SendKeysByName("_Entity.CAPCity", configs.legalEntityCity);
                driver.SendKeysByName("_Entity.CAPZip", configs.legalEntityZip);

                taxId = driver.GetAttributeByName("_Entity.TaxID", "value");

                driver.wait.Until(webDriver =>
                {
                    try
                    {
                        return driver.IsIdDisplayed("btnSavePracAppStep1");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSavePracAppStep1");

                if (driver.IsIdDisplayed("btnSavePracAppStep1"))
                {
                    // it must not have submitted
                    result = "Fail :: LegalEntityInformation :: Form didn't submit. Check values";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    return false;
                }
                using var bppContext = new BPPContext();
                var data = bppContext.LegalEntity.FirstOrDefault(x => x.LeName == configs.legalEntityName && x.TaxId == taxId);
                if (data != null)
                {
                    try
                    {
                        SetLegalEntityGuid(data.LegalEntityId);
                        result = "LegalEntityInformation :: Retrieved New Legal Entity Guid";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");

                        result = "Pass :: LegalEntityInformation :: Legal Entity Submission complete-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                    catch (Exception ex)
                    {
                        result = "Fail :: LegalEntityInformation :: Issue retrieving Legal Entity Guid";
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");

                        result = ex.Message;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                        return false;
                    }
                }
                else
                {
                    //todo: Return fail message and where it failed to the console.
                    result = "Fail :: LegalEntityInformation :: Attempt to return data from db-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: LegalEntityInformation :: Issue submitting Legal Entity portion of application";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            result = "LegalEntityInformation :: Moving on to next page: Legal Entity Download page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
        public bool LegalEntityDownload()
        {
            var result = "LegalEntityDownload :: Opening Legal Entity Download page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            try
            {
                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsClassNameDisplayed("downloadbtn");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                if (!driver.IsClassNameDisplayed("downloadbtn"))
                {
                    result = "Fail :: LegalEntityDownload :: Download Button is not displayed";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    return false;
                }
                driver.ClickByClassName("downloadbtn");
                var original = driver.SwitchNewTabs();
                driver.ExecuteJs("window.close()");
                driver.SwitchBackTabs(original);

                driver.wait.Until(webDriver =>
                {
                    try
                    {
                        return driver.IsIdDisplayed("btnSavePracAppStep2");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSavePracAppStep2");
                result = "Pass :: LegalEntityDownload :: Legal Entity Download complete-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }
            catch (Exception ex)
            {
                result = "Fail :: LegalEntityDownload :: Issue downloading Legal Entity document portion of application";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                return false;
            }
            result = "LegalEntityDownload :: Moving on to next page: Practice Name(s) Information page-- " + DateTime.Now;
            Console.WriteLine(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
        public bool PracticeNamesInformation()
        {
            var result = "PracticeNamesInformation :: Opening New Member Practice Name(s) Information page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var id = 0;
            try
            {
                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsIdDisplayed("practiceName-" + id);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.SendKeysById("practiceName-" + id, configs.practiceName1);
                driver.ClickById("btnSavePracticeName");
                
                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsClassNameDisplayed("h2color");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                if (driver.GetElementByClassName("h2color").Text == "Location Information")
                {
                    using var bppContext = new BPPContext();
                    var data = bppContext.Practice.Where(x => x.LegalEntityId == GetLegalEntityGuid()).ToList();
                    if (data.Count > 0)
                    {
                        result = "PracticeNamesInformation :: Retrieved Practices that were submitted based on Legal Entity";
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");

                        result = "Pass :: PracticeNamesInformation :: Practice Name Submission complete-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                    }
                    else
                    {
                        //todo: Return fail message and where it failed to the console.
                        result = "Fail :: PracticeNamesInformation :: Attempt to return data from db-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                        return false;
                    }
                }
                else
                {
                    result = "Fail :: PracticeNamesInformation :: Practice Name Submission Failed-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }

            }
            catch (Exception ex)
            {
                result = "Fail :: PracticeNamesInformation :: Issue submitting Practice Name portion of application";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            result = "PracticeNamesInformation :: Moving on to next page: Location(s) page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
        public bool LocationsInformation()
        {
            var result = "LocationsInformation :: Opening New Member Location(s) Information page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var id = 0;
            try
            {
                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsDisplayedByName("practicesMulti-" + id);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                var practices = driver.GetElementByName("practicesMulti-" + id);
                var selectElement = driver.GetSelectElement(practices);

                using var bppContext = new BPPContext();
                var selectedPractice = bppContext.Practice.Where(x => x.PracticeName == configs.practiceName1).OrderByDescending(x => x.CreatedDt).Select(x => x.PracticeGuid).FirstOrDefault();

                selectElement.SelectByValue(selectedPractice.ToString());
                driver.SendKeysById("locationName-" + id, configs.location1LocationName);
                driver.SendKeysByName("Address1-" + id, configs.location1Address1);
                driver.SendKeysByName("City-" + id, configs.location1City);
                driver.SendKeysByName("Zip-" + id,configs.location1Zip);
                driver.SendKeysByName("Phone-" + id,configs.location1Phone);
                driver.SendKeysByName("OfficeManagerName-" + id, configs.location1OfficeManagerName);
                driver.SendKeysByName("OfficeManagerPhoneNumber-" + id, configs.location1OfficeManagerPhoneNumber);
                driver.SendKeysByName("OfficeManagerEmail-" + id, configs.email);

                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsClassNameDisplayed("add-section");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickByClassName("add-section");
                id++;

                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsDisplayedByName("practicesMulti-" + id);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                var practices2 = driver.GetElementByName("practicesMulti-" + id);
                var selectElement2 = driver.GetSelectElement(practices2);

                var selectedPractice2 = bppContext.Practice.Where(x => x.PracticeName == configs.practiceName1).OrderByDescending(x => x.CreatedDt).Select(x => x.PracticeGuid).FirstOrDefault();
                selectElement2.SelectByValue(selectedPractice2.ToString());

                driver.SendKeysById("locationName-" + id, configs.location2LocationName);
                driver.SendKeysByName("Address1-" + id,configs.location2Address1);
                driver.SendKeysByName("City-" + id,configs.location2City);
                driver.SendKeysByName("Zip-" + id,configs.location2Zip);
                driver.SendKeysByName("Phone-" + id,configs.location2Phone);
                driver.SendKeysByName("OfficeManagerName-" + id,configs.location2OfficeManagerName);
                driver.SendKeysByName("OfficeManagerPhoneNumber-" + id,configs.location2OfficeManagerPhoneNumber);
                driver.SendKeysByName("OfficeManagerEmail-" + id,configs.email);

                var element = driver.GetElementById("btnSaveLocationsStep3");
                var actions = driver.GetActions();
                actions.MoveToElement(element).Click().Perform();
            }
            catch (Exception ex)
            {
                result = "Fail :: LocationsInformation :: Issue inputting Location Information into application";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }

            var doesLocation1Match = false;
            try
            {
                using (var context = new BPPContext())
                {
                    var data = context.Practice.Where(x => x.LegalEntityId == GetLegalEntityGuid()).OrderBy(x => x.Id).FirstOrDefault();
                    if (data != null)
                    {
                        var practice1 = data.PracticeGuid;

                        var location1 = context.Location.FirstOrDefault(x => x.PracticeGuid == practice1);
                        if (location1!=null && location1.LocationName == configs.location1LocationName)
                        {
                            doesLocation1Match = true;
                        }
                    }
                    else
                    {
                        result = "Fail :: LocationsInformation :: Practice not found-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Error(result);
                        m.AddToBody(result + "<br/>");
                        return false;
                    }
                }
                if (doesLocation1Match)
                {
                    result = "LocationsInformation :: Retrieved Locations that were submitted and they do match the expected Result";
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");

                    result = "Pass :: LocationsInformation :: Locations Submission complete-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Info(result);
                    m.AddToBody(result + "<br/>");
                }
                else
                {
                    //todo: Return fail message and where it failed to the console.
                    result = "Fail :: LocationsInformation :: Attempt to return data from db-- " + DateTime.Now;
                    Logger.Error(result);
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: LocationsInformation :: Exception occurred while getting location information ( return data from db )-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }

            result = "LocationsInformation :: Moving on to next page: Provider(s) page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
        public bool ProvidersInformation()
        {
            var result = "ProvidersInformation :: Opening New Member Provider(s) Information page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var id = 0;

            try
            {
                driver.wait.Until(webDriver =>
                {
                    try
                    {
                        return driver.IsDisplayedByName("ProviderfirstNameMulti-" + id);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.SendKeysByName("ProviderfirstNameMulti-" + id,configs.provider1FirstName);
                driver.SendKeysByName("ProviderlastNameMulti-" + id,configs.provider1LastName);

                var degree = driver.GetElementByName("ProviderDegree-" + id);
                var degreeElement = driver.GetSelectElement(degree);
                degreeElement.SelectByValue(configs.provider1DegreeValue);

                result = "ProvidersInformation :: Testing NPI Number";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                driver.SendKeysByName("ProvidernpiNumber-" + id, configs.provider1Npi);

                result = "ProvidersInformation :: NPI Number does exist in the NPPES Registry.";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");

                driver.SendKeysByName("ProviderPersonalEmail-" + id, configs.email);

                driver.SendKeysByName("ProviderPersonalPhone-" + id, configs.provider1Phone);

                try
                {
                    driver.wait.Until(webDriver =>
                    {
                        
                        try
                        {
                            return driver.IsDisplayedByName("ProviderprimarySpecialtyMulti-" + id);
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var primarySpecialty = driver.GetElementByName("ProviderprimarySpecialtyMulti-" + id);
                    var primarySpecialtyElement = driver.GetSelectElement(primarySpecialty);
                    primarySpecialtyElement.SelectByValue(configs.provider1PrimarySpecialtyValue);
                }
                catch (Exception ex)
                {
                    result = "Fail :: ProvidersInformation :: Exception occurred while selecting Specialty";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }

                using var bppContext = new BPPContext();
                var practiceNames = driver.GetElementByName("PracticeNames-" + id);
                var practiceNameElement = driver.GetSelectElement(practiceNames);

                var selectedPractice = bppContext.Practice.Where(x => x.PracticeName == configs.practiceName1).OrderByDescending(x => x.CreatedDt).Select(x => x.PracticeGuid).FirstOrDefault();
               
                practiceNameElement.SelectByValue(selectedPractice.ToString());

                try
                {
                    driver.wait.Until(webDriver =>
                    {
                        
                        try
                        {
                            return driver.IsDisplayedByName("ProviderPrimaryLocationMulti-" + id);
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var primaryLocation = driver.GetElementByName("ProviderPrimaryLocationMulti-" + id);
                    var primaryLocationElement = driver.GetSelectElement(primaryLocation);

                    var selectedLocation = bppContext.Location.Where(x => x.LocationName == configs.location1LocationName).OrderByDescending(x => x.CreatedDt).Select(x => x.Id).FirstOrDefault();

                    primaryLocationElement.SelectByValue(selectedLocation.ToString());
                }
                catch (Exception ex)
                {
                    result = "Fail :: ProvidersInformation :: Exception occurred while selecting Location";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
                // });

                var btnAddSection = driver.GetElementByClassName("add-section");
                var addSectionScript = btnAddSection.GetAttribute("onclick");
                driver.ExecuteJs(addSectionScript);
                id++;

                driver.SendKeysByName("ProviderfirstNameMulti-" + id, configs.provider2FirstName);
                driver.SendKeysByName("ProviderlastNameMulti-" + id, configs.provider2LastName);

                var degree2 = driver.GetElementByName("ProviderDegree-" + id);
                var degreeElement2 = driver.GetSelectElement(degree2);
                degreeElement2.SelectByValue(configs.provider2DegreeValue);

                driver.SendKeysByName("ProvidernpiNumber-" + id, configs.provider2Npi);
                driver.SendKeysByName("ProviderPersonalEmail-" + id, configs.email);
                driver.SendKeysByName("ProviderPersonalPhone-" + id, configs.provider2Phone);

                var primarySpecialty2 = driver.GetElementByName("ProviderprimarySpecialtyMulti-" + id);
                var primarySpecialtyElement2 = driver.GetSelectElement(primarySpecialty2);
                primarySpecialtyElement2.SelectByValue(configs.provider2PrimarySpecialtyValue);

                var practiceNames2 = driver.GetElementByName("PracticeNames-" + id);
                var practiceNameElement2 = driver.GetSelectElement(practiceNames2);

                var selectedPractice2 = bppContext.Practice.Where(x => x.PracticeName == configs.practiceName1).OrderByDescending(x => x.CreatedDt).Select(x => x.PracticeGuid).FirstOrDefault();

                practiceNameElement2.SelectByValue(selectedPractice2.ToString());

                try
                {
                    driver.wait.Until(webDriver =>
                    {
                        
                        try
                        {
                            return driver.IsDisplayedByName("ProviderPrimaryLocationMulti-" + id);
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    var primaryLocation2 = driver.GetElementByName("ProviderPrimaryLocationMulti-" + id);
                    var primaryLocationElement2 = driver.GetSelectElement(primaryLocation2);

                    var selectedLocation2 = bppContext.Location.Where(x => x.LocationName == configs.location2LocationName).OrderByDescending(x => x.CreatedDt).Select(x => x.Id).FirstOrDefault();

                    primaryLocationElement2.SelectByValue(selectedLocation2.ToString());
                }
                catch (Exception ex)
                {
                    result = "Fail :: ProvidersInformation :: Exception occurred while selecting Location 2";
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
            }
            catch (Exception ex)
            {
                result = "Fail :: ProvidersInformation :: Exception Occurred. See Exception details";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            try
            {
                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsIdDisplayed("btnSaveProviders");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                
                driver.wait.Until(webDriver =>
                {
                    try
                    {
                        return driver.IsIdDisplayed("btnSaveProviders");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSaveProviders");
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception  " + e.Message);
                Logger.Error(result);
            }

            result = "ProvidersInformation :: Moving on to next page: Joinder Agreement page-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
    }
}
