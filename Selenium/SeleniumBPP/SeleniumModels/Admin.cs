﻿using System;
using System.Collections.Generic;
using System.Text;
using NLog;
using SeleniumCommon;

namespace SeleniumBPP.SeleniumModels
{
    public class Admin
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver d;
        private readonly Configs _configs;
        private readonly Mail m;
        public Admin(Driver driver, Configs configs, Mail mail)
        {
            d = driver;
            _configs = configs;
            m = mail;
        }

        public bool AdminTest()
        {
            var success = AdminLogin();
            if (!success) return success;

            var survey = new Survey(d, _configs, m);
            success = survey.CheckSurvey();
            if (!success) return success;
            success = MasterLookupList();
            if (!success) return success;
            success = ProviderProfile();
            if (!success) return success;
            success = ReloadMasterLookupList();
            if (!success) return success;
            success = PracticeProfile();
            if (!success) return success;
            success = ReloadMasterLookupList();
            if (!success) return success;
            success = EntityProfile();
            if (!success) return success;
            success = ReloadMasterLookupList();

            return success;
        }
        
        private bool AdminLogin()
        {
            var result = "AdminLogin :: Opening Login page.";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            try
            {
                d.NavigateToUrl(_configs.bppUrl);
                d.ClickById("loginButton");
                d.wait.Until(wd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("ContentPlaceHolder1_MFALoginControl1_UserIDView_txtUserid_UiInput");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.SendKeysById("ContentPlaceHolder1_MFALoginControl1_UserIDView_txtUserid_UiInput", _configs.adminUsername);
                d.SendKeysById("ContentPlaceHolder1_MFALoginControl1_UserIDView_tbxPassword_UiInput", _configs.adminPassword);
                
                d.ClickByClassName("btn-primary");

                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("content-heading");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                if (d.IsIdDisplayed("content-heading") && d.GetElementById("content-heading").Text == "Master Lookup")
                {
                    result = "Pass :: AdminLogin :: Admin Login successful";
                    Console.WriteLine(result);
                    m.AddToBody(result + "<br/>");
                    return true;
                }

                result = "Fail :: AdminLogin :: Admin login Unsuccessful";
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            catch (Exception ex)
            {
                result = "Fail :: AdminLogin :: Exception occurred Admin Login-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
        }
        private bool MasterLookupList()
        {
            var result = "MasterLookupList :: Loading Master Lookup List-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            d.NavigateToUrl(_configs.masterLookupUrl);
            
            try
            {
                d.wait.Until(webd =>
                {

                    try
                    {
                        var isDisplayed = d.IsClassNameDisplayed("dataTables_empty");
                        if (isDisplayed) return false;

                        result = "Pass :: MasterLookupList :: Master Lookup List has loaded-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                        return true;
                    }
                    catch (Exception)
                    {
                        result = "Pass :: MasterLookupList :: Master Lookup List has loaded-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                        return true;
                    }
                });
            }
            catch (Exception ex)
            {
                result = "Fail :: MasterLookupList :: Issue loading Master Lookup List-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }

            //On Master Lookup List
            //Search by inputted Provider
            result = "MasterLookupList :: Searching based on inputted Providers";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var provider = _configs.provider1Npi;

            result = "MasterLookupList :: Searching by NPI Number";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            d.SendKeysById("searchbox", provider);

            result = "Pass :: MasterLookupList :: Search successful";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            return true;
        }
        private bool ProviderProfile()
        {
            var result = "";
            try
            {
                d.wait.Until(wd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("Provider");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickById("Provider");

                result = "Pass :: ProviderProfile :: Provider Profile loading";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                
                var originalTab = d.SwitchNewTabs();
                
                result = "Pass :: ProviderProfile :: Provider Profile Loaded";
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsDisplayedByName("_Providers[0].ProviderAncillary.Maiden_Name");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                var maidenName = d.GetAttributeByName("_Providers[0].ProviderAncillary.Maiden_Name", "value");
                if (maidenName != null)
                {
                    d.ClearElementByName("_Providers[0].ProviderAncillary.Maiden_Name");
                }
                d.SendKeysByName("_Providers[0].ProviderAncillary.Maiden_Name", _configs.provider1MaidenName);

                var aka = d.GetAttributeByName("_Providers[0].ProviderAncillary.AKA", "value");
                if (aka != null)
                {
                    d.ClearElementById("provAlsoKnownAs");
                }
                d.SendKeysById("provAlsoKnownAs", _configs.provider1Aka);
                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("Employed");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickById("Employed");

                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsDisplayedByName("_Providers[0].Facilties[0].isSelected");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickByName("_Providers[0].Facilties[0].isSelected");
                
                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsDisplayedByName("_Providers[0].Facilties[2].isSelected");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickByName("_Providers[0].Facilties[2].isSelected");
                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsDisplayedByName("_Providers[0].Facilties[4].isSelected");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickByName("_Providers[0].Facilties[4].isSelected");
                
                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("btnSaveTop");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });

                var element = d.GetElementById("btnSaveTop");
                var script = element.GetAttribute("onclick");
                d.ExecuteJs(script);

                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("btnCloseModal");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickById("btnCloseModal");
                //Attempt updating profile
                try
                {
                    d.wait.Until(webd =>
                    {

                        try
                        {
                            var isDisplayed = d.IsExecuteJs("return jQuery.active == 0");
                            result = "Pass :: ProviderProfile :: Provider Profile Page Update successful-- " + DateTime.Now;
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");

                            result = "ProviderProfile :: Pausing for review";
                            Console.WriteLine(result);
                            Logger.Info(result);
                            m.AddToBody(result + "<br/>");

                            return isDisplayed;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                }
                catch (Exception ex)
                {
                    result = "Fail :: ProviderProfile :: Attempt to update profile-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }

                //Attempt Cloning profile               
                try
                {
                    d.wait.Until(webd =>
                    {

                        try
                        {
                            return d.IsIdDisplayed("btnCloneProfile");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    d.ClickById("btnCloneProfile");

                    d.wait.Until(webd =>
                    {

                        try
                        {
                            return d.IsIdDisplayed("CloneProviderProfile");
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    });
                    d.ClickById("CloneProviderProfile");
                }
                catch (Exception ex)
                {
                    result = "Fail :: ProviderProfile :: Attempt to clone profile-- " + DateTime.Now;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");

                    result = ex.Message;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
                result = "Pass :: ProviderProfile :: Provider Profile Page Clone successful-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                d.wait.Until(webd =>
                {
                    try
                    {
                        return d.IsIdDisplayed("btnCloseModal");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickById("btnCloseModal");

                d.ExecuteJs("window.close();");
                d.SwitchBackTabs(originalTab);
            }
            catch (Exception ex)
            {
                result = "Fail :: ProviderProfile :: Issue Loading Provider Profile--" + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            return true;
        }
        private bool ReloadMasterLookupList()
        {
            var result = "ReloadMasterLookupList :: Re-loading Master Lookup List-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            //Reloading Master Lookup Page
            d.NavigateToUrl(_configs.masterLookupUrl);
            
            try
            {
                d.wait.Until(webd =>
                {

                    try
                    {
                        var isDisplayed = d.IsClassNameDisplayed("dataTables_empty");
                        if (isDisplayed) return false;

                        result = "Pass :: ReloadMasterLookupList :: Master Lookup List has reloaded-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                        return true;
                    }
                    catch (Exception)
                    {
                        result = "Pass :: ReloadMasterLookupList :: Master Lookup List has reloaded-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                        return true;
                    }
                });
               
                return true;
            }
            catch (Exception ex)
            {
                result = "Fail :: ReloadMasterLookupList :: Issue reloading Master Lookup List-- " + DateTime.Now;
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
        }
        private bool PracticeProfile()
        {
            d.wait.Until(webd =>
            {

                try
                {
                    return d.IsIdDisplayed("Practice");
                }
                catch (Exception)
                {
                    return false;
                }
            });
            d.ClickById("Practice");

            var result = "Pass :: PracticeProfile :: Practice Profile loading";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            var originalTabInstance = d.SwitchNewTabs();

            Console.WriteLine("Pass :: PracticeProfile ::  Practice Profile Loaded");
            Logger.Info("Pass :: PracticeProfile ::  Practice Profile Loaded");
            d.wait.Until(webd =>
            {

                try
                {
                    var isDisplayed = d.IsDisplayedByName("_Practices[0].PracticeLocations[0].Address2");
                    return isDisplayed;
                }
                catch (Exception)
                {
                    return false;
                }
            });
            var address2 = d.GetElementByName("_Practices[0].PracticeLocations[0].Address2").GetAttribute("value");
            if (address2 != null)
            {
                d.ClearElementByName("_Practices[0].PracticeLocations[0].Address2");
            }
            d.SendKeysByName("_Practices[0].PracticeLocations[0].Address2", _configs.location1Address2);

            d.wait.Until(webd =>
            {

                try
                {
                    return d.IsIdDisplayed("btnSaveTop");
                }
                catch (Exception)
                {
                    return false;
                }
            });
            d.ClickById("btnSaveTop");

            try
            {
                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("btnCloseModal");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickById("btnCloseModal");
                result = "Pass :: PracticeProfile :: Provider Profile Page Update successful-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

            }
            catch (Exception ex)
            {
                result = "Fail :: PracticeProfile :: Attempt to update profile-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }

            result = "Pass :: PracticeProfile :: Update successful-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            //now lets close our new tab
            d.ExecuteJs("window.close();");
            // and switch our Webd back to the original tab's window handle
            d.SwitchBackTabs(originalTabInstance);

            return true;
        }
        private bool EntityProfile()
        {
            d.wait.Until(webd =>
            {

                try
                {
                    return d.IsIdDisplayed("Legal Entity");
                }
                catch (Exception)
                {
                    return false;
                }
            });
            d.ClickById("Legal Entity");

            var result = "Pass :: EntityProfile :: Legal Entity Profile loading";
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            //Accessing Legal Entity Profile
            // save a reference to our original tab's window handle
            var originalTabInstance = d.SwitchNewTabs();

            Console.WriteLine("Pass :: EntityProfile :: Legal Entity Profile Loaded");
            Logger.Info("Pass :: EntityProfile :: Legal Entity Profile Loaded");
            d.wait.Until(webd =>
            {

                try
                {
                    return d.IsDisplayedByName("_Entity.CAPAddress2");
                }
                catch (Exception)
                {
                    return false;
                }
            });
            var capAddress = d.GetAttributeByName("_Entity.CAPAddress2", "value");
            if (capAddress != null)
            {
                d.ClearElementByName("_Entity.CAPAddress2");
            }
            d.SendKeysByName("_Entity.CAPAddress2", _configs.legalEntityAddress2);

            d.wait.Until(webd =>
            {

                try
                {
                    return d.IsIdDisplayed("btnSaveTop");
                }
                catch (Exception)
                {
                    return false;
                }
            });
            var element = d.GetElementById("btnSaveTop");
            var script = d.GetAttributeByElement(element, "onclick");
            d.ExecuteJs(script);

            //Attempt updating profile
            try
            {
                d.wait.Until(webd =>
                {

                    try
                    {
                        return d.IsIdDisplayed("btnCloseModal");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                d.ClickById("btnCloseModal");
                result = "Pass :: EntityProfile :: Legal Entity Profile Page Update successful-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");
            }
            catch (Exception ex)
            {
                result = "Fail :: EntityProfile :: Attempt to update profile-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                return false;
            }
            d.ExecuteJs("window.close();");
            d.SwitchBackTabs(originalTabInstance);
            return true;
        }
    }
}
