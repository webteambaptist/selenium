﻿using System;
using System.Collections.Generic;
using System.Text;
using SeleniumCommon;

namespace SeleniumBPP.SeleniumModels
{
    public class Member
    {
        private Guid newMemberGuid;
        private Guid legalEntityGuid;
        private readonly Configs configs;
        private readonly Driver driver;
        private readonly Mail m;
        public Member(Driver driver, Configs configs, Mail mail)
        {
            this.driver = driver;
            this.configs = configs;
            this.m = mail;
        }
        public Guid GetNewMemberGuid()
        {
            return newMemberGuid;
        }
        public void SetNewMemberGuid(Guid guid)
        {
            newMemberGuid = guid;
        }
        public Guid GetLegalEntityGuid()
        {
            return legalEntityGuid;
        }
        public void SetLegalEntityGuid(Guid guid)
        {
            legalEntityGuid = guid;
        }
        public bool JoinderAgreementDownload()
        {
            var result = "JoinderAgreementDownload :: Opening Joinder Agreement page-- " + DateTime.Now;
            Console.WriteLine(result);
            m.AddToBody(result + "<br/>");

            try
            {
                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsClassNameDisplayed("downloadbtn");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickByClassName("downloadbtn");
                driver.SwitchTabs();
                
                driver.wait.Until(webDriver =>
                {
                    
                    try
                    {
                        return driver.IsIdDisplayed("btnSavePracAppStep2");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnSavePracAppStep2");
                
                result = "Pass :: JoinderAgreementDownload :: Joinder Agreement Download complete-- " + DateTime.Now;
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
            }
            catch (Exception ex)
            {
                result = "Fail :: JoinderAgreementDownload :: Issue downloading Joinder Agreement document portion of application";
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                m.AddToBody(result + "<br/>");
                return false;
            }

            result = "JoinderAgreementDownload :: Moving on to confirmation page-- " + DateTime.Now;
            Console.WriteLine(result);
            m.AddToBody(result + "<br/>");
            return true;
        }
    }
}
