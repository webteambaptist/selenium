﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Runtime.CompilerServices;
using NLog;
using SeleniumBPP.Models;
using SeleniumCommon;

namespace SeleniumBPP.SeleniumModels
{
    public class Survey
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Driver driver;
        private readonly Configs configs;
        private readonly Mail m;
        public Survey(Driver driver, Configs configs, Mail mail)
        {
            this.driver = driver;
            this.configs = configs;
            m = mail;
        }

        public bool SurveyTest()
        {
            return CompleteSurvey();
        }
        private bool CompleteSurvey()
        {
            var result = "CompleteSurvey :: Loading the Survey -- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            driver.NavigateToUrl(configs.surveyUrl);

            result = "CompleteSurvey :: Loaded Survey -- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsDisplayedByName("SubmitterName");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.SendKeysByName("SubmitterName", configs.submitterName);
                driver.SendKeysByName("SubmitterTIN", configs.taxId);
                driver.SendKeysByName("SubmitterEmailAddress", configs.email);
                driver.SendKeysByName("SubmitterPhoneNumber", configs.submitterPhone);
                driver.SendKeysByName("SubmitterPhysicianGroup", configs.legalEntityName);

                var id = 0;
                driver.SendKeysByName("ProviderNPI-" + id, configs.provider1Npi);
                var name = configs.provider1FirstName + " " + configs.provider1LastName;
                driver.SendKeysByName("ProviderName-" + id, name);
                driver.SendKeysByName("effectiveDate-" + id, "03/17/2020");
                
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsDisplayedByName("Practice-" + id);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickByName("Practice-" + id);

                using var bppContext = new BPPContext();
                var practiceNames = driver.GetElementByName("Practice-" + id);
                var practiceNameElement = driver.GetSelectElement(practiceNames);

                var selectedPractice = bppContext.Practice.Where(x => x.PracticeName == configs.practiceName1).OrderByDescending(x => x.CreatedDt).Select(x => x.PracticeGuid).FirstOrDefault();

                practiceNameElement.SelectByValue(selectedPractice.ToString());

                var statusChange = driver.GetElementByName("statusChange-" + id);
                var statusChangeElement = driver.GetSelectElement(statusChange);

                var selectedStatus = bppContext.StatusChange.Where(x => x.StatusChange1 == configs.surveyStatusChange).Select(x => x.Id).FirstOrDefault();

                statusChangeElement.SelectByValue(selectedStatus.ToString());

                var statusChangeReason = driver.GetElementByName("statusChangeReason-" + id);
                var statusChangeReasonElement = driver.GetSelectElement(statusChangeReason);

                var selectedStatusChangeReason = bppContext.StatusChangeReason.Where(x => x.StatusChangeReason1 == configs.surveyStatusChangeReason).Select(x => x.Id).FirstOrDefault();

                statusChangeReasonElement.SelectByValue(selectedStatusChangeReason.ToString());
                if (driver.IsClassNameDisplayed("InvalidProviderNPI-" + id))
                {
                    result = "Fail :: CompleteSurvey :: Invalid NPI entered inputting Survey data -- " + DateTime.Now;
                    result += "NPI = " + configs.provider1Npi;
                    Console.WriteLine(result);
                    Logger.Error(result);
                    m.AddToBody(result + "<br/>");
                    return false;
                }
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        return driver.IsIdDisplayed("btnDone");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                });
                driver.ClickById("btnDone");

                result = "Pass :: CompleteSurvey :: Survey Submission complete-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                return true;
            }
            catch (Exception ex)
            {
                result = "Fail :: CompleteSurvey :: Issue inputting Survey data -- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
        }
        public bool CheckSurvey()
        {
            var result = "SurveyAdmin :: Loading the Update Worklist -- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            try
            {
                driver.NavigateToUrl(configs.updateWorkList);
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        var isDisplayed = driver.IsClassNameDisplayed("dataTables_empty");
                        if (isDisplayed) return false;

                        result = "Pass :: SurveyAdmin :: Update Worklist has loaded-- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                        return true;
                    }
                    catch (Exception)
                    {
                        return true;
                    }
                });
            }
            catch (Exception ex)
            {
                result = "Fail :: SurveyAdmin :: Issue loading Update Worklist-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            var providerName = configs.provider1FirstName + " " + configs.provider1LastName;
            #region Test Data
            //var _providerName = "Samuel Smith III";
            #endregion

            result = "SurveyAdmin :: Attempting to access Survey Details for " + providerName + " -- " + DateTime.Now;

            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            driver.wait.Until(webDriver =>
            {

                try
                {
                    return driver.IsDisplayedByLinkText(providerName);
                }
                catch (Exception)
                {
                    return false;
                }
            });
            driver.ClickByLinkText(providerName);

            result = "SurveyAdmin :: Loading the Survey Details page for User:" + providerName + "-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            driver.wait.Until(webDriver =>
            {

                try
                {
                    return driver.IsIdDisplayed("btnApproveChanges");
                }
                catch (Exception)
                {
                    return false;
                }
            });
            driver.ClickById("btnApproveChanges");

            using var bppContext = new BPPContext();
            var updateSurveyChange = bppContext.ProviderUpdateStaging.FirstOrDefault(x => x.ProviderName == providerName);
            if (updateSurveyChange != null)
            {
                updateSurveyChange.Proccessed = 1;
                bppContext.SaveChanges();

                result = "Pass :: SurveyAdmin :: Survey Change has been approved -- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                driver.NavigateToUrl(configs.updateWorkList);

                result = "SurveyAdmin :: Navigating back to the Update Worklist page -- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                result = "Pass :: SurveyAdmin :: Update Worklist has been re-loaded-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Info(result);
                m.AddToBody(result + "<br/>");

                return true;
            }
            else
            {
                result = "Fail :: SurveyAdmin :: Survey Not Found-- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
        }
        public bool SurveyArchiveList()
        {
            var result = "SurveyArchiveList :: Loading the Survey Archive List -- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            driver.NavigateToUrl(configs.archiveUrl);

            try
            {
                driver.wait.Until(webDriver =>
                {

                    try
                    {
                        var isDisplayed = driver.IsClassNameDisplayed("dataTables_empty");
                        if (isDisplayed) return false;

                        result = "Pass :: SurveyArchiveList :: Survey Archive List has loaded -- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                        return true;
                    }
                    catch (Exception)
                    {
                        result = "Pass :: SurveyArchiveList :: Survey Archive List has loaded -- " + DateTime.Now;
                        Console.WriteLine(result);
                        Logger.Info(result);
                        m.AddToBody(result + "<br/>");
                        return true;
                    }
                });
            }
            catch (Exception ex)
            {
                result = "Fail :: SurveyArchiveList :: Issue loading Survey Archive List -- " + DateTime.Now;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");

                result = ex.Message;
                Console.WriteLine(result);
                Logger.Error(result);
                m.AddToBody(result + "<br/>");
                return false;
            }
            var providerName = configs.provider1FirstName + " " + configs.provider1LastName;
            #region Test Data
            //var _providerName = "Samuel Smith III";
            #endregion

            result = "SurveyArchiveList :: Attempting to access Survey Details for " + providerName + " from the Survey Archive List -- " + DateTime.Now;

            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");
            driver.wait.Until(webDriver =>
            {
                try
                {
                    return driver.IsDisplayedByXPath("//input[@type='search']");
                }
                catch (Exception)
                {
                    return false;
                }
            });
            driver.SendKeysByXPath("//input[@type='search']", providerName);
            
            driver.wait.Until(webDriver =>
            {

                try
                {
                    return driver.IsDisplayedByLinkText(providerName);
                }
                catch (Exception)
                {
                    return false;
                }
            });
            driver.ClickByLinkText(providerName);

            result = "SurveyArchiveList :: Loading the Survey Details page for User:" + providerName + " from the Survey Archive List-- " + DateTime.Now;
            Console.WriteLine(result);
            Logger.Info(result);
            m.AddToBody(result + "<br/>");

            driver.NavigateToUrl(configs.bppUrl);

            return true;
        }
    }
}
