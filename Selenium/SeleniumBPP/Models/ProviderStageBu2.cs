﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderStageBu2
    {
        public int ProviderId { get; set; }
        public int? LocationId { get; set; }
        public string TaxId { get; set; }
        public int? Npi { get; set; }
        public string FirstName { get; set; }
        public string Aka { get; set; }
        public string Mi { get; set; }
        public string LastName { get; set; }
        public string MaidenName { get; set; }
        public string NameSuffix { get; set; }
        public string Degree { get; set; }
        public string PrimarySpecialty { get; set; }
        public string SecondarySpecialty { get; set; }
        public string SpcCode1 { get; set; }
        public string SpcCode2 { get; set; }
        public string SpecialtyNote { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PersonalPhoneAreaCode { get; set; }
        public string PersonalPhone { get; set; }
        public string PersonalExt { get; set; }
        public string Email { get; set; }
        public DateTime? EligCertVerified { get; set; }
        public DateTime? EligCertExpiration { get; set; }
        public string EligCertStatus { get; set; }
        public string EligCertNote { get; set; }
        public string MedStaffPriv { get; set; }
        public DateTime? SignedMemberAppl { get; set; }
        public string ApplStatus { get; set; }
        public DateTime? SignedJoinderAgrmt { get; set; }
        public string JoinderAgrmtStatus { get; set; }
        public DateTime? InProcess { get; set; }
        public DateTime? ReadyForApproval { get; set; }
        public DateTime? RecommendationSent { get; set; }
        public DateTime? BoardConsiderationDate { get; set; }
        public string Outcome { get; set; }
        public string OutcomeReason { get; set; }
        public DateTime? OutcomeNotification { get; set; }
        public DateTime? BppEffectiveDate { get; set; }
        public string CmsSpecialtyCode { get; set; }
        public string ProviderStatus { get; set; }
        public DateTime? StatusDate { get; set; }
        public string StatusReason { get; set; }
        public string StatusNote { get; set; }
        public DateTime? LeftNetworkDate { get; set; }
        public string ReasonLeft { get; set; }
        public DateTime? NotificationLetterDate { get; set; }
        public string NotificationComment { get; set; }
        public string EmploymentStatus { get; set; }
        public string SpecialtyType { get; set; }
        public string ServicePopulation { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? DegreeId { get; set; }
    }
}
