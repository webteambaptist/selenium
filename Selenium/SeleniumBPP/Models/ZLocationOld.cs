﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ZLocationOld
    {
        public int Id { get; set; }
        public Guid PracticeGuid { get; set; }
        public string LocationName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public bool? IsPrimary { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string PointOfContact { get; set; }
        public string PocphoneNumber { get; set; }
        public string Pocemail { get; set; }
        public DateTime CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public Guid? Guid { get; set; }
        public string Email { get; set; }
    }
}
