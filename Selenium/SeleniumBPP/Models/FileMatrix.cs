﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class FileMatrix
    {
        public int Id { get; set; }
        public DateTime CreatedDt { get; set; }
        public string TaxidNpi { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string FileTitle { get; set; }
    }
}
