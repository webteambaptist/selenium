﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProvSpecialtyStage
    {
        public int? ProvSpecId { get; set; }
        public int? Npi { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimSec { get; set; }
        public string SpecCode { get; set; }
        public string Specialty { get; set; }
        public string SpecialtyNote { get; set; }
        public int Id { get; set; }
    }
}
