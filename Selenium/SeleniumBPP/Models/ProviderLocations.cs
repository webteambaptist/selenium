﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderLocations
    {
        public int Id { get; set; }
        public string Npinumber { get; set; }
        public int LocationId { get; set; }
        public bool IsPrimary { get; set; }
        public Guid? PracticeGuid { get; set; }
        public int? ProviderPracticeMatrixId { get; set; }
    }
}
