﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class Email
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Html { get; set; }
        public DateTime? CreatDt { get; set; }
        public string CreatedBy { get; set; }
    }
}
