﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderUpdateStaging
    {
        public long Id { get; set; }
        public string ProviderName { get; set; }
        public string NationalProviderIdentification { get; set; }
        public Guid? Practice { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int? StatusChange { get; set; }
        public int? StatusChangeReason { get; set; }
        public string Other { get; set; }
        public DateTime? Created { get; set; }
        public int? Proccessed { get; set; }
        public Guid? SubmitterGuid { get; set; }
        public string UpdateLastName { get; set; }
        public int? UpdateStatus { get; set; }
    }
}
