﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderLocationsAudit
    {
        public int Id { get; set; }
        public string Npinumber { get; set; }
        public int LocationId { get; set; }
        public bool IsPrimary { get; set; }
        public Guid? PracticeGuid { get; set; }
        public int? ProviderPracticeMatrixId { get; set; }
        public DateTime? ModifedDt { get; set; }
        public string ModifiedUser { get; set; }
    }
}
