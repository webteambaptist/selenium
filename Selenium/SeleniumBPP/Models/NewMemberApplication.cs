﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class NewMemberApplication
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int AppStep { get; set; }
        public Guid? LeGuid { get; set; }
        public DateTime? CreateDt { get; set; }
        public DateTime? ModifiedDt { get; set; }
    }
}
