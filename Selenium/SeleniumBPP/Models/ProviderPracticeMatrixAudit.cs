﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderPracticeMatrixAudit
    {
        public int Id { get; set; }
        public Guid PracticeGuid { get; set; }
        public string Npinumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public string ModifiedUser { get; set; }
    }
}
