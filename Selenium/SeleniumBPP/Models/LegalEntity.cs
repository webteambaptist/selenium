﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class LegalEntity
    {
        public int Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public string TaxId { get; set; }
        public string LeName { get; set; }
        public string LeAddress1 { get; set; }
        public string LeAddress2 { get; set; }
        public string LeCity { get; set; }
        public string LeState { get; set; }
        public string LeZip { get; set; }
        public string Pocname { get; set; }
        public string PocphoneNumber { get; set; }
        public string Pocfax { get; set; }
        public string Pocemail { get; set; }
        public string Capname { get; set; }
        public string Capaddress1 { get; set; }
        public string Capaddress2 { get; set; }
        public string Capcity { get; set; }
        public string Capstate { get; set; }
        public string Capzip { get; set; }
        public bool Capcompleted { get; set; }
        public DateTime? MemberApplicationSigned { get; set; }
        public DateTime? ParticipationAgreementSigned { get; set; }
        public DateTime? BusinessAssociationSigned { get; set; }
        public DateTime? OutcomeNotification { get; set; }
        public string OutcomeNotificationNote { get; set; }
        public DateTime? BppeffectiveDate { get; set; }
        public DateTime? ExecutedAgreementReturned { get; set; }
        public string Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string Pocextension { get; set; }
        public DateTime? InactiveDate { get; set; }
        public string InactiveNotes { get; set; }
    }
}
