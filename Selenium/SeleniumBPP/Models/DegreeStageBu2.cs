﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class DegreeStageBu2
    {
        public int? DegreeId { get; set; }
        public string Value { get; set; }
        public string Meaning { get; set; }
    }
}
