﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderNotes
    {
        public int Id { get; set; }
        public DateTime CreateDt { get; set; }
        public string Npinumber { get; set; }
        public string Note { get; set; }
    }
}
