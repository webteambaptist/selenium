﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class PracticeNotes
    {
        public int Id { get; set; }
        public DateTime CreateDt { get; set; }
        public Guid PracticeGuid { get; set; }
        public string Note { get; set; }
    }
}
