﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderPracticeMatrix
    {
        public int Id { get; set; }
        public Guid PracticeGuid { get; set; }
        public string Npinumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
