﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderUpdateSubmitter
    {
        public long Id { get; set; }
        public string SubmitterName { get; set; }
        public string SubmittersPhysicianGroup { get; set; }
        public string SubmittersTin { get; set; }
        public string SubmittersEmailAddress { get; set; }
        public string SubmittersPhoneNumber { get; set; }
        public DateTime? Created { get; set; }
        public Guid? Guid { get; set; }
    }
}
