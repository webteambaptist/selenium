﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderFacilities
    {
        public int Id { get; set; }
        public string Npinumber { get; set; }
        public int FacilityId { get; set; }
        public Guid? PracticeGuid { get; set; }
        public int? ProviderPracticeMatrixId { get; set; }
    }
}
