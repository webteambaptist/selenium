﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class Practice
    {
        public int Id { get; set; }
        public string PracticeName { get; set; }
        public Guid? LegalEntityId { get; set; }
        public Guid PracticeGuid { get; set; }
        public int? PracticeApprovalStatus { get; set; }
        public DateTime? CreatedDt { get; set; }
        public DateTime? ModifiedDt { get; set; }
    }
}
