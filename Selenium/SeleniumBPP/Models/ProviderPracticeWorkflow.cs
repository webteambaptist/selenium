﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderPracticeWorkflow
    {
        public int Id { get; set; }
        public string Npinumber { get; set; }
        public Guid PracticeGuid { get; set; }
        public DateTime? DateSignedMemberApp { get; set; }
        public DateTime? DateSignedJoinder { get; set; }
        public DateTime? DatePresentedMqprocess { get; set; }
        public DateTime? DatePresentedReady { get; set; }
        public DateTime? DateRecommendationSent { get; set; }
        public DateTime? BoardConsiderationDate { get; set; }
        public string Outcome { get; set; }
        public DateTime? DateNotifcationOutcome { get; set; }
        public string OutcomeNotes { get; set; }
        public string PrimaryPracticeLocation { get; set; }
        public DateTime? EligibilityCertDate { get; set; }
        public string MedStaffPrivileges { get; set; }
        public DateTime? BppeffectiveDate { get; set; }
        public string Witness { get; set; }
        public DateTime? DateJoinderReceived { get; set; }
        public int? MsoverificationStatusId { get; set; }
        public int? MqrecommendationStatusId { get; set; }
        public int? BoardOutcomeStatusId { get; set; }
        public int? DenialReasonStatusId { get; set; }
        public int? ProviderStatusId { get; set; }
        public int? InactiveReasonStatusId { get; set; }
        public DateTime? ProviderInactiveDate { get; set; }
        public DateTime? NotificationDate { get; set; }
        public int? ProviderPracticeMatrixId { get; set; }
    }
}
