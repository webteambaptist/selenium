﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class DenialReasonStatus
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
    }
}
