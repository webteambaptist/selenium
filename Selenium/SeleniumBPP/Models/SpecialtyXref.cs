﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class SpecialtyXref
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
    }
}
