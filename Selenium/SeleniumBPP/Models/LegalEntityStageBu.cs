﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class LegalEntityStageBu
    {
        public int LegalEntityId { get; set; }
        public string TaxId { get; set; }
        public string LeName { get; set; }
        public string LeMailAddr1 { get; set; }
        public string LeMailAddr2 { get; set; }
        public string LeMailCity { get; set; }
        public string LeMailState { get; set; }
        public string LeMailZip { get; set; }
        public string PocName { get; set; }
        public string PocAreaCode { get; set; }
        public string PocPhone { get; set; }
        public string PocExt { get; set; }
        public string PocFaxAreaCode { get; set; }
        public string PocFax { get; set; }
        public string PocEmail { get; set; }
        public string CapName { get; set; }
        public bool? SameAddr { get; set; }
        public string CapMailAddr1 { get; set; }
        public string CapMailAddr2 { get; set; }
        public string CapMailCity { get; set; }
        public string CapMailState { get; set; }
        public string CapMailZip { get; set; }
        public string CapCompleted { get; set; }
        public DateTime? SignedNewMemberAppl { get; set; }
        public DateTime? SignedNewNetParticipAgrmt { get; set; }
        public DateTime? SignedNewBusinAssocAgrmt { get; set; }
        public DateTime? OutcomeNotific { get; set; }
        public string OutcomeNotificNote { get; set; }
        public DateTime? BppEffectiveDate { get; set; }
        public DateTime? ExecutedAgrmtReturned { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
