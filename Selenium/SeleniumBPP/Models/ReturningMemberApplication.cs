﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ReturningMemberApplication
    {
        public Guid Guid { get; set; }
        public Guid LeGuid { get; set; }
        public string Tin { get; set; }
        public string Email { get; set; }
        public int AppStep { get; set; }
        public DateTime? CreateDt { get; set; }
        public DateTime? ModifiedDt { get; set; }
    }
}
