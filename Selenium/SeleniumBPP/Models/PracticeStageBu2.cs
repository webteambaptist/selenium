﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class PracticeStageBu2
    {
        public int PracticeId { get; set; }
        public int? LegalEntityId { get; set; }
        public string TaxId { get; set; }
        public string LeName { get; set; }
        public string PracticeName { get; set; }
        public bool? PracticeLogoRecvd { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
