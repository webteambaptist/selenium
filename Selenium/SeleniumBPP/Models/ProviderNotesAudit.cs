﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderNotesAudit
    {
        public int Id { get; set; }
        public DateTime CreateDt { get; set; }
        public string Npinumber { get; set; }
        public string Note { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public string ModifiedUser { get; set; }
    }
}
