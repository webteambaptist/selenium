﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class SpecialtyStageBu2
    {
        public int? SpecialtyId { get; set; }
        public string SpecializationCode { get; set; }
        public string Specialty { get; set; }
        public int Id { get; set; }
    }
}
