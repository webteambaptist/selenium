﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ContractsOptOut
    {
        public int Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public int ContractId { get; set; }
        public bool OptOut { get; set; }
    }
}
