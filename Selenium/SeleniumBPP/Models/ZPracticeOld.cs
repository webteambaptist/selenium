﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ZPracticeOld
    {
        public int Id { get; set; }
        public string PracticeName { get; set; }
        public string TaxId { get; set; }
        public string Emrvendor { get; set; }
        public string Emrversion { get; set; }
        public string OfficeManagerName { get; set; }
        public string OfficeManagerPhoneNumber { get; set; }
        public string OfficeManagerEmail { get; set; }
        public string BillingManagerName { get; set; }
        public string BillingManagerPhoneNumber { get; set; }
        public string BillingManagerEmail { get; set; }
        public string PointOfContact { get; set; }
        public string PocphoneNumber { get; set; }
        public string Pocemail { get; set; }
        public DateTime? CreatedDt { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public Guid Guid { get; set; }
        public int? PracticeApprovalStatus { get; set; }
        public string Capname { get; set; }
        public string Capaddress { get; set; }
        public string Capcity { get; set; }
        public string Capstate { get; set; }
        public string Capzip { get; set; }
        public string Msspdetails { get; set; }
        public string Notes { get; set; }
        public DateTime? DateSignedMemApp { get; set; }
        public DateTime? DateSignedNpa { get; set; }
        public DateTime? DateSignedBaa { get; set; }
        public DateTime? Capcompleted { get; set; }
        public DateTime? DateNotificationGroupOutcome { get; set; }
        public DateTime? BppeffectiveDate { get; set; }
        public DateTime? DateReturnEag { get; set; }
        public bool? PracticeLogoReceived { get; set; }
        public bool? IsBaareceived { get; set; }
        public bool? IsBppreceived { get; set; }
        public string GroupNpi { get; set; }
        public string OptOutComments { get; set; }
    }
}
