﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderFacilityStage
    {
        public int? ProvFacId { get; set; }
        public int? Npi { get; set; }
        public int? FacilityId { get; set; }
        public int Id { get; set; }
        public int? ProviderId { get; set; }
    }
}
