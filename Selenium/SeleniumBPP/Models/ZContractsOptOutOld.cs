﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ZContractsOptOutOld
    {
        public int Id { get; set; }
        public Guid PracticeGuid { get; set; }
        public int ContractId { get; set; }
        public int OptOut { get; set; }
    }
}
