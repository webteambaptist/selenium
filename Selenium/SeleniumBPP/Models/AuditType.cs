﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class AuditType
    {
        public int Id { get; set; }
        public string AuditType1 { get; set; }
        public string Description { get; set; }
    }
}
