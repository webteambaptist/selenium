﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderAncillary
    {
        public int Id { get; set; }
        public string Npinumber { get; set; }
        public DateTime CreatedDt { get; set; }
        public string Aka { get; set; }
        public string MaidenName { get; set; }
        public string EmploymentStatus { get; set; }
        public string SpecialtyType { get; set; }
        public string ServicePopulation { get; set; }
        public Guid? PracticeGuid { get; set; }
        public int? ProviderPracticeMatrixId { get; set; }
    }
}
