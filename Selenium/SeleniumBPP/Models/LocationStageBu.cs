﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class LocationStageBu
    {
        public int LocationId { get; set; }
        public int? PracticeId { get; set; }
        public int? LegalEntityId { get; set; }
        public string TaxId { get; set; }
        public string LeName { get; set; }
        public string Practice { get; set; }
        public string Location { get; set; }
        public string LocAddr1 { get; set; }
        public string LocAddr2 { get; set; }
        public string LocCity { get; set; }
        public string LocState { get; set; }
        public string LocZip { get; set; }
        public string EmrVendor { get; set; }
        public string EmrVersion { get; set; }
        public string PracticePhoneAreaCode { get; set; }
        public string PracticePhone { get; set; }
        public string PracticeExt { get; set; }
        public string PracticeFaxAreaCode { get; set; }
        public string PracticeFax { get; set; }
        public string OfficeMngr { get; set; }
        public string OfficeMngrPhoneAreaCode { get; set; }
        public string OfficeMngrPhone { get; set; }
        public string OfficeMngrPhoneExt { get; set; }
        public string OfficeMngrFaxAreaCode { get; set; }
        public string OfficeMngrFax { get; set; }
        public string OfficeMngrEmail { get; set; }
        public string BillingMngr { get; set; }
        public string BillingMngrPhoneAreaCode { get; set; }
        public string BillingMngrPhone { get; set; }
        public string BillingMngrPhoneExt { get; set; }
        public string BillingMngrFaxAreaCode { get; set; }
        public string BillingMngrFax { get; set; }
        public string BillingMngrEmail { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
