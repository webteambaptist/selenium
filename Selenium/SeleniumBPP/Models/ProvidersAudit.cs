﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProvidersAudit
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Npinumber { get; set; }
        public DateTime? CreatedDt { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public bool? IsBhprivileged { get; set; }
        public Guid Guid { get; set; }
        public string Cms { get; set; }
        public bool IsDeleted { get; set; }
        public int? DegreeId { get; set; }
        public string SpecialtyNotes { get; set; }
        public string ModifiedUser { get; set; }
    }
}
