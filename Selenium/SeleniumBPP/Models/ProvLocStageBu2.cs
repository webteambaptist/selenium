﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProvLocStageBu2
    {
        public int LocationId { get; set; }
        public int? PracticeId { get; set; }
        public string TaxId { get; set; }
        public int? Npi { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Location { get; set; }
        public bool? IsPrimary { get; set; }
        public string ProviderStatus { get; set; }
        public string PracticePhysicalAddr1 { get; set; }
        public string PracticePhysicalAddr2 { get; set; }
        public string PracticeCity { get; set; }
        public string PracticeState { get; set; }
        public string PracticeZip { get; set; }
        public int Id { get; set; }
    }
}
