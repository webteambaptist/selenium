﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class LegalEntityNotes
    {
        public int Id { get; set; }
        public DateTime CreateDt { get; set; }
        public Guid LegalEntityId { get; set; }
        public string Note { get; set; }
    }
}
