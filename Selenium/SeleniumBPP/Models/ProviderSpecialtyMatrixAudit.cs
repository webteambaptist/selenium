﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderSpecialtyMatrixAudit
    {
        public int Id { get; set; }
        public string Npinumber { get; set; }
        public int SpecialtyId { get; set; }
        public bool IsPrmary { get; set; }
        public Guid? PracticeGuid { get; set; }
        public int? ProviderPracticeMatrixId { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public string ModifiedUser { get; set; }
    }
}
