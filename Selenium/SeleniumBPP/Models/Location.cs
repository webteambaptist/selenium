﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class Location
    {
        public int Id { get; set; }
        public Guid PracticeGuid { get; set; }
        public Guid? LocationGuid { get; set; }
        public string LocationName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool? IsPrimary { get; set; }
        public string PhoneNumber { get; set; }
        public string Extension { get; set; }
        public string Fax { get; set; }
        public string Emrvendor { get; set; }
        public string Emrversion { get; set; }
        public string OfficeManagerName { get; set; }
        public string OfficeManagerPhoneNumber { get; set; }
        public string OfficeManagerExtension { get; set; }
        public string OfficeManagerEmail { get; set; }
        public string BillingManagerName { get; set; }
        public string BillingManagerPhoneNumber { get; set; }
        public string BillingManagerExtension { get; set; }
        public string BillingManagerEmail { get; set; }
        public DateTime CreatedDt { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public bool IsRemoved { get; set; }
    }
}
