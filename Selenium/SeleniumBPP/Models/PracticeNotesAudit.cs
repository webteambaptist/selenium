﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class PracticeNotesAudit
    {
        public int Id { get; set; }
        public DateTime CreateDt { get; set; }
        public Guid PracticeGuid { get; set; }
        public string Note { get; set; }
        public DateTime? ModifedDt { get; set; }
        public string ModifiedUser { get; set; }
    }
}
