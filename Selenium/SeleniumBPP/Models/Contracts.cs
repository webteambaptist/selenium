﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class Contracts
    {
        public int Id { get; set; }
        public string ContractName { get; set; }
    }
}
