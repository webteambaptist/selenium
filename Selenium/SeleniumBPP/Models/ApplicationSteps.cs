﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ApplicationSteps
    {
        public int Id { get; set; }
        public string ApplicationStep { get; set; }
    }
}
