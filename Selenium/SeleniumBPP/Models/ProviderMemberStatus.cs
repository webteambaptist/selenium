﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderMemberStatus
    {
        public int Id { get; set; }
        public string MemberStatus { get; set; }
        public int StatusId { get; set; }
    }
}
