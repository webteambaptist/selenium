﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class FacilityStageBu2
    {
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
    }
}
