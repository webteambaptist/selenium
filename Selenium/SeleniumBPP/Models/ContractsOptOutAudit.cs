﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ContractsOptOutAudit
    {
        public int Id { get; set; }
        public Guid LegalEntityId { get; set; }
        public int ContractId { get; set; }
        public bool OptOut { get; set; }
        public DateTime? ModifiedDt { get; set; }
        public string ModifiedUser { get; set; }
    }
}
