﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class Facilities
    {
        public int Id { get; set; }
        public string FacilityName { get; set; }
        public string ShortName { get; set; }
    }
}
