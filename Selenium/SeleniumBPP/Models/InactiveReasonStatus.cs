﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class InactiveReasonStatus
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
    }
}
