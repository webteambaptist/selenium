﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class SpecialtyMatrix
    {
        public int Id { get; set; }
        public string Specialty { get; set; }
        public string Code { get; set; }
        public int? SpecialtyId { get; set; }
    }
}
