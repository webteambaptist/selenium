﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class ProviderSpecialtyMatrix
    {
        public int Id { get; set; }
        public string Npinumber { get; set; }
        public bool IsPrmary { get; set; }
        public int SpecialtyId { get; set; }
        public Guid? PracticeGuid { get; set; }
        public int? ProviderPracticeMatrixId { get; set; }
    }
}
