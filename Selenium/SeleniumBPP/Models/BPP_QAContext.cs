﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;

namespace SeleniumBPP.Models
{
    public partial class BPP_QAContext : DbContext
    {
        static IConfigurationRoot config;
        public BPP_QAContext()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            config = builder.Build();
        }

        public BPP_QAContext(DbContextOptions<BPP_QAContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ApplicationSteps> ApplicationSteps { get; set; }
        public virtual DbSet<AuditLog> AuditLog { get; set; }
        public virtual DbSet<AuditType> AuditType { get; set; }
        public virtual DbSet<BoardOutcomeStatus> BoardOutcomeStatus { get; set; }
        public virtual DbSet<Contracts> Contracts { get; set; }
        public virtual DbSet<ContractsOptOut> ContractsOptOut { get; set; }
        public virtual DbSet<ContractsOptOutAudit> ContractsOptOutAudit { get; set; }
        public virtual DbSet<DegreeType> DegreeType { get; set; }
        public virtual DbSet<DenialReasonStatus> DenialReasonStatus { get; set; }
        public virtual DbSet<Email> Email { get; set; }
        public virtual DbSet<Facilities> Facilities { get; set; }
        public virtual DbSet<FileMatrix> FileMatrix { get; set; }
        public virtual DbSet<InactiveReasonStatus> InactiveReasonStatus { get; set; }
        public virtual DbSet<LegalEntity> LegalEntity { get; set; }
        public virtual DbSet<LegalEntityAudit> LegalEntityAudit { get; set; }
        public virtual DbSet<LegalEntityNotes> LegalEntityNotes { get; set; }
        public virtual DbSet<LegalEntityNotesAudit> LegalEntityNotesAudit { get; set; }
        public virtual DbSet<LegalEntityStatus> LegalEntityStatus { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LocationAudit> LocationAudit { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<MqrecommendationStatus> MqrecommendationStatus { get; set; }
        public virtual DbSet<MsoverificationStatus> MsoverificationStatus { get; set; }
        public virtual DbSet<NewMemberApplication> NewMemberApplication { get; set; }
        public virtual DbSet<Pdfgenerator> Pdfgenerator { get; set; }
        public virtual DbSet<Practice> Practice { get; set; }
        public virtual DbSet<PracticeAudit> PracticeAudit { get; set; }
        public virtual DbSet<PracticeMemberStatus> PracticeMemberStatus { get; set; }
        public virtual DbSet<PracticeNotes> PracticeNotes { get; set; }
        public virtual DbSet<PracticeNotesAudit> PracticeNotesAudit { get; set; }
        public virtual DbSet<ProviderAncillary> ProviderAncillary { get; set; }
        public virtual DbSet<ProviderAncillaryAudit> ProviderAncillaryAudit { get; set; }
        public virtual DbSet<ProviderFacilities> ProviderFacilities { get; set; }
        public virtual DbSet<ProviderFacilitiesAudit> ProviderFacilitiesAudit { get; set; }
        public virtual DbSet<ProviderLocations> ProviderLocations { get; set; }
        public virtual DbSet<ProviderLocationsAudit> ProviderLocationsAudit { get; set; }
        public virtual DbSet<ProviderMemberStatus> ProviderMemberStatus { get; set; }
        public virtual DbSet<ProviderNotes> ProviderNotes { get; set; }
        public virtual DbSet<ProviderNotesAudit> ProviderNotesAudit { get; set; }
        public virtual DbSet<ProviderPracticeMatrix> ProviderPracticeMatrix { get; set; }
        public virtual DbSet<ProviderPracticeMatrixAudit> ProviderPracticeMatrixAudit { get; set; }
        public virtual DbSet<ProviderPracticeWorkflow> ProviderPracticeWorkflow { get; set; }
        public virtual DbSet<ProviderPracticeWorkflowAudit> ProviderPracticeWorkflowAudit { get; set; }
        public virtual DbSet<ProviderSpecialtyMatrix> ProviderSpecialtyMatrix { get; set; }
        public virtual DbSet<ProviderSpecialtyMatrixAudit> ProviderSpecialtyMatrixAudit { get; set; }
        public virtual DbSet<ProviderUpdateStaging> ProviderUpdateStaging { get; set; }
        public virtual DbSet<ProviderUpdateSubmitter> ProviderUpdateSubmitter { get; set; }
        public virtual DbSet<Providers> Providers { get; set; }
        public virtual DbSet<ProvidersAudit> ProvidersAudit { get; set; }
        public virtual DbSet<ReturningMemberApplication> ReturningMemberApplication { get; set; }
        public virtual DbSet<SpecialtyMatrix> SpecialtyMatrix { get; set; }
        public virtual DbSet<SpecialtyXref> SpecialtyXref { get; set; }
        public virtual DbSet<StatusChange> StatusChange { get; set; }
        public virtual DbSet<StatusChangeReason> StatusChangeReason { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured) return;
            // optionsBuilder.UseSqlServer("Server=BHTMSQTST01V;Database=BPP_QA;Trusted_Connection=True;");
            var defaultConnection = config.GetSection("ConnectionString").GetSection("DefaultConnection").Value;
            optionsBuilder.UseSqlServer(defaultConnection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationSteps>(entity =>
            {
                entity.ToTable("ApplicationSteps", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ApplicationStep)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<AuditLog>(entity =>
            {
                entity.ToTable("AuditLog", "dbo");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AuditType)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ColumnName)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Pk)
                    .IsRequired()
                    .HasColumnName("PK")
                    .HasMaxLength(50);

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<AuditType>(entity =>
            {
                entity.ToTable("AuditType", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AuditType1)
                    .HasColumnName("AuditType")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Description).HasMaxLength(50);
            });

            modelBuilder.Entity<BoardOutcomeStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("BoardOutcomeStatus", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StatusName).HasMaxLength(200);
            });

            modelBuilder.Entity<Contracts>(entity =>
            {
                entity.ToTable("Contracts", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ContractName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContractsOptOut>(entity =>
            {
                entity.ToTable("ContractsOptOut", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");
            });

            modelBuilder.Entity<ContractsOptOutAudit>(entity =>
            {
                entity.ToTable("ContractsOptOutAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);
            });

            modelBuilder.Entity<DegreeType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("DegreeType", "dbo");

                entity.Property(e => e.Degree)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DegreeId).HasColumnName("DegreeID");

                entity.Property(e => e.DegreeName).HasMaxLength(200);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<DenialReasonStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("DenialReasonStatus", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StatusName).HasMaxLength(200);
            });

            modelBuilder.Entity<Email>(entity =>
            {
                entity.ToTable("Email", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatDt)
                    .HasColumnName("CreatDT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.Html)
                    .HasColumnName("HTML")
                    .HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(250);
            });

            modelBuilder.Entity<Facilities>(entity =>
            {
                entity.ToTable("Facilities", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FacilityName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ShortName).HasMaxLength(50);
            });

            modelBuilder.Entity<FileMatrix>(entity =>
            {
                entity.ToTable("FileMatrix", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.FileTitle)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.TaxidNpi)
                    .IsRequired()
                    .HasColumnName("TaxidNPI")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<InactiveReasonStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("InactiveReasonStatus", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StatusName).HasMaxLength(200);
            });

            modelBuilder.Entity<LegalEntity>(entity =>
            {
                entity.ToTable("LegalEntity", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BppeffectiveDate)
                    .HasColumnName("BPPEffectiveDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.BusinessAssociationSigned).HasColumnType("datetime");

                entity.Property(e => e.Capaddress1)
                    .HasColumnName("CAPAddress1")
                    .HasMaxLength(100);

                entity.Property(e => e.Capaddress2)
                    .HasColumnName("CAPAddress2")
                    .HasMaxLength(100);

                entity.Property(e => e.Capcity)
                    .HasColumnName("CAPCity")
                    .HasMaxLength(50);

                entity.Property(e => e.Capcompleted).HasColumnName("CAPCompleted");

                entity.Property(e => e.Capname)
                    .HasColumnName("CAPName")
                    .HasMaxLength(100);

                entity.Property(e => e.Capstate)
                    .HasColumnName("CAPState")
                    .HasMaxLength(2);

                entity.Property(e => e.Capzip)
                    .HasColumnName("CAPZip")
                    .HasMaxLength(10);

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExecutedAgreementReturned).HasColumnType("datetime");

                entity.Property(e => e.InactiveDate).HasColumnType("datetime");

                entity.Property(e => e.InactiveNotes).HasMaxLength(100);

                entity.Property(e => e.LastUpdated).HasColumnType("datetime");

                entity.Property(e => e.LeAddress1)
                    .HasColumnName("LE_Address1")
                    .HasMaxLength(100);

                entity.Property(e => e.LeAddress2)
                    .HasColumnName("LE_Address2")
                    .HasMaxLength(100);

                entity.Property(e => e.LeCity)
                    .HasColumnName("LE_City")
                    .HasMaxLength(50);

                entity.Property(e => e.LeName)
                    .HasColumnName("LE_Name")
                    .HasMaxLength(100);

                entity.Property(e => e.LeState)
                    .HasColumnName("LE_State")
                    .HasMaxLength(2);

                entity.Property(e => e.LeZip)
                    .HasColumnName("LE_Zip")
                    .HasMaxLength(10);

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.MemberApplicationSigned).HasColumnType("datetime");

                entity.Property(e => e.OutcomeNotification).HasColumnType("datetime");

                entity.Property(e => e.OutcomeNotificationNote).HasColumnType("ntext");

                entity.Property(e => e.ParticipationAgreementSigned).HasColumnType("datetime");

                entity.Property(e => e.Pocemail)
                    .HasColumnName("POCEmail")
                    .HasMaxLength(50);

                entity.Property(e => e.Pocextension)
                    .HasColumnName("POCExtension")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Pocfax)
                    .HasColumnName("POCFax")
                    .HasMaxLength(50);

                entity.Property(e => e.Pocname)
                    .HasColumnName("POCName")
                    .HasMaxLength(100);

                entity.Property(e => e.PocphoneNumber)
                    .HasColumnName("POCPhoneNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.TaxId)
                    .IsRequired()
                    .HasColumnName("TaxID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LegalEntityAudit>(entity =>
            {
                entity.ToTable("LegalEntityAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BppeffectiveDate)
                    .HasColumnName("BPPEffectiveDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.BusinessAssociationSigned).HasColumnType("datetime");

                entity.Property(e => e.Capaddress1)
                    .HasColumnName("CAPAddress1")
                    .HasMaxLength(100);

                entity.Property(e => e.Capaddress2)
                    .HasColumnName("CAPAddress2")
                    .HasMaxLength(100);

                entity.Property(e => e.Capcity)
                    .HasColumnName("CAPCity")
                    .HasMaxLength(50);

                entity.Property(e => e.Capcompleted).HasColumnName("CAPCompleted");

                entity.Property(e => e.Capname)
                    .HasColumnName("CAPName")
                    .HasMaxLength(100);

                entity.Property(e => e.Capstate)
                    .HasColumnName("CAPState")
                    .HasMaxLength(2);

                entity.Property(e => e.Capzip)
                    .HasColumnName("CAPZip")
                    .HasMaxLength(10);

                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExecutedAgreementReturned).HasColumnType("datetime");

                entity.Property(e => e.InactiveDate).HasColumnType("datetime");

                entity.Property(e => e.InactiveNotes).HasMaxLength(100);

                entity.Property(e => e.LastUpdated).HasColumnType("datetime");

                entity.Property(e => e.LeAddress1)
                    .HasColumnName("LE_Address1")
                    .HasMaxLength(100);

                entity.Property(e => e.LeAddress2)
                    .HasColumnName("LE_Address2")
                    .HasMaxLength(100);

                entity.Property(e => e.LeCity)
                    .HasColumnName("LE_City")
                    .HasMaxLength(50);

                entity.Property(e => e.LeName)
                    .HasColumnName("LE_Name")
                    .HasMaxLength(100);

                entity.Property(e => e.LeState)
                    .HasColumnName("LE_State")
                    .HasMaxLength(2);

                entity.Property(e => e.LeZip)
                    .HasColumnName("LE_Zip")
                    .HasMaxLength(10);

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.MemberApplicationSigned).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.OutcomeNotification).HasColumnType("datetime");

                entity.Property(e => e.OutcomeNotificationNote).HasColumnType("ntext");

                entity.Property(e => e.ParticipationAgreementSigned).HasColumnType("datetime");

                entity.Property(e => e.Pocemail)
                    .HasColumnName("POCEmail")
                    .HasMaxLength(50);

                entity.Property(e => e.Pocextension)
                    .HasColumnName("POCExtension")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Pocfax)
                    .HasColumnName("POCFax")
                    .HasMaxLength(50);

                entity.Property(e => e.Pocname)
                    .HasColumnName("POCName")
                    .HasMaxLength(100);

                entity.Property(e => e.PocphoneNumber)
                    .HasColumnName("POCPhoneNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.TaxId)
                    .IsRequired()
                    .HasColumnName("TaxID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LegalEntityNotes>(entity =>
            {
                entity.ToTable("LegalEntityNotes", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CreateDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnType("ntext");
            });

            modelBuilder.Entity<LegalEntityNotesAudit>(entity =>
            {
                entity.ToTable("LegalEntityNotesAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CreateDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnType("ntext");
            });

            modelBuilder.Entity<LegalEntityStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LegalEntityStatus", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StatusName).HasMaxLength(200);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.ToTable("Location", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Address2).HasMaxLength(255);

                entity.Property(e => e.BillingManagerEmail).HasMaxLength(255);

                entity.Property(e => e.BillingManagerExtension).HasMaxLength(10);

                entity.Property(e => e.BillingManagerName).HasMaxLength(255);

                entity.Property(e => e.BillingManagerPhoneNumber).HasMaxLength(50);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Emrvendor)
                    .HasColumnName("EMRVendor")
                    .HasMaxLength(255);

                entity.Property(e => e.Emrversion)
                    .HasColumnName("EMRVersion")
                    .HasMaxLength(50);

                entity.Property(e => e.Extension).HasMaxLength(10);

                entity.Property(e => e.Fax).HasMaxLength(50);

                entity.Property(e => e.IsRemoved).HasColumnName("isRemoved");

                entity.Property(e => e.LocationGuid).HasColumnName("LocationGUID");

                entity.Property(e => e.LocationName).HasMaxLength(255);

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("ModifiedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OfficeManagerEmail).HasMaxLength(255);

                entity.Property(e => e.OfficeManagerExtension).HasMaxLength(10);

                entity.Property(e => e.OfficeManagerName).HasMaxLength(255);

                entity.Property(e => e.OfficeManagerPhoneNumber).HasMaxLength(50);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Zip)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LocationAudit>(entity =>
            {
                entity.ToTable("LocationAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Address2).HasMaxLength(255);

                entity.Property(e => e.BillingManagerEmail).HasMaxLength(255);

                entity.Property(e => e.BillingManagerExtension).HasMaxLength(10);

                entity.Property(e => e.BillingManagerName).HasMaxLength(255);

                entity.Property(e => e.BillingManagerPhoneNumber).HasMaxLength(50);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Emrvendor)
                    .HasColumnName("EMRVendor")
                    .HasMaxLength(255);

                entity.Property(e => e.Emrversion)
                    .HasColumnName("EMRVersion")
                    .HasMaxLength(50);

                entity.Property(e => e.Extension).HasMaxLength(10);

                entity.Property(e => e.Fax).HasMaxLength(50);

                entity.Property(e => e.IsRemoved).HasColumnName("isRemoved");

                entity.Property(e => e.LocationGuid).HasColumnName("LocationGUID");

                entity.Property(e => e.LocationName).HasMaxLength(255);

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("ModifiedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.OfficeManagerEmail).HasMaxLength(255);

                entity.Property(e => e.OfficeManagerExtension).HasMaxLength(10);

                entity.Property(e => e.OfficeManagerName).HasMaxLength(255);

                entity.Property(e => e.OfficeManagerPhoneNumber).HasMaxLength(50);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Zip)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Messages>(entity =>
            {
                entity.ToTable("Messages", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Recipient).HasMaxLength(255);

                entity.Property(e => e.Sender).HasMaxLength(255);

                entity.Property(e => e.Subject).HasMaxLength(255);
            });

            modelBuilder.Entity<MqrecommendationStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MQRecommendationStatus", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StatusName).HasMaxLength(200);
            });

            modelBuilder.Entity<MsoverificationStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MSOVerificationStatus", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StatusName).HasMaxLength(200);
            });

            modelBuilder.Entity<NewMemberApplication>(entity =>
            {
                entity.HasKey(e => e.Guid);

                entity.ToTable("NewMemberApplication", "dbo");

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateDt).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.LeGuid).HasColumnName("LE_Guid");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Pdfgenerator>(entity =>
            {
                entity.ToTable("PDFGenerator", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime");

                entity.Property(e => e.Html).HasColumnName("HTML");

                entity.Property(e => e.Title).HasMaxLength(250);
            });

            modelBuilder.Entity<Practice>(entity =>
            {
                entity.HasKey(e => e.PracticeGuid);

                entity.ToTable("Practice", "dbo");

                entity.Property(e => e.PracticeGuid)
                    .HasColumnName("PracticeGUID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("ModifiedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PracticeApprovalStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PracticeName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<PracticeAudit>(entity =>
            {
                entity.ToTable("PracticeAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LegalEntityId).HasColumnName("LegalEntityID");

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("ModifiedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.PracticeApprovalStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.PracticeGuid)
                    .HasColumnName("PracticeGUID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.PracticeName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<PracticeMemberStatus>(entity =>
            {
                entity.ToTable("PracticeMemberStatus", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MemberStatus)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");
            });

            modelBuilder.Entity<PracticeNotes>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PracticeNotes", "dbo");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CreateDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnType("ntext");
            });

            modelBuilder.Entity<PracticeNotesAudit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PracticeNotesAudit", "dbo");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CreateDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ModifedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnType("ntext");
            });

            modelBuilder.Entity<ProviderAncillary>(entity =>
            {
                entity.ToTable("ProviderAncillary", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Aka)
                    .HasColumnName("AKA")
                    .HasMaxLength(200);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmploymentStatus).HasMaxLength(200);

                entity.Property(e => e.MaidenName)
                    .HasColumnName("Maiden_Name")
                    .HasMaxLength(200);

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");

                entity.Property(e => e.ServicePopulation).HasMaxLength(200);

                entity.Property(e => e.SpecialtyType).HasMaxLength(200);
            });

            modelBuilder.Entity<ProviderAncillaryAudit>(entity =>
            {
                entity.ToTable("ProviderAncillaryAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Aka)
                    .HasColumnName("AKA")
                    .HasMaxLength(200);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmploymentStatus).HasMaxLength(200);

                entity.Property(e => e.MaidenName)
                    .HasColumnName("Maiden_Name")
                    .HasMaxLength(200);

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");

                entity.Property(e => e.ServicePopulation).HasMaxLength(200);

                entity.Property(e => e.SpecialtyType).HasMaxLength(200);
            });

            modelBuilder.Entity<ProviderFacilities>(entity =>
            {
                entity.ToTable("ProviderFacilities", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FacilityId).HasColumnName("FacilityID");

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");
            });

            modelBuilder.Entity<ProviderFacilitiesAudit>(entity =>
            {
                entity.ToTable("ProviderFacilitiesAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FacilityId).HasColumnName("FacilityID");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");
            });

            modelBuilder.Entity<ProviderLocations>(entity =>
            {
                entity.ToTable("ProviderLocations", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");
            });

            modelBuilder.Entity<ProviderLocationsAudit>(entity =>
            {
                entity.ToTable("ProviderLocationsAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.ModifedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");
            });

            modelBuilder.Entity<ProviderMemberStatus>(entity =>
            {
                entity.ToTable("ProviderMemberStatus", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MemberStatus)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");
            });

            modelBuilder.Entity<ProviderNotes>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ProviderNotes", "dbo");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CreateDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ProviderNotesAudit>(entity =>
            {
                entity.ToTable("ProviderNotesAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDt)
                    .HasColumnName("CreateDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ProviderPracticeMatrix>(entity =>
            {
                entity.ToTable("ProviderPracticeMatrix", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ProviderPracticeMatrixAudit>(entity =>
            {
                entity.ToTable("ProviderPracticeMatrixAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ProviderPracticeWorkflow>(entity =>
            {
                entity.ToTable("ProviderPracticeWorkflow", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BoardConsiderationDate).HasColumnType("datetime");

                entity.Property(e => e.BoardOutcomeStatusId).HasColumnName("BoardOutcomeStatusID");

                entity.Property(e => e.BppeffectiveDate)
                    .HasColumnName("BPPEffectiveDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateJoinderReceived).HasColumnType("datetime");

                entity.Property(e => e.DateNotifcationOutcome).HasColumnType("datetime");

                entity.Property(e => e.DatePresentedMqprocess)
                    .HasColumnName("DatePresentedMQProcess")
                    .HasColumnType("datetime");

                entity.Property(e => e.DatePresentedReady).HasColumnType("datetime");

                entity.Property(e => e.DateRecommendationSent).HasColumnType("datetime");

                entity.Property(e => e.DateSignedJoinder).HasColumnType("datetime");

                entity.Property(e => e.DateSignedMemberApp).HasColumnType("datetime");

                entity.Property(e => e.DenialReasonStatusId).HasColumnName("DenialReasonStatusID");

                entity.Property(e => e.EligibilityCertDate).HasColumnType("datetime");

                entity.Property(e => e.InactiveReasonStatusId).HasColumnName("InactiveReasonStatusID");

                entity.Property(e => e.MedStaffPrivileges).HasMaxLength(255);

                entity.Property(e => e.MqrecommendationStatusId).HasColumnName("MQRecommendationStatusID");

                entity.Property(e => e.MsoverificationStatusId).HasColumnName("MSOVerificationStatusID");

                entity.Property(e => e.NotificationDate).HasColumnType("datetime");

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.Outcome).HasColumnType("ntext");

                entity.Property(e => e.OutcomeNotes).HasColumnType("ntext");

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.PrimaryPracticeLocation).HasMaxLength(255);

                entity.Property(e => e.ProviderInactiveDate).HasColumnType("datetime");

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");

                entity.Property(e => e.ProviderStatusId).HasColumnName("ProviderStatusID");

                entity.Property(e => e.Witness).HasMaxLength(255);
            });

            modelBuilder.Entity<ProviderPracticeWorkflowAudit>(entity =>
            {
                entity.ToTable("ProviderPracticeWorkflowAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BoardConsiderationDate).HasColumnType("datetime");

                entity.Property(e => e.BoardOutcomeStatusId).HasColumnName("BoardOutcomeStatusID");

                entity.Property(e => e.BppeffectiveDate)
                    .HasColumnName("BPPEffectiveDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateJoinderReceived).HasColumnType("datetime");

                entity.Property(e => e.DateNotifcationOutcome).HasColumnType("datetime");

                entity.Property(e => e.DatePresentedMqprocess)
                    .HasColumnName("DatePresentedMQProcess")
                    .HasColumnType("datetime");

                entity.Property(e => e.DatePresentedReady).HasColumnType("datetime");

                entity.Property(e => e.DateRecommendationSent).HasColumnType("datetime");

                entity.Property(e => e.DateSignedJoinder).HasColumnType("datetime");

                entity.Property(e => e.DateSignedMemberApp).HasColumnType("datetime");

                entity.Property(e => e.DenialReasonStatusId).HasColumnName("DenialReasonStatusID");

                entity.Property(e => e.EligibilityCertDate).HasColumnType("datetime");

                entity.Property(e => e.InactiveReasonStatusId).HasColumnName("InactiveReasonStatusID");

                entity.Property(e => e.MedStaffPrivileges).HasMaxLength(255);

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.MqrecommendationStatusId).HasColumnName("MQRecommendationStatusID");

                entity.Property(e => e.MsoverificationStatusId).HasColumnName("MSOVerificationStatusID");

                entity.Property(e => e.NotificationDate).HasColumnType("datetime");

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.Outcome).HasColumnType("ntext");

                entity.Property(e => e.OutcomeNotes).HasColumnType("ntext");

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.PrimaryPracticeLocation).HasMaxLength(255);

                entity.Property(e => e.ProviderInactiveDate).HasColumnType("datetime");

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");

                entity.Property(e => e.ProviderStatusId).HasColumnName("ProviderStatusID");

                entity.Property(e => e.Witness).HasMaxLength(255);
            });

            modelBuilder.Entity<ProviderSpecialtyMatrix>(entity =>
            {
                entity.ToTable("ProviderSpecialtyMatrix", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");

                entity.Property(e => e.SpecialtyId).HasColumnName("SpecialtyID");
            });

            modelBuilder.Entity<ProviderSpecialtyMatrixAudit>(entity =>
            {
                entity.ToTable("ProviderSpecialtyMatrixAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PracticeGuid).HasColumnName("PracticeGUID");

                entity.Property(e => e.ProviderPracticeMatrixId).HasColumnName("ProviderPracticeMatrixID");

                entity.Property(e => e.SpecialtyId).HasColumnName("SpecialtyID");
            });

            modelBuilder.Entity<ProviderUpdateStaging>(entity =>
            {
                entity.ToTable("ProviderUpdateStaging", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Created)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.NationalProviderIdentification).HasMaxLength(50);

                entity.Property(e => e.Proccessed).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProviderName).HasMaxLength(50);

                entity.Property(e => e.SubmitterGuid).HasColumnName("SubmitterGUID");

                entity.Property(e => e.UpdateLastName).HasMaxLength(50);
            });

            modelBuilder.Entity<ProviderUpdateSubmitter>(entity =>
            {
                entity.ToTable("ProviderUpdateSubmitter", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Created)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Guid).HasColumnName("GUID");

                entity.Property(e => e.SubmitterName).HasMaxLength(50);

                entity.Property(e => e.SubmittersEmailAddress).HasMaxLength(50);

                entity.Property(e => e.SubmittersPhoneNumber).HasMaxLength(50);

                entity.Property(e => e.SubmittersTin)
                    .HasColumnName("SubmittersTIN")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Providers>(entity =>
            {
                entity.HasKey(e => e.Npinumber)
                    .HasName("PK_Providers_1");

                entity.ToTable("Providers", "dbo");

                entity.Property(e => e.Npinumber)
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.Cms)
                    .HasColumnName("CMS")
                    .HasMaxLength(3);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateOfBirth).HasMaxLength(50);

                entity.Property(e => e.DegreeId).HasColumnName("DegreeID");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IsBhprivileged).HasColumnName("isBHPrivileged");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.MiddleName).HasMaxLength(10);

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("ModifiedDT")
                    .HasColumnType("datetime");

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.SpecialtyNotes).HasMaxLength(100);

                entity.Property(e => e.Suffix).HasMaxLength(100);
            });

            modelBuilder.Entity<ProvidersAudit>(entity =>
            {
                entity.ToTable("ProvidersAudit", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Cms)
                    .HasColumnName("CMS")
                    .HasMaxLength(3);

                entity.Property(e => e.CreatedDt)
                    .HasColumnName("CreatedDT")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateOfBirth).HasMaxLength(50);

                entity.Property(e => e.DegreeId).HasColumnName("DegreeID");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.IsBhprivileged).HasColumnName("isBHPrivileged");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.MiddleName).HasMaxLength(10);

                entity.Property(e => e.ModifiedDt)
                    .HasColumnName("ModifiedDT")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser).HasMaxLength(50);

                entity.Property(e => e.Npinumber)
                    .IsRequired()
                    .HasColumnName("NPINumber")
                    .HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.SpecialtyNotes).HasMaxLength(100);

                entity.Property(e => e.Suffix).HasMaxLength(100);
            });

            modelBuilder.Entity<ReturningMemberApplication>(entity =>
            {
                entity.HasKey(e => e.Guid);

                entity.ToTable("ReturningMemberApplication", "dbo");

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateDt).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.LeGuid).HasColumnName("LE_GUID");

                entity.Property(e => e.ModifiedDt).HasColumnType("datetime");

                entity.Property(e => e.Tin)
                    .IsRequired()
                    .HasColumnName("TIN")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SpecialtyMatrix>(entity =>
            {
                entity.ToTable("SpecialtyMatrix", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Specialty)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.SpecialtyId).HasColumnName("SpecialtyID");
            });

            modelBuilder.Entity<SpecialtyXref>(entity =>
            {
                entity.ToTable("SpecialtyXREF", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(2000);
            });

            modelBuilder.Entity<StatusChange>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("StatusChange", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StatusChange1)
                    .IsRequired()
                    .HasColumnName("StatusChange");
            });

            modelBuilder.Entity<StatusChangeReason>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("StatusChangeReason", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StatusChangeReason1)
                    .IsRequired()
                    .HasColumnName("StatusChangeReason");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
