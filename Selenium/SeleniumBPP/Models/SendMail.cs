﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using Newtonsoft.Json;
using SeleniumBPP.SeleniumModels;
using SeleniumCommon;

namespace SeleniumBPP.Models
{
    public class SendMail
    {
        public Configs configs;
        public SendMail()
        {
            configs = new Configs();
        }
        public string SendMailMessage(MailMessage m)
        {
            try
            {
                m.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient();
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Host = "smtp.bmcjax.com";
                
                smtp.Port = 25;
                smtp.EnableSsl = false;

                if (string.IsNullOrEmpty(m.To.ToString())) return "NO SENDER"; //must have a sender

                smtp.Send(m);

                return "";

            }
            catch (Exception err)
            {
                return err.Message;
            }
        }
    }
}
