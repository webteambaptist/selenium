﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class Messages
    {
        public int Id { get; set; }
        public DateTime CreatedDt { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string Attachments { get; set; }
    }
}
