﻿using System;
using System.Collections.Generic;

namespace SeleniumBPP.Models
{
    public partial class DegreeType
    {
        public int Id { get; set; }
        public int DegreeId { get; set; }
        public string Degree { get; set; }
        public string DegreeName { get; set; }
    }
}
